#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type pri = 1000000007;

type midds[300];
type begs[300];
type endds[300];

type elems[30][30];

void zz()
{
  for(int i=0; i<30; i++)
    for(int j=0; j<30; j++)
      elems[i][j] = 0;

  for(int i=0; i<300;i++) 
  {
    midds[i] = 0;
    begs[i] = 0;
    endds[i] = 0;
  }
}

string shorten(string s)
{
  for(int i=0; i<s.length()-1; i++)
    if(s[i] == s[i+1])
    {
      return s.substr(0, i) + shorten( s.substr(i+1, s.length()-i-1)  );
    }

  return s;
}

type factMod(type n)
{
  if(n == 0) return 1;
  return (n*(factMod(n-1)))%pri;
}

bool visiste[300];

type solve(type a)
{
  if(visiste[a])
    return 0;

  visiste[a] = 1;
  type next;
  type cc = 0;
  for(int i='a'; i<='z'; i++)
  {
    if(elems[a][i-97] != 0)
    {
      next = a;
      cc ++;
    }
  }
  if(cc > 1 || cc==0)
    return 0;
  // cout<<next<<"\n";
  return (factMod(elems[a][a])*solve(next))%pri;

}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);


  int T;
  cin>>T;
  int t=0;
  while(t<T)
  {
    int n;
    cin>>n;
    zz();
    string stab[122];
    for(int i=0; i<n ;i++) 
      { 
        string tmp;
        cin>>tmp;
        stab[i] = shorten(tmp);

      }

    for(int i=0; i<n;i++)
    {
      for(int j=1; j<stab[i].length()-1; j++)
        midds[stab[i][j]] = 1;

      elems[stab[i][0] - 97][ stab[i][stab[i].length()-1] - 97 ]++;
      begs[stab[i][0]] += 1;
      endds[stab[i][stab[i].length()-1] ] += 1;
    }

    bool nono = false;

    cout<<elems[2][3]<<"\n";

    for(int i=0; i<300; i++)
    {
      visiste[i] = 0;
      if( begs[i] && midds[i])
      {
        nono = true;
        break;
      }

      if( endds[i] && midds[i])
      {
        nono = true;
        break;
      }

      // if(begs[i] > 1 || endds[i] > 1)
      // {
      //   nono = true;
      //   break;
      // }
    }

    type res = 0;


    for(int x = 'a'; x<='z'; x++)
    {

      for(int i=0; i<300; i++)
        visiste[i] = 0;
      res += solve(x-97);

    }



  if(nono)
	 cout <<"Case #"<<t+1<<": "<<0<<"\n";
  else
   cout <<"Case #"<<t+1<<": "<<res<<"\n";


	t++;

  }

  return 0;
 
}