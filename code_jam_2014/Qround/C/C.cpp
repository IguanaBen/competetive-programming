#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int T;
  cin>>T;

  int cas = 1;

  	char array[1000][1000];
  while(T--)
  {
  	int r,c,m;
  	cin>>r>>c>>m;
  	cout<<"Case #"<<cas<<": ";
  	cas++;


  	for(int i=0; i<r; i++)
  		for(int j=0; j<c; j++)
  			array[i][j] = '.';

  		// cout<<"\n";
  	int xx =0;
  	while(m)
  	{
	  	// for(int i=xx; i<r && m>0; i++, m--)
	  	// 	array[xx][i] = '*';

	  	// for(int j=1+xx; j<c && m>0; j++, m--)
	  	// 	array[j][xx] = '*';

  		for(int i=xx; i>=0 && m>0; i--)
  			if(xx-i < r && i <c)
  			{
  				array[xx-i][i] = '*';
  				m--;
  			}
	  	xx++;
	  	// break;
	}

	array[r-1][c-1] = 'c';

	if((array[max(r-2,0)][c-1] == '.' || array[max(r-2,0)][c-1] == 'c' ) && 
		(array[max(r-2,0)][max(c-2,0)] == '.' || array[max(r-2,0)][max(c-2,0)] == 'c' ) && 
		(array[r-1][max(c-1,0)] == '.' || array[r-1][max(c-1,0)] == 'c' )  )
	{
		cout<<"\n";
		for(int i=0; i<r; i++)
		{
  			for(int j=0; j<c; j++)
  				cout<<array[i][j];

  			cout<<"\n";
  		}
	}
	else
	{
		cout<<"Impossible\n";
	}

  }

  return 0;
 
}