#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <string.h>
#include <set>

#define type long long
#define oo 1000000000 // Infinity

using namespace std;


type field[54][54];
const type infty = 1000000003;

int index(int a, int b)
{
  return a+b*1033;
}


const int N = 1033*1033;


const int INF = 2000000000;

typedef long long LL;

struct Edge {
  int from, to, cap, flow, index;
  Edge(int from, int to, int cap, int flow, int index) :
    from(from), to(to), cap(cap), flow(flow), index(index) {}
};

struct PushRelabel {
  int N;
  vector<vector<Edge> > G;
  vector<LL> excess;
  vector<int> dist, active, count;
  queue<int> Q;

  PushRelabel(int N) : N(N), G(N), excess(N), dist(N), active(N), count(2*N) {}

  void AddEdge(int from, int to, int cap) {
    G[from].push_back(Edge(from, to, cap, 0, G[to].size()));
    if (from == to) G[from].back().index++;
    G[to].push_back(Edge(to, from, 0, 0, G[from].size() - 1));
  }

  void Enqueue(int v) { 
    if (!active[v] && excess[v] > 0) { active[v] = true; Q.push(v); } 
  }

  void Push(Edge &e) {
    int amt = int(min(excess[e.from], LL(e.cap - e.flow)));
    if (dist[e.from] <= dist[e.to] || amt == 0) return;
    e.flow += amt;
    G[e.to][e.index].flow -= amt;
    excess[e.to] += amt;    
    excess[e.from] -= amt;
    Enqueue(e.to);
  }
  
  void Gap(int k) {
    for (int v = 0; v < N; v++) {
      if (dist[v] < k) continue;
      count[dist[v]]--;
      dist[v] = max(dist[v], N+1);
      count[dist[v]]++;
      Enqueue(v);
    }
  }

  void Relabel(int v) {
    count[dist[v]]--;
    dist[v] = 2*N;
    for (int i = 0; i < G[v].size(); i++) 
      if (G[v][i].cap - G[v][i].flow > 0)
  dist[v] = min(dist[v], dist[G[v][i].to] + 1);
    count[dist[v]]++;
    Enqueue(v);
  }

  void Discharge(int v) {
    for (int i = 0; excess[v] > 0 && i < G[v].size(); i++) Push(G[v][i]);
    if (excess[v] > 0) {
      if (count[dist[v]] == 1) 
  Gap(dist[v]); 
      else
  Relabel(v);
    }
  }

  LL GetMaxFlow(int s, int t) {
    count[0] = N-1;
    count[N] = 1;
    dist[s] = N;
    active[s] = active[t] = true;
    for (int i = 0; i < G[s].size(); i++) {
      excess[s] += G[s][i].cap;
      Push(G[s][i]);
    }
    
    while (!Q.empty()) {
      int v = Q.front();
      Q.pop();
      active[v] = false;
      Discharge(v);
    }
    
    LL totflow = 0;
    for (int i = 0; i < G[s].size(); i++) totflow += G[s][i].flow;
    return totflow;
  }
};



PushRelabel *dinn;
void add(int a, int b, type c)
{
  dinn->AddEdge(a,b,c);
}


bool fields[1033][1033];
void reset_fields()
{
	for(int i=0; i<1033; i++)
		for(int j=0; j<1033; j++)
			fields[i][j] = 0;
}


struct quad
{
	int x0, y0, x1, y1;
};

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);


  int T;
  cin>>T;
  int cases = 1;

  while(T--)
  {

	  PushRelabel din(N);
	  dinn = &din;
	  int w,h,b;
	  cin>>w>>h>>b;

	  set<int> wr;
	  vector<int

	  for(int i =0; i<b; i++)
	  {
	  	int x0, y0, x1, y1;
	  	cin>>x0>>y0>>x1>>y1;
	  	wr.insert(y0);
	  	wr.insert(y1);
	  }





	  int e = 0;
	  int s = 1;


	    type res = din.GetMaxFlow(s,e);
	    // type res = find_max_flow();
	    if(res >= infty)
	    	res = 0;

	cout<<"Case #"<<cases<<": "<<res<<"\n";

	cases++;

  }

  return 0;
 
}