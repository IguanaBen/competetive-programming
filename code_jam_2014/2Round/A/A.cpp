#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <set>

#define type long long

using namespace std;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int T;
  cin>>T;
  int cases = 1;
  while(T--)
  {
  	int res = 0;
  	int n, cap;
  	cin>>n>>cap;
  	
  	vector<int> vv;

  	for(int i=0; i<n; i++)
  	{
  		int xx;
  		cin>>xx;
  		vv.push_back(xx);
  	}

  	// cout<<(*riter)<<"\n";



  	while(vv.size() > 0)
  	{
  		vector<int>::iterator it = max_element(vv.begin(), vv.end());
  		int df = cap-(*it);
      vv.erase(it, it+1);
      // cout<<vv.size()<<"\n";
  		// cout<<df<<"\n";
  		vector<int>::iterator it2 = upper_bound(vv.begin(), vv.end(), df);
  		if(it2 != vv.begin())
  		{
  			// it2--;
  			int ind = it2-vv.begin();
  			// cout<<*it2<<" "<<ind<<"\n";
        // cout<<df<<"\n";
        // if(vv[ind-1] <= df)
        // cout<<vv[ind-1]<<"\n";
  			 vv.erase(vv.begin() +  -1 +  ind);
  		}

  		// if(it2 != vv.end())
  			// cout<<*it<<" "<<(*it2)<<"\n";

  		res++;

  	}


  	cout<<"Case #"<<cases<<": "<<res<<"\n";
  		cases++;
  }

  return 0;
 
}