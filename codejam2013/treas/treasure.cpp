#include <iostream>
#include <vector>
#include <sstream>
// #include <array>

using namespace std;

struct chest
{
	chest()
	{
		for (int i = 0; i < 50; ++i)
			key_types[i] = 0;

		k = 0;
		n_k = 0;
	}
	int k;
	int n_k;
	int key_types[50];
};



// struct sets
// {
// 	bool p;
// };

// sets tab[4194350];

chest ch[23];
string ans;

bool check(int n, int k,  bool *visited, int *sets, string &ans)
{

	// cout<<"K: "<<k<<endl;

	// for (int i = 0; i < n; ++i)
	// {
	// 	cout<<visited[i+1]<<" ";
	// }
	// cout<<endl;
	// for (int i = 0; i < n; ++i)
	// {

	// 	// 
	// 	cout<<sets[i+1]<<" ";
	// }
	// cout<<endl;

	if(k==0)
	{	
		ans = "";
		return true;
	}
	// cout<<"N"<<n<<endl;
	for (int i = 0; i < n; ++i)
	{
		if(visited[i+1] == false)
		{	
			// cout<<sets[ch[i+1].k]<<endl;
			if( sets[ch[i+1].k] > 0)
			{
				// cout<<k<<" "<<i+1<<endl;
				sets[ch[i+1].k]--;

				for (int j = 1; j <= n; ++j)
				{
					sets[j] += ch[i+1].key_types[j];
					// cout<<sets[j]<<endl;
				}
				// cout<<endl;

				visited[i+1] = true;
				bool b = check(n,k-1,visited, sets, ans);

				if(b)
				{
					stringstream ss;
					ss << (i+1);
					string str = ss.str();

					ans = str + " " + ans ;
					return true;
				}
				visited[i+1] = false;
				sets[ch[i+1].k]++;
				for (int j = 1; j <= n; ++j)
				{
					sets[j] -= ch[i+1].key_types[j];
					// cout<<ch[i+1].key_types[j]<<endl;
				}
			}
		}


	}

	return false;
}

int main()
{
	int t;
	cin>>t;
	for (int l = 0; l < t; ++l)
	{
		ans = "";
		int s_k, n;
		cin>>s_k>>n;
		int key_types[50];
		
		for (int i = 0; i < 50; ++i)
			key_types[i] = 0;

		for (int i = 0; i < s_k; ++i)
		{
			int r;
			cin>>r;
			key_types[r]++;
		}
		// cout<<"S";

		for (int i = 0; i < n; ++i)
		{
			cin>>ch[i+1].k>>ch[i+1].n_k;
			int r;
			// cout<<ch[i+1].k<<" " << ch[i+1].n_k<<endl;
			for (int j = 0; j < ch[i+1].n_k; ++j)
			{

				cin>>r;
				// cout<<ch[i+1].key_types[j]<<endl;
				ch[i+1].key_types[r] += 1;
			}
			// cout<<endl;
		}
		// cout<<endl<<endl;
		// ans = " ";
		string a;
		bool vis[21];

		// cout<<"Case #"<<l+1<<": ";

		for (int i = 0; i < 21; ++i)
			vis[i] = 0;
		if(check(n, n, vis, key_types, a ))
			cout<<a;
		else
			cout<<"IMPOSSIBLE";

		cout<<endl;
	}

	return 0;
}