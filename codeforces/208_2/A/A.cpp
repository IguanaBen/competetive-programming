#include <iostream>
#include <algorithm>

using namespace std;

int tab[1033][2];
const int off = 1000033;


bool not_intersect(int a1, int b1, int a2, int b2)
{
	return (( a2 <= a1 && b1 <=b2 ) || (a1 <=a2 && b2<=b1) || b2<=a1 || b1 <=a2)  ;
}

int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n;
	cin>>n;

	int prev;
	cin>>prev;

	for(int i=1;i<n;i++)
	{
		int t;
		cin>>t;

		if( t < prev)
		{
			tab[i-1][0] = t;
			tab[i-1][1] = prev;
		}
		else
		{
			tab[i-1][1] = t;
			tab[i-1][0] = prev;
		}

		prev = t;
	}

	for(int i=0; i<n-1; i++)
	{
		for(int j=0; j<n-1; j++)
		{
			if( i!= j)
			{
				if(! not_intersect(tab[i][0], tab[i][1], tab[j][0], tab[j][1] ))
				{
					cout<<"yes\n";
					return 0;
				}
			}
		}
	}

	cout<<"no\n";

	return 0;

}