#include <iostream>
#include <string>

using namespace std;

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int mn = 10000;
	int tab[] = {1,1,2,7,4};
	for(int i=0; i<5; i++)
	{
		int a ;
		cin>>a;
	 	mn = min(mn,a/tab[i]);
	}

	cout<<mn<<"\n";
	return 0;
}