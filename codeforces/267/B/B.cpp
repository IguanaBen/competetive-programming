#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;

unsigned int pl[10000];

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	int n, m, k;
	cin>>n>>m>>k;
	for(int i=0; i<m; i++)
		cin>>pl[i];

	unsigned int mm;
	cin>>mm;

	int res = 0;
	for(int i=0; i<m; i++)
		if(__builtin_popcount(pl[i]^mm) <= k)
			res++;
	
	cout<<res<<"\n";

	return 0;
}