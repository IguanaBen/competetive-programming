#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;

type dp[5011][5011];
type p[5011];

type ss[5011];

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	type n,m,k;
	cin>>n>>m>>k;

	ss[0] = 0;
	for(int i=0; i<n; ++i)
	{
		cin>>p[i];
		ss[i+1] = ss[i] + p[i];
	}

	for(int i=0; i<=n; ++i)
		for(int j=0; j<=k; ++j)
		{
			if(i == 0 || j == 0)
				dp[i][j] = 0;
			else
			{
				if( i >= m)
					dp[i][j] = max(dp[i-1][j], dp[i-m][j-1] + ss[i] - ss[i-m]);
				else
					dp[i][j] = dp[i-1][j];
			}
		}

	cout<<dp[n][k]<<"\n";


	return 0;
}