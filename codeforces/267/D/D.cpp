#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;

vector<int> graph[100044];
int lengths[100044];
int rs[100044];
int transClosure[100044];	//sort of...
int visited[100044];

string essay[100044];
int resultR[100044];
int resultLen[100044];
int verticesSorted[100044];

bool cmp(int a, int b)
{
	if(resultR[a] == resultR[b])
		return resultLen[a] < resultLen[b];
	else
		return resultR[a] < resultR[b];
}

void dfs(int v, int minVInd)
{
	transClosure[v] = minVInd;
	visited[v] = true;

	for(int i=0; i<graph[v].size(); ++i)
		if(!visited[graph[v][i]])
			dfs(graph[v][i], minVInd);
}

int main()
{
	// std::ios_base::sync_with_stdio(0);
	// cin.tie(NULL);

	int n;
	cin>>n;
	for(int i=0; i<n; ++i)
		cin>>essay[i];
	
	int m;
	cin>>m;
	string s1, s2;
	map<string, int> strToVMap;	//great name
	int nextV = 0;
	for(int j=0; j<m; ++j)
	{
		cin>>s1>>s2;
		std::transform(s1.begin(), s1.end(), s1.begin(), ::tolower);
		std::transform(s2.begin(), s2.end(), s2.begin(), ::tolower);

		if(strToVMap.find(s1) == strToVMap.end())
		{
			strToVMap[s1] = nextV;
			resultR[nextV] = std::count(s1.begin(), s1.end(), 'r');
			resultLen[nextV] = s1.length();
			nextV++;
		}

		if(strToVMap.find(s2) == strToVMap.end())
		{
			strToVMap[s2] = nextV;
			resultR[nextV] = std::count(s2.begin(), s2.end(), 'r');
			resultLen[nextV] = s2.length();
			nextV++;
		}

		int v1, v2;
		v1 = strToVMap[s1];
		v2 = strToVMap[s2];
		graph[v2].push_back(v1);
	}

	for(int i=0; i<nextV; ++i)
		verticesSorted[i] = i;

	sort(verticesSorted, verticesSorted + nextV, cmp);

	for(int i=0; i<nextV; ++i)
		if(!visited[ verticesSorted[i] ])
			dfs(verticesSorted[i], verticesSorted[i]);

	type totalR = 0;
	type totalLen = 0;

	for(int i=0; i<n; i++)
	{
		std::transform(essay[i].begin(), essay[i].end(), essay[i].begin(), ::tolower);

		if(strToVMap.find(essay[i]) != strToVMap.end())
		{
			int indx = strToVMap[essay[i]];
			totalR += resultR[transClosure[indx]];
			totalLen += resultLen[transClosure[indx]];
		}
		else
		{
			totalR += std::count(essay[i].begin(), essay[i].end(), 'r');
			totalLen += essay[i].length();
		}

	}

	cout<<totalR<<" "<<totalLen<<"\n";

	return 0;
}