#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);


	int cnt = 0;
	int n;
	cin>>n;
	for(int i=0; i<n; i++)
	{
		int a,b;
		cin>>a>>b;
		cnt += (a+2 <= b);
	}

	cout<<cnt<<"\n";

	return 0;
}