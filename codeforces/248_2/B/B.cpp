#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type tab1[100033];
type tab2[100033];

type vsums[100033];
type usums[100033];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;
  for(int i=0; i<n; i++)
  {
  	cin>>tab1[i];
  	tab2[i] = tab1[i];
  }
  sort(tab2, tab2 + n);

  vsums[0] = 0;
  usums[0] = 0;

  for(int i=0; i<n; i++)
  {
  	vsums[i+1] = vsums[i] + tab1[i];
  	usums[i+1] = usums[i] + tab2[i];
  }
  int m;
  cin>>m;
  int t,b,e;
  for(int i=0; i<m; i++)
  {
  	cin>>t>>b>>e;
  	if(t == 1)
  		cout<<vsums[e]-vsums[b-1]<<"\n";
  	else
  		cout<<usums[e]-usums[b-1]<<"\n";
  }

  return 0;
 
}