#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);
  int n;
  int n1=0, n2=0;
  cin>>n;
  for(int i=0; i<n; ++i) 
  {
  	int t;
  	cin>>t;
  	if(t == 100)
  		n1++;
  	else
  		n2++;
  }

  int sum = n1+ n2*2;
  if(sum % 2)
  	cout<<"NO\n";
  else
  	if(sum % 4 == 0)
  		cout<<"YES\n";
  	else
  		if(n1)
  			cout<<"YES\n";
  		else
  			cout<<"NO\n";

  return 0;
 
}