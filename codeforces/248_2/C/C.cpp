#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

type tab[100033];
type dif[100033];
vector<int> poss[100033];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n, m;
  cin>>m>>n;
  type sum = 0;

  for(int i=0; i<n; i++)
  {
  	cin>>tab[i];
  	// sum += tab[i];
  	if(i != 0)
  		sum += abs(tab[i] - tab[i-1]);
  	// if(i != n-1)
  		// sum += abs(tab[i] - tab[i+1]);

  	poss[tab[i]].push_back(i);
  }

  type mn = sum;
  for(int i=0; i<n; i++)
  {
  	type csum = sum;

  	type mergeleft = tab[i], mergeright = tab[i];
    if(i < n-1)
      mergeright = tab[i+1];

    if( i > 0)
      mergeleft = tab[i-1];

    if(mergeleft != tab[i] || mergeright != tab[i])
    {


  	for(int j=0; j<poss[tab[i]].size() && i < n-1; j++)
  	{
  		int cpos = poss[tab[i]][j];
  		if( cpos != 0 )
  		{
  			csum -= abs(tab[cpos] - tab[cpos-1]);
  			csum += abs(mergeright - tab[cpos-1]);
  		}

  		if( cpos != n-1)
  		{
  			csum -= abs(tab[cpos] - tab[cpos+1]);
  			csum += abs(mergeright - tab[cpos+1]);
  		}
  	}

  	mn = min(csum, mn);
  	csum = sum;
  	for(int j=0; j<poss[tab[i]].size() && i > 0; j++)
  	{
  		int cpos = poss[tab[i]][j];
  		if( cpos != 0 )
  		{
  			csum -= abs(tab[cpos] - tab[cpos-1]);
  			csum += abs(mergeleft - tab[cpos-1]);
  		}

  		if( cpos != n-1)
  		{
  			csum -= abs(tab[cpos] - tab[cpos+1]);
  			csum += abs(mergeleft - tab[cpos+1]);
  		}
  	}
  	mn = min(csum, mn);
  	}

  }
  cout<<mn<<"\n";



  return 0;
 
}