#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	int n,m,a,b;
	cin>>n>>m>>a>>b;

	if(b >= m*a)
		cout<<n*a<<"\n";
	else
	{
		int res = (n/m)*b;
		int dd = n % m;
		res += min(b, dd*a);
		cout<<res<<"\n";
	}

	return 0;
}