#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;


type arr[1000042];
type pref_sum[1000042];
type pref_sum_r[1000042];

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	
	int n;
	cin>>n;

	for(int i=0; i<n; i++)
		cin>>arr[i];

	vector<int> il;
	vector<int> ir;

	pref_sum[0] = arr[0];
	for(int i=1; i<n; i++)
		pref_sum[i] = pref_sum[i-1] + arr[i];

	pref_sum_r[n-1] = arr[n-1];
	for(int j=n-2; j>=0; j--)
		pref_sum_r[j] = pref_sum_r[j+1] + arr[j];


	type target = pref_sum_r[0];
	type res = 0;

	if(target % 3 != 0)
	{
		cout<<"0\n";
		return 0;
	}

	target /= 3;

	for(int i=0; i<n; i++)
		if(pref_sum_r[i] == target)
		{
			// cout<<i<<"\n";
			ir.push_back(i);
		}

	type i2 = 0;

	type irc = ir.size();

	// cout<<pref_sum[0]<<"\n";

	for(int i=0; i<n; i++)
	{
		if(pref_sum[i] == target)
		{
			while(ir[i2] <= (i+1) )
			{
				i2++;
				if(i2 >= irc)
					break;
			}

			// cout<<i<<" "<<i2<<"\n";
			if(i2 < irc)
				res += (irc - i2);
		}
	}

	target /= 3;

	cout<<res<<"\n";


	return 0;
}