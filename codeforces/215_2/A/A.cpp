#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int tab[1000];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n,d,m;
  cin>>n>>d;

  for(int i=0; i<n; i++)
  	cin>>tab[i];

  cin>>m;

  sort(tab, tab +n);

  int sum = 0;

  for(int i=0; i<min(m, n); i++)
  	sum += tab[i];

  for(int i= min(m,n); i<m; i++)
  	sum -= d;

  cout<<sum<<"\n";

  return 0;
 
}



