#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int a[1000000];
int b[1000000];

int pos[1000000];

map<type, type> ma;
map<type, type> mb;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n,m,p;
  cin>>n>>m>>p;

  int l = (m-1)*p;

  int num_bad = 0;

// cout<<l<<"\n";
  // cout<<ma[12313]<<"\n";

  for(int i=0; i<n; i++)
  {
  	cin>>a[i];
 	ma[a[i]] = 0;
 	mb[a[i]] = 0;

  }


  for(int i=0; i<m; i++)
  {
  	cin>>b[i];
 	ma[b[i]] = 0;
 	mb[b[i]] = 0;
  }

  for(int i=0; i<m; i++)
  	mb[b[i]]++;

  // cout<<l<<"\n";

  if( l > n)
  {
  	cout<<0<<"\n";
  }
  else
  {

  	int num_pos = 0;
  	for(int i=0; i<=l; i++)
  		ma[a[i]]++;

  	for(int i=0; i<m ;i++)
  	{
  		if(mb[b[i]] > ma[b[i]])
  			num_bad++;
  	}

  	int s = 0;
  	// cout<<l<<"\n";
  	while(1)
  	{
  		// cout<<ma[1]<<" "<<ma[2]<<" "<<ma[3]<<" "<<ma[4]<<" "<<num_bad<<"\n";
  		if(num_bad == 0)
	  	{
	  		pos[num_pos] = s+1;
	  		// cout<<"ASds\n";
	  		num_pos++;
	  	}

	  	if(ma[a[s]] == mb[a[s]] && mb[a[s]] != 0)
	  		num_bad++;


	  	l++;
	  	ma[a[s]]--;

	  	if( l == n)
	  		break;

	  	if(ma[a[l]] + 1 == mb[a[l]] && mb[a[l]] != 0)
	  		num_bad--;

	  	ma[a[l]]++;

	  	s++;
  	}

  	cout<<num_pos<<"\n";

  	for(int i=0; i<num_pos; i++)
  		cout<<pos[i]<<" ";

  	if(num_pos)
 	 	cout<<"\n";
  }

  return 0;
 
}



