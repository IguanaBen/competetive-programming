#include <iostream>
#include <algorithm>

using namespace std;


int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n;
	cin>>n;
	while(n>=0)
	{
		int digit = n % 10;
		n/=10;

		if( digit >= 5)
		{
			cout<<"-0|";
			for(int i=0;i<digit-5; i++)
				cout<<"0";
			cout<<"-";
			for(int i=0;i< 5-(digit-5) -1; i++)
				cout<<"0";
			
		}
		else
		{
			cout<<"0-|";
			for(int i=0;i<digit; i++)
				cout<<"0";
			cout<<"-";
			for(int i=0;i< 5-(digit) - 1; i++)
				cout<<"0";
		}
		cout<<"\n";

		if( n ==0)
			break;

	}


	return 0;

}