#include <iostream>
#include <algorithm>

using namespace std;


int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n,k;
	cin>>n>>k;

	int sum = 2*k;

	int inv_pairs = (n-k);

	for(int i=0; i<inv_pairs; i++)
		cout<<2*i+2<<" "<<2*i+1<<" ";

	for(int i=inv_pairs; i<n; i++)
		cout<<2*i+1<<" "<<2*i+2<<" ";

	cout<<"\n";
	return 0;

}