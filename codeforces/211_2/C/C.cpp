#include <iostream>
#include <algorithm>
#include <map>

#define type long long

using namespace std;


int tab[100033];

type qpow(type a, type b, type mod)
{
	if( b == 0)
		return 1;

	if( b== 1)
		return a;

	if( b % 2)
	{
		type rr = qpow(a, b/2, mod);
		return (rr*rr*a) % mod;
	}

		type rr = qpow(a, b/2, mod);
		return (rr*rr) % mod;

}

type qpow2(type a, type b, type mod)
{
	if( b == 0)
		return 1;

	if( b== 1)
		return a;

	if( b % 2)
	{
		type rr = qpow(a, b/2, mod);
		if( (rr*rr*a) > mod  || rr == -1 )
			return -1;
		else
			return (rr*rr*a);
	}

		type rr = qpow(a, b/2, mod);

		if( rr * rr > mod || rr == -1)
			return -1;
		else
			return rr*rr;

}

int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n,x;
	cin>>n>>x;
	
	map<type, type> mapss;

	for(int i=0; i<n; i++)
	{
		type a;
		map[a]++;
		tab[i] = a;
	}


	for(int i=0; i<n; i++)
	{
		if( map[tab[i]] != 0)
		{
			type xx = tab[i];

			type pw = qpow2(xx, map[tab[i]], 100000);
			if(pw != -1)
			{
				map[tab[i]] =0;
			}
		}
	}


	return 0;

}