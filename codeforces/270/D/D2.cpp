#include <iostream>
#include <algorithm>
#include <vector>
#include <stdlib.h> 

using namespace std;

int edge_i = 0;

int arr1[2003][2003];
int arr2[2003][2003];

int prime =  2113;

int vx[2003];
int vy[2003];
int vz[2003];
int vzp[2003];

void random_v(int n, int *vx)
{

	for(int j=0; j<n; j++)
		vx[j] = rand() % prime;

	// return res_v;
}


void vec_mult(int arr[2003][2003], int n, int *vx, int *vy)
{

	for(int i=0; i<n; i++)
	{
		int res = 0;
		for(int j=0; j<n; j++)
		{
			res += (arr[i][j] * vx[j]);
			res = res;
		}
		vy[i] = res;
		// res_v.push_back(res);
	}
}

const int iter = 3;

int main()
{	
	cin.tie(NULL);
  	std::ios::sync_with_stdio(false);
	srand (time(NULL));

		int n;
		cin>>n;

		int val = 0;
		edge_i = 0;
		for(int i=0; i<n; i++)
		{
			for(int j=0; j<n; j++)
			{
				cin>>arr1[i][j];

			}
		}

		for(int i=0; i<n; i++)
		{
			for(int j=0; j<n; j++)
			{
				cin>>arr2[i][j];

			}
		}

		bool O = true;

		for(int i=0; i<iter; i++)
		{
			random_v(n, vx);
			vec_mult(arr1, n, vx, vy);
			vec_mult(arr1, n, vy, vz);
			vec_mult(arr2, n, vx, vzp);

			bool ok = true;
			for(int j=0; j<n; j++)
			{
				if(vz[j] > vzp[j])
				{
					ok = false;
					break;
				}
			}

			if(!ok)
			{
				O = false;
				break;
			}
		}

		if(O)
			cout<<"TAK\n";
		else
			cout<<"NIE\n";
	return 0;
}
