#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;


type tab[2042];

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	int n,k;
	cin>>n>>k;
	for(int i=0; i<n; i++)
		cin>>tab[i];

	type res = 0;
	sort(tab, tab + n);
	for(int j=n-1; j>=0; j-=k)
		res += (tab[j]-1)*2;

	cout<<res<<"\n";

	return 0;
}