#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;

bool primes[1000042];

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	vector<int> composites;

	int n;
	cin>>n;

	for(int i=0; i<1000042; i++)
		primes[i] = true;

	primes[2] = true;
	for(int i=2; i<1000042; i++)
	{
		if(primes[i] == true)
			for(int j=2*i; j<1000042; j+= i)
				primes[j] = false;
	}
	// cout<<primes[4]<<"\n";

	for(int i=4; i<n; i++)
	{
		if(!primes[i] && !primes[n-i] )
		{
			cout<<i<<" "<<n-i<<"\n";
			return 0;
		}
	}

	return 0;
}