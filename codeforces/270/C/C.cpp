#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;

string names[100042];
string surnames[100042];


bool dp[100042][2];
int perm[100042];

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);


	dp[0][0] = 1;
	dp[0][1] = 1;

	int n;
	cin>>n;
	for(int i=0; i<n; i++)
		cin>>names[i]>>surnames[i];

	for(int i=0; i<n; i++)
	{

		cin>>perm[i];
		perm[i]--;
	}

	// cout<<(names[1] > names[0])<<"\n";

	for(int i=1; i<n; i++)
	{
		if(names[ perm[i] ] > names[perm[i-1]])
			dp[i][0] |= dp[i-1][0];

		if(names[ perm[i] ] > surnames[perm[i-1]])
			dp[i][0] |= dp[i-1][1];

		if(surnames[ perm[i] ] > names[perm[i-1]])
			dp[i][1] |= dp[i-1][0];

		if(surnames[ perm[i] ] > surnames[perm[i-1]])
			dp[i][1] |= dp[i-1][1];
	}

	// for(int i=1; i<n; i++)
	// {
	// 	cout<<dp[i][0]<<"\n";
	// 	cout<<dp[i][1]<<"\n\n";	
	// }
	// cout<<dp[2][0]<<"\n";
	// cout<<dp[3][1]<<"\n";	

	if(dp[n-1][0] || dp[n-1][1])
		cout<<"YES\n";
	else
		cout<<"NO\n";

	return 0;
}