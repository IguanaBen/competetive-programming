#include <iostream>


using namespace std;


int gcd(int x, int y)
{
	if(y == 0)
		return x;

	return gcd(y, x % y );
}

int main()
{

	int a,b,x,y;

	cin>>x>>y>>a>>b;
	int lcm = x*y;
	lcm /= gcd(x,y);

	if( a % lcm == 0)
		cout<< b/lcm - a/lcm +1<<"\n";
	else
		cout<<b/lcm - a/lcm<<"\n";

	// cout<<(b/lcm)

	
	return 0;
}