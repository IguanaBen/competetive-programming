#include <iostream>
// #include <math.h>
#include <algorithm>

using namespace std;


long long tab[100042];


long long gcd(long long x, long long y)
{
	if(y == 0)
		return x;

	return gcd(y, x % y );
}



int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	long long n;
	cin>>n;

	for(int i=0; i<n; i++)
		cin>>tab[i];

	sort(tab, tab+n);

	long long super_sum = 0;
	long long ss2 = 0;

	long long sum = tab[0];
	for(int a=1; a<n;a++)
	{
		// cout<<tab[a]<<endl;
		ss2 += (tab[a]-tab[a-1])*a;
		super_sum += ss2;

		sum+= tab[a];
	}

	// cout<<super_sum<<endl;


	long long res = super_sum*2 + sum;
	long long d = n;

	long long gc = gcd(res, d);

	cout<<res/gc<<" "<<n/gc<<"\n";


	return 0;

}