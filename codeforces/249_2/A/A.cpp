#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int bus = 0;
  int bus_n = 1;
  int n,m;
  cin>>n>>m;
  while(n--)
  {
  	int t;
  	cin>>t;
  	bus += t;
  	if( bus > m)
  	{
  		bus = t;
  		bus_n++;
  	}
  }
  cout<<bus_n<<"\n";

  return 0;
 
}