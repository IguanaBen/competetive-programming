#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

char chart[1033][50033];
int input[1033];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;


  for(int i=0; i<n; i++)
  	cin>>input[i];


  int mn = input[0];
   int s1 = 0;
  int s2 = 0;

  int mx = 0;
  for(int i=0; i<n; i++)
  {
  	s1 += input[i];

  	if(i % 2)
  		s2 += input[i];
  	else 
  		s2 -= input[i];

  	mn = min(mn, s2);
  	mx = max(mx, s2);

  }

  // cout<<mn<<"\n";

  int prev = 0;
  int prevv = 0-mn;

  s1 = 0;
  s2 = 0;

  if(input[0]  > 0 )
  	chart[s1][s2 - mn] = '/';
  else
  	chart[s1][s2 - mn] = '\\';

  for(int i=0; i<n; i++)
  {
  	s1 += input[i];

  	if(i % 2)
  		s2 += input[i];
  	else 
  		s2 -= input[i];

  	int cc =0;

  	if(s2-mn < prevv)
  		for(int kk = prev+1; kk<s1; kk++)
  		{
  			cc++;
  			chart[kk][prevv -cc] = '/';
  			// cout<<"SS\n";

  		}
  	else
  		for(int kk = prev+1; kk<s1; kk++)
  		{
  			// cout<<prevv - cc<<"\n";
  			chart[kk][ prevv +cc ] = '\\';
  			cc++;
  			// cout<<"SS\n";
  		}



  	if(s2-mn < prevv)
  		chart[s1][s2 - mn] = '/';
  	else
  		chart[s1][s2 - mn - 1] = '\\';

  	// cout<<s1<<" "<<s2-mn<<"\n";

  	prev = s1;
  	prevv = s2-mn;

  }
  // cout<<"MX: "<<mx<<"\n";


  for(int i=0; i<(mx-mn); i++)
  {
  	for(int j=1; j< s1+1; j++)
  	{
  		if(chart[j][i] == '\\' || chart[j][i] == '/' )
  			cout<<chart[j][i];
  		else
  			cout<<" ";
  	}

  	// if( i != mx-1)
	  	cout<<"\n";
  }




  return 0;
 
}