#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  string s;
  int k;
  cin>>s>>k;

  if(s.length() == 1)
  {
  	cout<<s<<"\n";
  	return 0;
  }

// cout<<s.substr(0, 0)<<" |"<<"\n";
  while(k > 0 && s.length() > 0)
  {
  	int mx_ind = 0;
  	char mxc = s[0];
  	for(int i=0; i<k &&  i+1 < s.length(); i++)
  	{
  		if(s[i+1] > mxc)
  		{
  			mx_ind = i+1;
  			mxc = s[i+1];
  		}
  	}

  	if(mx_ind == 0)
  	{
  		cout<<s[0];
  		if( s.length() > 0)
  			s = s.substr(1, s.length() - 1 );
  		else
  			return 0;
  		// cout<<" X\n";
  		// break;
  	}
  	else
  	{
  		// cout<<" ss\n";

  	k -= mx_ind;
  	// cout<<s[mx_ind]<<" "<<s.substr(1, mx_ind-1)
  	// <<" "<<s.substr(mx_ind+1, s.length() - mx_ind + 1)<<"\n";
  	cout<<s[mx_ind];

  	if(mx_ind+1 < s.length())
  		s = s.substr(0, mx_ind) + s.substr(mx_ind+1, s.length() - mx_ind + 1);
  	else
  		s = s.substr(0, mx_ind);

    }

  }
  cout<<s<<"\n";

  return 0;
 
}