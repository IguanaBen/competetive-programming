#include <iostream>
#include <algorithm>

using namespace std;


int tab[200022][2];

int watched(int step, int x)
{
	if(tab[step][0] <= x && x <= tab[step][1])
		return true;
	else
		return false;
}

int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n,m,s,f;


	cin>>n>>m>>s>>f;

	int t;

	for(int i=0;i<m;i++)
	{

		cin>>t;
		if(t > 200021)
			break;

		cin>>tab[t][0]>>tab[t][1];
	}

	int step_n = 1;

	if( s < f)
	{
		int t_d = f-s;

		int ind = s;
		while(t_d > 0)
		{
			if(watched(step_n, ind) || watched(step_n, ind+1))
				cout<<"X";
			else
			{
				cout<<"R";
				t_d--;
				ind++;

			}
			step_n++;
		}
	}
	else
	{
		int t_d = s-f;

		int ind = s;
		while(t_d > 0)
		{
			if(watched(step_n, ind) || watched(step_n, ind-1))
				cout<<"X";
			else
			{
				cout<<"L";
				t_d--;
				ind--;

			}
			step_n++;
		}
	}
	cout<<"\n";	



	return 0;

}