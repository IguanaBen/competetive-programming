#include <iostream>
#include <algorithm>

#define type long long

using namespace std;

type tab[1076];

type gcd(type a, type b)
{
	if(a == 0)
		return b;

	if(b == 0)
		return a;

	return gcd(b, a % b);
}

// inna definicja gcd, ta z odejmowaniem
// zamiast min_gcd gcd wszystkich!

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n;
	cin>>n;
	for(int i=0; i<n; i++)
		cin>>tab[i];


	type min_gcd = 1000000044;
	type maxx = -1;

	for(int i=0; i<n; i++)
		for(int j=0; j<i; j++)
		{
			min_gcd  = min(gcd(tab[i], tab[j]), min_gcd);
			maxx = max(maxx, tab[i]);
		}

	type d = maxx / min_gcd;
	for(int i=0; i<n; i++)
	{
		if(tab[i] % min_gcd == 0)
			d--;
	}

	if( d % 2)
		cout<<"Alice\n";
	else
		cout<<"Bob\n";



	return 0;
}