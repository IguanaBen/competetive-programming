#include <iostream>
#include <algorithm>

using namespace std;

int tab[1000];


int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n;
	cin>>n;

	for(int i=0; i<n;i++)
		cin>>tab[i];

	sort(tab, tab+n);

	cout<<tab[n-1]<<" ";

	for(int i=1; i<n-1;i++)
		cout<<tab[i]<<" ";
		
	cout<<tab[0]<<"\n";

	return 0;
}