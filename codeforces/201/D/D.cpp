#include <iostream>
#include <string>

#define type long long

using namespace std;

int stack[100007];

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);


	string str;
	cin>>str;

	int stack_size = 0;
	int index = 0;
	stack[index] = -1;

	for(int i=0; i<str.length(); i++)
	{
		stack_size++;
		if(str[i] == '+')
			stack[stack_size] = 1;
		else
			stack[stack_size] = 0;

		while(stack_size > 1 && stack[stack_size] == stack[stack_size-1])
		{
			stack_size -= 2;
		}
	}

	if(stack_size == 0)
		cout<<"Yes\n";
	else
		cout<<"No\n";

	return 0;
}