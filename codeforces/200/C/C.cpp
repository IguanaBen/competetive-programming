#include <iostream>
#include <string>

#define type long long

using namespace std;

type solve(type a, type b)
{
	if( a == 0 || b == 0)
		return 0;

	if( a >= b)
		return a/b + solve(a % b, b);
	else
		return b/a + solve(b % a, a);

}

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	type a, b;
	cin>>a>>b;

	cout<<solve(a,b)<<"\n";

	return 0;
}