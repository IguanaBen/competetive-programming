#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int deg[100033];
vector< pair<int, int> > graph[100033];

bool mark[100033];

int num(int v, bool need, int par)
{
	// cout<<v<<" "<<par<<"\n";

	if(graph[v].size() == 1 && v != 1)
	{
		mark[v] = need;
		return need;
	}

	
	int sum = 0;
	for(int i=0; i<graph[v].size(); i++)
	{
		if(graph[v][i].first != par)
		{
			// cout<<v<<" "<<graph[v][i].first<<" "<<graph[v].size()<<"\n";
			if( graph[v][i].second == 1)
				sum += num(graph[v][i].first, 0, v);
			else
				sum += num(graph[v][i].first, 1, v);
		}
	}

	if(sum == 0 && need)
	{
		mark[v] = 1;
		return 1;
	}

	return sum;

}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);


  int n;
  cin>>n;
  for( int i=0 ;i<n-1; i++)
  {
  	int a,b,c;
  	cin>>a>>b>>c;
  	
  	graph[a].push_back(make_pair(b,c));
  	graph[b].push_back(make_pair(a,c));
  }

  int r = num(1,0, 0);
  cout<<r<<"\n";
  for(int i=0; i<n; i++)
  {
  	if(mark[i+1])
  		cout<<i+1<<" ";
  }

  cout<<"\n";

  return 0;
 
}



