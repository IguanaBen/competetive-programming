#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <math.h>
#include <string>

#define type long long

using namespace std;
const type prime = 1000000007;

int pi[100033];
bool occ[100033];


void calculate_pi(string s)
{
	pi[1] = 0;
	int d = 0;
	for(int i=2; i<=s.length(); i++)
	{
		while(d > 0 && s[d] != s[i-1])
			d = pi[d];

		if(s[d] == s[i-1])
			d++;

		pi[i] = d;
	}
}

void occurences(string t, string pat)
{
	// vector<int> result;
	string temp = pat + "#" + t;
	calculate_pi(temp);
	int l = pat.length();
	for(int i=pat.length()+1; i<temp.length(); i++)
	{
		if(pi[i+1] == l)
			occ[i-l] = true;
			// result.push_back(i-l-1);
	}
	// return result;
}

type res[100033];
type ss[100033];
type k[100033];
type n[100033];
type s[100033];

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	string s,t;
	cin>>s>>t;
	occurences(s, t);

	int l = s.length();
	k[0] = 0;
	n[0] = 0;
	for(int i=1; i<=s.length(); i++)
	{
		if(!occ[i])
		{
			n[i] = (k[i-1] + n[i-1]) % prime;
			k[i] = k[i-1];
			// occ[i] = occ[i-1];
		}
		else
		{
			n[i] = (k[i-1] + n[i-1] ) % prime;
			k[i] = (k[i-1] + k[i-l] + n[i-l] + n[i-1] + 1) % prime;
		}
		// else
			// occ[i] = (occ[i-l] + ss[i-l]) % prime;

	}

	cout<< (n[l] + k[l]) % prime<<"\n";

	// for(int i=0; i<vec.size(); i++)
	// {
	// 	cout<<vec[i]<<" ";
	// }

	// cout<<"\n";
	// for(int i=1; i<=s.length() + t.length() + 1; i++)
	// {
	// 	cout<<pi[i]<<" ";
	// }
	// cout<<"\n";

	// cout<<"\n";
	// for(int i=0; i<str.length(); i++)
	// {

	// }

	return 0;
}