#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <math.h>

#define type long long
#define tt int

using namespace std;

// const int tree_size = 1000044;
const int last_level = 1048576;
tt ctree[last_level*2];

void set_value(int ind, tt val)
{
	int i1 = last_level + ind;
	ctree[i1] = val;
	i1/=2;
	while(i1 != 1)
	{
		ctree[i1] = ctree[i1*2] + ctree[i1*2 + 1];
		i1/=2;
	}
}

tt cnt(int b, int e)
{
	if(b == e) return ctree[b + last_level];

	int i1 = b + last_level;
	int i2 = e + last_level; 
	tt res = ctree[i1] + ctree[i2];

	while(i1/2 != i2/2)
	{
		if( i1 % 2 == 0) res += ctree[i1 + 1];
		if( i2 % 2) res += ctree[i2 - 1];
		i1 /= 2;
		i2 /= 2;
	}

	return res;
}

int arr[1000043];
int pos[1000043];

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	int n;
	cin>>n;

	map<int, int> positions;
	set<int> kks;

	type res = 0;

	for(int i=0; i<n; i++)
		cin>>arr[i+1];

	for(int i=0; i<n; i++)
	{
		int t = arr[i+1];
		pos[t] = i+1;
		if(t > arr[i])
		{

			map<int,int>::iterator it = positions.upper_bound(t);
			map<int,int>::iterator itx = positions.lower_bound(t);;

			int sind = 0;
			if(it != positions.end())
				sind = it->second;
			// cout<<sind<<"\n";
			res += cnt(sind, i) + 1;
			positions.erase(positions.begin(), itx);
			set_value(i+1, 1);
		}
		else
		{
			res += 1;
			set<int>::iterator it= kks.upper_bound(t);
			set<int>::iterator itx= it;
			for(;it!= kks.end(); it++)
				set_value(pos[*it], 0);

			kks.erase(itx, kks.end());

			set_value(i+1, 1);
		}
		kks.insert(t);
		// cout<<cnt(0, i+1)<<"\n";

		positions[t] = i+1;
	}
	// cout<<"\n";
	cout<<res<<"\n";

	return 0;
}