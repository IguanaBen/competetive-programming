#include <iostream>
#include <string>
#include <vector>

using namespace std;

#define type long long

const type prime = 1000000007;

type mod_string(string str)
{
    if(str.length() == 1)
        return (str[0] - '0');
    
    type res = (str[0] - '0');

    for(int i=1; i<str.length(); i++)
    {
        res*=10;
        res %= prime;

        res += (str[i] - '0');
    }

    return (res % prime);
}

// string div2(string s)
// {
//     string d2 = s;
//     type acc = 0;
//     for(int i=0; i<s.length(); i++)
//     {
//         acc += s[i] - '0';
//         type r = acc/2; 
//         d2[i] = (acc/2 + '0'); 
//         acc -= r*2;
//         acc*=10;
//     }
//     return d2;
// }

type qpows(type a, string &b, int i1, int i2)
{
    if(a == 0)
        return 0;

    if(i1 == i2)
    {
        if(b[i2] == '0') return 1;

        type res = a;
        for(int i=0; i<b[i2]-'0'-1;i++)
            res = (res*a) % prime;
        return res;
    } 


    type tr = qpows(a, b, i1, i2-1);
    type t = tr;
    // cout<<tr<<"\n";
    for(int i=0; i<9; i++)
        t = (t*tr) % prime;

    for(int i=0; i<b[i2] - '0'; i++)
        t = (t*a) % prime;

    return t;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int t;
    cin>>t;
    while(t--)
    {
        string s1, s2;
        cin>>s1>>s2;
        cout<<qpows(mod_string(s1), s2, 0, s2.length()-1)<<"\n";
    }

    // cout<<qpows(2, "12", 0,1)<<"\n";
    // cout<<div2("1")<<" "<<5463/2<<"\n";
    // cout<<qpow(2,4)<<"\n";
    // cout<<mod_string("12")<<"\n";
    return 0;
} 