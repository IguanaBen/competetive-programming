#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

const int last_level = 131072;
const int root_index = 1;

struct node
{
	int m_sum;
	int max_delay;

	node(){ max_delay = -1000000; m_sum = 0; }
};

node tree[last_level*4 + 42];

int get_max()
{
	return tree[root_index].max_delay;
}

void update_node( int node_index)
{
	tree[node_index].m_sum = tree[node_index*2].m_sum + tree[node_index*2 + 1].m_sum;
	tree[node_index].max_delay = max( tree[node_index*2].max_delay,
			 tree[node_index*2 + 1].max_delay + tree[node_index*2].m_sum );
}

void update(int m, int d)
{
	int index = last_level + d;

	tree[index].m_sum += m;
	tree[index].max_delay = tree[index].m_sum - d;

	index /= 2;

	while(index >0)
	{
		update_node(index);
		index/=2;
	}

}


int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n;
	int m, d;
	cin>>n;
	for(int i=0; i<n; i++)
	{
		cin>>d>>m;
		update(m, d);
		int res = get_max();
		if(res > 0)
			cout<<res<<"\n";
		else
			cout<<0<<"\n";
	}


	return 0;
}