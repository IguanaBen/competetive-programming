#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

const int last_level = 131072;
const int root_index = 1;

struct node
{
	int m_sum;
	int delay;
	int max_delay;

	node(){ max_delay = 0; delay=0; m_sum = -1000; }
};

node tree[last_level*4 + 42];

int get_max()
{
	return tree[root_index].max_delay;
}


int get_val(int d)
{
	int index = 1;
	int subtree_size = last_level / 2;
	int sum = 0;

	while( index < last_level )
	{
		if( d >= subtree_size)
		{
			sum += tree[index*2].m_sum;
			index = index*2 + 1;
			d -= subtree_size;
		}
		else
		{
			index = index*2;

		}

		subtree_size /=2;
	}

	sum += tree[index].m_sum;

	return sum;

}

void update_node(int node_index, int subtree_size)
{	

	if(node_index < last_level)		//lub subtree_size > 0
	{
			tree[node_index].max_delay = 
				tree[node_index].delay +
				max( tree[node_index*2].max_delay,  
					tree[node_index*2 + 1].max_delay - (subtree_size - tree[node_index*2].m_sum) );

			tree[node_index].m_sum = tree[node_index*2].m_sum + tree[node_index*2 + 1].m_sum;

	}
	else
		tree[node_index].max_delay = tree[node_index].m_sum;
}

void add_delay(int d, int del, int index, int subtree_size)
{
	// int index = 1;
	// int subtree_size = last_level / 2;

	if( index < last_level )
	{
		if( d >= subtree_size)
		{
			cout<<"R "<<index<<" "<<subtree_size<<endl;
			add_delay(d-subtree_size, del, index*2 + 1, subtree_size/2);
		}
		else
		{
			cout<<"L "<< index<<" "<<subtree_size<<endl;
			tree[index*2 + 1].delay += del;
			add_delay(d, del, index*2, subtree_size/2);
		}
	}
	else
	{
		cout<<"ind: "<<index - last_level<<" "<<d<<endl;
		tree[index].delay += del;
	}
	update_node(index, subtree_size);

}

void update(int m, int d)
{

	// tree[last_level + d].delay += m;
	int m_val = get_val(d);
	tree[last_level + d].m_sum += m;
	tree[last_level + d].val = d - m_val + m;



	int index = last_level + d;
	int subtr_size = 1;

	update_node(index, 0);
	index /=2;
	while( index > 0)
	{
		// update_node(index, subtr_size);
		tree[index].m_val = 
		// subtr_size *= 2;
		index /= 2;
	}


	cout<<": "<<get_max()<<endl;

	int r = get_val(d);
	// r = r-d ??
	cout<<"$: "<<r<<endl;
	add_delay(d+1, r, 1, last_level/2);

}


int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n;
	int m, d;
	cin>>n;
	for(int i=0; i<n; i++)
	{
		cin>>d>>m;
		update(m, d);
		int res = get_max();
		if(res > 0)
			cout<<res<<"\n";
		else
			cout<<0<<"\n";

		cout<<"FAG "<<tree[(47+last_level)/32].max_delay<<
		" "<<tree[(47+last_level)/16].max_delay<<
		" "<<tree[(47+last_level)/16 + 1].max_delay - ( 4 - tree[(47+last_level)/16].m_sum)<<endl;
	}


	return 0;
}