#include <iostream>
#include <string>
#include <vector>

using namespace std;

#define type long long

const type prime = 1000000007;

vector<int> cnt[1000033];
int prices[1000033];
int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int t;
    cin>>t;
    while(t--)
    {
        int n, m;
        cin>>m>>n;
        for(int i=0; i<10044;i++)
            cnt[i].clear();

        for(int i=0; i<n; i++)
        {
            int t;
            cin>>t;
            prices[i] = t;
            cnt[t].push_back(i+1);
        }

        for(int i=0; i<n; i++)
        {
            int d = m-prices[i];
            if(d > 0)
            {
                if(cnt[d].size() > 0 && d != prices[i])
                {
                    cout<<min(cnt[d][0], cnt[prices[i]][0])<<" "<<max(cnt[d][0], cnt[prices[i]][0])<<"\n";
                    break;
                }

                if(cnt[d].size() > 1 && d == prices[i])
                {
                    cout<<min(cnt[prices[i]][1], cnt[prices[i]][0])<<" "<<max(cnt[prices[i]][1], cnt[prices[i]][0])<<"\n";
                    break;
                }
            }
        }
    }
    return 0;
} 