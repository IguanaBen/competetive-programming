
#TODO: clear ancestor array after deletion

def solve():

	p = int(input())
	Pp = 100001

	# tree = [[] for i in range(Pp)]
	del_tree = [ -1 for i in range(Pp)]

	pow_tab = []
	pow2 = 1

	for i in range(17):
		pow_tab.append(pow2)
		pow2 *= 2

	# powala z 10^5
	# anc_tab = [-1 for i in range(18) ]

	tree_anc = [ [-1 for x in range(17)] for i in range(Pp) ]

	for i in range(p):
		edge = [int(x) for x in input().split()]
		# tree[edge[1]].append(edge[0])
		tree_anc[edge[0]][0] = edge[1]
		del_tree[edge[0]] = 1
		del_tree[edge[1]] = 1	

	root = -1

	del_tree[0] = -1

	for i in range(Pp):
		if tree_anc[i][0] == 0:
			tree_anc[i][0] = -1
			root = i

	for i in range(1, 17):
		for j in range(1, Pp):
			if tree_anc[j][i-1] > 0: 
				tree_anc[j][i] = tree_anc[tree_anc[j][i-1]][i-1]
			# else:
				# break

	# for i in range(30):
		# print(tree_anc[i])

	# print(root)

	# wlasciwie to log^2 (n)
	# ...ale da sie ulepszyc
	def find(u, k):
		if k == 0:
			return u
		
		t = 1
		t2 = k
		lvl = 0
		while t2 > 1 and lvl < 16:
			lvl += 1
			t2 //= 2
			t = t*2

		if tree_anc[u][lvl] < 1:
			return 0

		return find(tree_anc[u][lvl], k-t)

	# print(find(5,1))
	q = int(input())

	for i in range(q):
		lst = [ int(x) for x in input().split() ]
		if lst[0] == 2:
			if del_tree[lst[1]] != -1 :
				print(find(lst[1], lst[2]))
			else:
				print(0)

		if lst[0] == 1:
			if del_tree[lst[1]] != -1:
				del_tree[lst[1]] = -1
			else:
				print(0)

		if lst[0] == 0:
			if del_tree[lst[1]] > 0:

				v = lst[2]
				tree_anc[v][0] = lst[1]
				del_tree[v] = 1
				for j in range(1, 17):
					if tree_anc[v][j-1] > 0: 
						tree_anc[v][j] = tree_anc[tree_anc[v][j-1]][j-1]
					else:
						break
					# print(tree_anc[v][j], v, j)
			else:
				print(0)



t = int(input())

for z in range(t):
	solve()
#spamietywanie !!!!