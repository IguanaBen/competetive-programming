#include <iostream>

using namespace std;


const int num = 100001;

int find(int u, int k, int tree_anc[100001][18] )
{
	if(k ==0 )
		return u;

	int kp = k;
	int km = 1;
	int lvl = 0;
	while( kp > 1 && lvl < 17)
	{
		lvl++;
		km = km<<1;
		kp = kp>>1;
	}

	// cout<<km<<" "<<kp<<"\n";

	// cout<<<<" "<<k-km<<"\n";
	if(tree_anc[u][lvl] < 0)
		return 0;


	return find(tree_anc[u][lvl], k-km, tree_anc);
}

int tree_anc[100001][18];
bool tree_del[100001];

void solve()
{
	for(int i = 0; i< 100001; i++)
		tree_del[i] = true;

	for(int i= 0; i< 18; i++)
		for(int j = 0; j< 100001; j++)
			tree_anc[j][i] = -1;

	int p;
	cin>>p;

	int x, y;
	for(int a=0; a<p; a++)
	{
		cin>>x>>y;
		tree_del[x] = false;
		tree_del[y] = false;

		tree_anc[x][0] = y;

		if ( y == 0)
			tree_anc[x][0] = -1;
	}

	for( int i=1; i<18; i++)
		for(int j= 1; j< num; j++)
		{
			// cout<<tree_anc[j][i-1]<<" "<<i-1<<"\n";
			if(tree_anc[j][i-1] > 0)
				tree_anc[j][i] = tree_anc[tree_anc[j][i-1]][i-1];
		}

	// cout<<"asd";
	int q;
	cin>>q;

	int a, b, c;
	for(int z=0; z<q; z++)
	{
		cin>>a;
		if(a == 2)
		{
			cin>>b>>c;

			if(tree_del[b] == false)
			{
				// cout<<b<<" "<<c<<"\n";
				cout<<find(b, c, tree_anc)<<"\n";
				// cout<<endl<<endl;
			}
			else
				cout<<0<<"\n";

		}

		if(a==1)
		{
			cin>>b;
			tree_del[b] = true;
			for(int i=0; i<18;i++)
				tree_anc[b][i] = -1;
		}

		if(a==0)
		{
			cin>>b>>c;

			tree_anc[c][0] = b;
			tree_del[c] = false;
			for (int x= 1; x< 18; x++)
			{
				if(tree_anc[c][x-1] > 0)
					tree_anc[c][x] = tree_anc[tree_anc[c][x-1]][x-1];
			}
		}

	}
}


int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int t;
	cin>>t;
	for(int a=0; a<t; a++)
		solve();
}