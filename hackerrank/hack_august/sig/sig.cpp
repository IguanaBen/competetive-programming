#include <iostream>
#include <algorithm>

using namespace std;

#define type long long

const int last_lvl = 131072;

const type L_end = -3000000000;
const type R_end = 3000000000;

void merge(pair<type, int> *a1, int s1,
			pair<type, int> *a2, int s2, pair<type, int> *res_array)
{



	int i1 = 0;
	int i2 = 0;
	int ir = 0;

	while(i1 < s1 && i2 < s2)
	{
		if(a1[i1].second <= a2[i2].second)
		{
			res_array[ir] = a1[i1];
			i1++;
		}
		else
		{
			res_array[ir] = a2[i2];
			i2++;
		}

		ir++;
	}

	while(i1 < s1)
	{
		res_array[ir] = a1[i1];
		i1++;
		ir++;
	}

	while(i2 < s2)
	{
		res_array[ir] = a2[i2];
		i2++;
		ir++;
	}

}

struct node
{
	int array_size;
	pair<type, int> *array;
	type *array_sums;

	node()
	{
		array_size = 0;
	}

	void set_leaf(int index, type val, int leaf_pos)
	{
		array = new pair<type, int>[1];
		array_sums = new type[1];

		array[0].second = index;
		array[0].first = val;

		if(leaf_pos == 0)
			array_sums[0] = R_end - val;

		else
			array_sums[0] = val - L_end;

		array_size = 1;
	}

	// n1-left, n2-right
	void update(node * n1, node *n2, int leaf_pos)
	{
		array_size = n1->array_size + n2->array_size;
		array = new pair<type, int>[array_size];
		array_sums = new type[array_size];

		// takze przy zapytaniu!
		if( n1->array_size > 0 && n2->array_size > 0)
			merge(n1->array, n1->array_size , n2->array, n2->array_size, array);
		else
			if(n1->array_size > 0)
				array = n1->array;
			else
				array = n2->array;

		if(leaf_pos == 0)
		{
			type sum = 0;
			for(int i=0; i<array_size; i++)
			{
				sum += R_end - array[i].first;
				array_sums[i] = sum;
			}
		}
		else
		{
			type sum = 0;
			for(int i=0; i<array_size; i++)
			{
				sum += array[i].first - L_end;
				array_sums[i] = sum;
			}
		}

	}

};

node treeX[262145];
node treeY[262145];

pair<type, int> pointsX[100042];
pair<type, int> pointsY[100042];

bool cmp_pair (pair<type, int> a, pair<type, int> b) 
{
	return a.first < b.first;
}

void initTree(node * tree, pair<type, int> * points, int num_points)
{
	int index = last_lvl;
	for(int i=0; i<num_points;i++)
	{
		tree[index].set_leaf(points[i].second, points[i].first, index % 2 );
		index++;
	}

	index = last_lvl - 1;

	while(index > 1)
	{
		tree[index].update(&tree[index<<1], &tree[(index<<1) + 1], index % 2 );
		index--;
	}
}


struct cmp_class
{
    bool operator() (  pair<type, int> const& left,  pair<type, int>  const& right)
    {
        return left.second < right.second;
    }
};

pair<type, int> get_sum_n(int i, int j, pair<type, int> *tab, int size, type * array_sums)
{

	if(size == 0)
		return make_pair(0,0);

	if(i > tab[size-1].second && j < tab[0].second)
		make_pair(0, 0);
	else
	{

		// for(int a=0; a<size;a++)
		// {
		// 	cout<<tab[a].first<<","<<tab[a].second<<" ";
		// }

		// cout<<endl;

		// na pewno dziala lower bound -find:0 , array: 4 5 10?
		int h_ind = lower_bound(tab, tab + size, make_pair(0, j), cmp_class()) - tab;
		int l_ind = upper_bound(tab, tab + size, make_pair(0, i), cmp_class()) - tab;

		if(h_ind > size-1)
			h_ind = size-1;


		if(l_ind > 0)
			if(tab[l_ind-1].second == i)
				l_ind--;

		if(h_ind > 0)
			if(tab[h_ind].second > j)
				h_ind--;

		if(tab[l_ind].second > j)
			return make_pair(0,0);

		// cout<<l_ind<<"|"<<h_ind<<endl;


		//array sums!!!!!!!
		type sum = array_sums[h_ind];
		if(l_ind > 0)
			sum -= array_sums[l_ind-1];

		return make_pair(sum, h_ind - l_ind +1);
	} 
}
// int lvl = 0;

pair<type, type> query(int i, int j, int s_ind, int node_ind, node* tree, type &sum)
{
	// int cop = lvl;
	// lvl++;
	if(node_ind >= last_lvl)
	{

		type dist_l = tree[node_ind].array[0].first - L_end;
		type dist_r = R_end - tree[node_ind].array[0].first;
		return make_pair(dist_l, dist_r);
	}

	// type t_s = 0;
	// int t_n = 0;


	pair<type, int> res_l = get_sum_n(i, j, tree[node_ind<<1].array, 
		tree[node_ind<<1].array_size, tree[node_ind<<1].array_sums );

	if(s_ind <= res_l.second)
	{
		pair<type, int> res_r
		 = get_sum_n(i, j, tree[(node_ind<<1) + 1].array, 
		 	tree[(node_ind<<1) + 1].array_size, tree[(node_ind<<1) + 1].array_sums);

		 // cout<<"L";
		 pair<type, type> result = query(i, j, s_ind, (node_ind<<1), tree, sum);

		 // cout<<  res_r.first - res_r.second*result.first <<"SDS"<<endl;
		 type mn = res_r.first - res_r.second*result.first;
		
		 sum += mn;

		 return result;
	}
	else
	{
		// cout<<"R";
		pair<type, type> result = query(i, j, s_ind - res_l.second , (node_ind<<1) + 1,
		 tree, sum);

		type mn = res_l.first - res_l.second*result.second;

		sum += mn;

		return result;

	}



}



type do_query(int i, int j, node *tree)
{
	type sum = 0;
	query(i, j, (j-i)/2 + 1, 1,  tree, sum);
	return sum;
}

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);
	int n, q;
	cin>>n>>q;

	for(int i=0; i<n;i++)
	{
		cin>>pointsX[i].first;
		pointsX[i].second = i+1;
	}

	for(int i=0; i<n;i++)
	{
		cin>>pointsY[i].first;
		pointsY[i].second = i+1;

		type xp = pointsX[i].first - pointsY[i].first;
		type yp = pointsX[i].first + pointsY[i].first;

		pointsX[i].first = xp;
		pointsY[i].first = yp;
	}

	sort(pointsX, pointsX + n, cmp_pair);
	sort(pointsY, pointsY + n, cmp_pair);
	initTree(treeX, pointsX, n);
	initTree(treeY, pointsY, n);

	int node_in = 4096*2*2*2*2*2;

	// for(int i=0; i < treeY[node_in].array_size; i++)
	// {
	// 	cout<<treeY[node_in].array[i].first<<"|"<<treeX[node_in].array[i].second<<"   ";
	// }

	// cout<<endl;

	int a,b;
	for(int i=0;i<q;i++)
	{
		cin>>a>>b;
		type s1 = 0, s2 = 0;
		s1 = do_query(a, b, treeX);
		// cout<<s1<<endl;
		s2 = do_query(a, b, treeY);
		// cout<<s1<<" "<<s2<<"\n";
		type res = s1+s2;
		if(res % 2)
			cout<<res/2<<".5\n";
		else
			cout<<res/2<<".0\n";
		// cout<<endl<<endl;
	}




}