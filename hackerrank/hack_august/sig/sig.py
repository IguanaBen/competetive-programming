
arr = [ int(x) for x in input().split() ]

import random;

n = arr[0]
q = arr[1]

pointsX = [ [0, i+1] for i in range(n)]
pointsY = [ [0, i+1] for i in range(n)]


# tree = [ [0 for i in range(131072)] for j in range(18) ] 
# tree_m = [ [0 for i in range(131072)] for j in range(18) ]


def merge(tab1, tab2):
	result = []
	i1, i2 = 0, 0
	while i1 < len(tab1) and i2 < len(tab2):
		 if tab1[i1][0] <= tab2[i2][0]:
		 	result.append(tab1[i1])
		 	i1 += 1
		 else:
		 	result.append(tab2[i2])
		 	i2 += 1

	result += tab1[i1:]
	result += tab2[i2:]

	return result

class node():
	array = []
	array_sums = []
	arr_size = 0

	def update(self, node_l, node_r, leaf_pos):

		self.array  = merge(node_l.array, node_r.array)

		self.arr_size = node_l.arr_size	+ node_r.arr_size		

		self.array_sums = [0 for i in range(self.arr_size)]
		
		if leaf_pos == 0:
			sm = 0
			for i in range(self.arr_size):
				sm += R_end - self.array[i][1]
				self.array_sums[i] = sm 
		else:
			sm = 0
			for i in range(self.arr_size):
				sm += self.array[i][1] - L_end
				self.array_sums[i] = sm 

		return

	def set_leaf(self, val, index, leaf_pos):

		if leaf_pos == 0:	# val - distance to right side
			self.array = [[index, val]]
			self.array_sums = [R_end - val] 
		else:
			self.array = [[index, val]]
			self.array_sums = [val - L_end]

		self.arr_size = 1
		return 
	pass


last_level = 131072

treeX = [ node() for i in range(131072*2+1) ]
treeY = [ node() for i in range(131072*2+1) ]


# 3 * 10^9
L_end = -3*1000000000
R_end = -L_end

# sin45 = 0.7071067811865475244008443621048490392848359376884740
# cos45 = 0.7071067811865475244008443621048490392848359376884740

# sc45 = sin45

ind = 0
for i in input().split():
	pointsX[ind][0] = int(i)
	ind += 1

ind = 0
for i in input().split():
	pointsY[ind][0] = int(i)
	ind += 1

for k in range(n):
	xp = ( pointsX[k][0] - pointsY[k][0] )
	yp = ( pointsX[k][0] + pointsY[k][0] )
	pointsX[k][0] = xp
	pointsY[k][0] = yp
	# print(xp, yp)

pointsX.sort()
pointsY.sort()




def init_tree(tree, points):

	ind = last_level
	for elem in points:
		tree[ind].set_leaf(elem[0], elem[1], ind % 2)
		ind += 1

	ind = last_level - 1

	while ind > 1:
		tree[ind].update(tree[ind*2], tree[ind*2 + 1], ind % 2)
		ind -= 1

init_tree(treeX, pointsX)
init_tree(treeY, pointsY)


def upper_bound(elem, i1, i2, tab):

	if i1==i2-1:
		if elem < tab[i2][0]:
			return i1
		else:
			return i2

	pivot = (i2-i1) // 2 + i1
	if tab[pivot][0] >= elem:
		return upper_bound(elem, i1, pivot, tab)
	else:
		return upper_bound(elem, pivot+1, i2, tab)

def lower_bound(elem, i1, i2, tab):

	if i1==i2-1:
		if elem <= tab[i1][0]:
			return i1
		else:
			return i2

	pivot = (i2-i1) // 2 + i1
	if tab[pivot][0] <= elem:
		return lower_bound(elem, pivot, i2, tab)
	else:
		return lower_bound(elem, i1, pivot-1, tab)

def get_sum_and_n(tab, tab_l , i, j):

	if j < tab[0] or i > tab[1]:
		return [0, -1]
	else:
		i1 = upper_bound(i, 0, tab_l, ) 

def find_med_sum(i, j, k, sum_l, q_l, sum_r, q_r, node, tree):

	if node > last_level:
		dist_l = tree[node][0][1] - L_end
		dist_r = R_end - tree[node][0][1]
		sum_l-= dist_l*q_l
		sum_r-= dist_r*q_r

		return sum_l+sum_r
	else:
		sum_l_n = 0
		sum_r_n = 0



def query(i, j):
	X_val = find_med_sum(i, j, (j-i)//2 , 0, 0, 0, 0, 1, treeX)
	Y_val = find_med_sum(i, j, (j-i)//2 , 0, 0, 0, 0, 1, treeY)



# print(treeY[4096*2*2*2*2+1].array)

tab1 = [ [1, 0], [3, 0] , [5, 0],  [7, 0], [9, 0], [12, 0], [13, 0]]
# print(upper_bound(13, 0, len(tab1)-1, tab1))
# print(lower_bound(13, 0, len(tab1), tab1))


print(treeX[2].array_sums)

# print(pointsX)
# print(pointsY)




# print(100000, 1)

# outstr1 = ""
# outstr2 = ""

# for i in range(100000):
	# outstr1 += str(random.randint(-1123, 1000000002)) + " "

# for i in range(100000):
	# outstr2 += str(random.randint(-1123, 1000000002)) + " "

# print(outstr1)
# print(outstr1)

