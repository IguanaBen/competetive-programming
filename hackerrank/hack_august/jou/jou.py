
n_k = input()
n_k_a = [int(x) for x in n_k.split() ]
n = n_k_a[0]
k = n_k_a[1]

pairs = [(0,0) for i in range(k)]

for i in range(k):
	inp = input()
	tab = [int(x) for x in inp.split()]
	pairs[i] = (tab[0], tab[1])

sets = [i for i in range(n)]
count = [1 for i in range(n)]

def find(elem):
	if sets[elem] == elem:
		return elem

	found = find(sets[elem])
	# print(elem)
	sets[elem] = found
	return found

def union(x, y):

	found_x = find(x)
	found_y = find(y)

	if found_x == found_y:
		return False

	if count[found_x] > count[found_y]:
		count[found_x] += count[found_y]
		sets[found_y] = found_x
	else:
		count[found_y] += count[found_x]
		sets[found_x] = found_y

	return True


for i in range(k):
	# print(pairs[i][0], pairs[i][1])
	union(pairs[i][0], pairs[i][1])


countrys = [0 for i in range(n)]

for i in range(n):
	if sets[i] == i:
		countrys[i] = count[i]
		# print(countrys[i])

result = 0
summ = 0 # prefix sum
for i in range(n):
	summ += countrys[i]
	result += (n-summ)*countrys[i]

print(result)




