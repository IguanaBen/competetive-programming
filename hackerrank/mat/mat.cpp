#include <iostream>
#include <vector>
#include <algorithm>

#define type long long

using namespace std;

vector< pair<int, type> > edges[100042];
bool visited[100042];
bool marked[100042];


type removed_sum = 0;

type solve(int v)
{
	visited[v] = true;
	//cout<<"visiting: "<<v;
	if( marked[v] )
	{
		//cout<<" marked "<<endl;
		for(int u = 0; u < edges[v].size(); u++)
		{
			if(!visited[edges[v][u].first])
			{
				type r = solve(edges[v][u].first);

				//if exist marked node in subtree
				if( r > -1 )
				{
					r  = min( r, edges[v][u].second);
					removed_sum += r;
				}
			}
		}

		//infty
		return 999999999;

	}
	else
	{
		//cout<<" unmarked "<<endl;
		int marked_n = 0;
		type mx = 0;
		int t_sum = 0;

		for(int u = 0; u < edges[v].size(); u++)
		{
			if(!visited[edges[v][u].first])
			{

				type r = solve(edges[v][u].first);
				//if exist marked node in subtree
				if( r > -1 )
				{
					r  = min(r, edges[v][u].second);
					t_sum += r;
					marked_n++;
					mx = max(mx, r);
				}
			}
		}


		if(marked_n > 0)
		{
			t_sum -= mx;
			removed_sum += t_sum;
			return mx;
		}
		else
			return -1;


	}
}

int main()
{
	int n, k;
	cin>>n>>k;

	int x, y;
	type c;
	for(int i = 0; i < n-1; i++ )
	{
		cin>>x>>y>>c;
		edges[x].push_back( make_pair(y, c) );
		edges[y].push_back( make_pair(x, c) );
	}

	int m = 0;
	for(int j = 0; j < k; j++)
	{
		cin>>m;
		marked[m] = true;
	}


	solve(0);
	cout<<removed_sum<<"\n";

	return 0;
}