#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

int ch[256];

int abs(int r)
{
	if( r < 0)
		return -r;
	return r;
}

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);
    string s;
    int n;
    cin>>n;

    for(int a =0; a<n; a++)
    {
    	for(int b =0; b<256;b++)
    		ch[b] = 0;

    	cin>>s;
    	if(s.length() % 2 )
    	{
    		cout<<-1<<"\n";
    	}
    	else
    	{
    		for(int z = 0; z < s.length()/2 ; z++)
    		{
    			ch[s[z] ]++;
    		}

    		for(int z = s.length()/2; z < s.length(); z++)
    		{
    			ch[s[z] ]--;
    		}

    		int result = 0;

    		for(int w = 0; w< 255; w++)
    		{
    			result+= abs(ch[w]);
    		}

    		cout << result/2 <<"\n";

    	}
    }
    return 0;
}
