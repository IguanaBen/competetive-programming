#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <math.h>

#define type unsigned long long

using namespace std;

type p2(type n)
{
	if(n == 1)
		return 1;
	return 2*p2(n/2);
}
bool solve(type n)
{
	if(n == 1)
		return 0;

	if( (n&(n-1)) == 0)
		return !solve(n/2);
	else
		return !solve(n-p2(n));
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	int t;
	cin>>t;
	while(t--)
	{
		type n;
		cin>>n;
		if(!solve(n))
			cout<<"Richard\n";
		else
			cout<<"Louise\n";
	}
	return 0;
}