s2 = [0 for i in range(270)]
s = str(input())

for l in s:
	s2[ord(l)]+=1

c = 0;
for i in range(270):
	if s2[i] % 2 == 1:
		c+=1

if c > 1:
	print("NO")
else:
	print("YES")
