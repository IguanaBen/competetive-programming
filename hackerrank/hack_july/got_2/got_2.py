def fact_mod_p_brute(n, p):
	
	if n == 0:
		return 1

	res = 1
	for i in range(1, n+1):
		res *= i
		res = res % p
	return res

s2 = [0 for i in range(270)]
s = str(input())

for l in s:
	s2[ord(l)]+=1

c = 0;
sum = 0 # could me length div 2
for i in range(270):
	if s2[i] % 2 == 1:
		c+=1
	s2[i] //= 2
	sum += s2[i]

pp = 1000000007
if c > 1:
	print("0")
else:
	licz = fact_mod_p_brute(sum,pp)
	# print(licz)
	mian = 1
	for i in range(270):
		mian *= fact_mod_p_brute(s2[i], pp)
		mian = mian % pp
	print ( (licz * pow(mian,pp-2,pp)) % pp )