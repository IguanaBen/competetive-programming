
def exp_mod(n, x, p):
	if n == 0:
		return 1

	t = exp_mod(n//2, x, p)

	if n % 2 == 1:
		return (t*t*x) % p
	else:
		return (t*t) % p

def exp_mod_f(n, x, p):
	if n == 0:
		return 1
		
	t = exp_mod_f(n//2, x, p)
	res = 1
	if n % 2 == 1:
		res =  t*t*x
	else:
		res = t*t
	while res > p:
		res //= 10
	return res

t = int(input())
for i in range(t):
	n_l = input()
	a = [int(x) for x in n_l.split()]
	# t = int(input())
	z0 = 10**(a[1])
	z2 = 10**(a[1]+140)
	# print(z, z2)
	l = exp_mod(a[0]-1, 2, z0)
	f = exp_mod_f(a[0]-1, 2, z2)
	# f = int((a[0]-1) * c)
	while l > z0:
		l //= 10
		# print(l)
	while f >= z0:
		f //= 10
		# print(f)

	# print(l, f)
	print(l + f)