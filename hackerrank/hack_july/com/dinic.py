
# 	2(1)	2(5)	4(9)
# 0	5(2)	5(6)	9(10)	13
# 	6(3)	3(7)	10(11)
# 	7(4)	7(8)	12(12)
from collections import deque
graph = [
			[1,2,3,4],	#0
			[5],		#1
			[6],		#2
			[5, 7],		#3
			[8],		#4
			[9, 10, 12],#5
			[11],		#6
			[10],		#7
			[],			#8
			[13],
			[13],
			[13],
			[13],
			[]
		]
graph2 = [[1, 2, 3, 4], [5], [7], [5, 6], [8], [9, 11, 12], [10, 12], [], [], [13], [13], [13], [13], []]

graph3 = [[1, 2, 3, 4], [6], [6], [6], [5], [10], [7, 8, 9, 10], [11], [11], [11], [11], []]





def level(graph, iter, levels, visited):
	qjuju = deque()
	qjuju.append((0,0))
	levels[0] = 0
	visited[0] = iter
	while qjuju:
		(v, l) = qjuju.pop()
		# print (v)
		levels[v] = l
		for i in range( len(graph[v]) ):
			if  visited[graph[v][i]] != iter:
				visited[graph[v][i]] = iter
				qjuju.append((graph[v][i], l+1))

	if visited[len(graph) - 1] == iter:
		return levels[len(graph)-1]
	else:
		return 0


# could be done better
def block_flow(v, graph, lvl, levels):
	if levels[v] == lvl:
		return 1
	else:
		sum = 0
		for i in range( len(graph[v]) ):
			if levels[v] + 1 == levels[graph[v][i-sum]]:
				if block_flow(graph[v][i-sum], graph, lvl, levels) > 0:
					t = graph[v][i-sum]
					del graph[v][i-sum]
					graph[t].append(v)
					sum = sum + 1
					if v != 0:
						return 1
		return sum



def dinic(graph):
	levels =  [ 0 for i in range( len(graph) + 1)] 
	visited =  [ 0 for i in range( len(graph) + 1)] 
	iter = 1
	l = level(graph, iter, levels, visited) 
	flow = 0
	while l > 0:
		iter = iter + 1
		# print(graph, l)
		flow = flow + block_flow(0, graph, l, levels)
		l = level(graph, iter, levels, visited) 
		# print(graph, l)

	return flow

print(dinic(graph3))





