

from collections import deque


max = 31623
# max = 20
primes = [2]

for i in range(3, max+1):
	p = 1
	for j in range(0, len(primes)):
		if i % primes[j] == 0:
			p = 0
			break
	if p != 0:
		primes.append(i)

# pre-primes < 10^6?
# print(len(primes))


n = int(input())
# n_l_1 = [2, 5, 6, 7]
# n_l_2 = [4, 9, 5*2, 12]
n_l = input()
n_l_1 = [int(x) for x in n_l.split()]
n_l = input()
n_l_2 = [int(x) for x in n_l.split()]
# n_l_1 = [3, 3, 3, 2]
# n_l_2 = [3, 3, 3, 6]




def gen_fact(factors_list, list, pri_list, pri_ind ):
	for i in range(n):
		t = list[i]
		index = 0
		while t > 1 and index < len(primes) :
			if t % primes[index] == 0:
				factors_list[i].append(index)
				pri_list[index] +=1
				while t % primes[index] == 0:
					t /= primes[index]
			index += 1
		if t > 1:
			primes.append(t)
			factors_list[i].append( len(primes) - 1)
			pri_list.append(1)
			pri_ind.append(-1)


# jesli jakas liczba pierwsza l | x w list_2 
# a nie wystepuje w zadnej rozkaldzie
# zadnej liczby w list_1 to ja pomijamy
def prime_edges(factors_list, list):
	for i in range(n):
		t = list[i]
		index = 0
		while t > 1 and index < len(primes) :
			if t % primes[index] == 0:
				factors_list[index].append(i)
				while t % primes[index] == 0:
					t /= primes[index]
			index += 1

# gen_fact(factors_1, n_l_1)
# prime_edges(factors_2, n_l_2)


def make_graph(n_l_1, n_l_2, n):
	
	pri_index = [ -1 for i in range(len(primes))]
	pri_list = [ 0 for i in range(len(primes))]
	# print(len(pri_list))

	graph = [[]]

	factors_1 = [ [] for i in range(n)]
	factors_2 = [ [] for i in range(len(primes))]
	# factors_3 = [ [] for i in range(n)]

	gen_fact(factors_1, n_l_1, pri_list, pri_index)
	prime_edges(factors_2, n_l_2)
	# print(len(primes))
	# print(len(pri_list))

	# print(factors_2)

	for i in range(n):
		graph[0].append(i+1)
	
	index_pri = 0
	for i in range(len(primes)):
		if pri_list[i] > 0:
			pri_index[i] = index_pri
			index_pri += 1

	for i in range(len(factors_1)):
		graph.append([])
		for j in range(len(factors_1[i])):
			if pri_index[factors_1[i][j]] != -1 :
				graph[i+1].append(pri_index[factors_1[i][j]] + n + 1)

	# print(factors_2)
	# print(graph)

	l2 = len(graph)
	l10 = len (factors_2)
	ind = 0
	for i in range( len (factors_2) ):
		if pri_list[i] > 0:
			# print(primes[i])
			graph.append([]) 
			for j in range(len(factors_2[i])):
				graph[l2 + ind].append(l2 + factors_2[i][j] + index_pri)
			ind += 1

	l3 = len(graph)
	for i in range( n ):
		graph.append([l3 + n])

	graph.append([])

	# print(graph)
	return graph


def level(graph, iter, levels, visited):
	qjuju = deque()
	qjuju.append((0,0))
	levels[0] = 0
	visited[0] = iter
	while qjuju:
		(v, l) = qjuju.pop()
		# print (v)
		levels[v] = l
		for i in range( len(graph[v]) ):
			if  visited[graph[v][i]] != iter:
				visited[graph[v][i]] = iter
				qjuju.append((graph[v][i], l+1))

	if visited[len(graph) - 1] == iter:
		return levels[len(graph)-1]
	else:
		return 0


# could be done better
def block_flow(v, graph, lvl, levels):
	if levels[v] == lvl:
		return 1
	else:
		sum = 0
		for i in range( len(graph[v]) ):
			if levels[v] + 1 == levels[graph[v][i-sum]]:
				if block_flow(graph[v][i-sum], graph, lvl, levels) > 0:
					t = graph[v][i-sum]
					del graph[v][i-sum]
					graph[t].append(v)
					sum = sum + 1
					if v != 0:
						return 1
		return sum



def dinic(graph):
	levels =  [ 0 for i in range( len(graph) + 1)] 
	visited =  [ 0 for i in range( len(graph) + 1)] 
	iter = 1
	l = level(graph, iter, levels, visited) 
	flow = 0
	while l > 0:
		iter = iter + 1
		# print(graph, l)
		flow = flow + block_flow(0, graph, l, levels)
		l = level(graph, iter, levels, visited) 
		# print(graph, l)

	return flow


graph  = make_graph(n_l_1, n_l_2, n)
# print(graph)
print(dinic(graph))
# print(primes)
# print(factors_1)
# print(factors_2)

