#include <iostream>
#include <algorithm>

using namespace std;

int n_tree;


//rozmiar drzewa,
/// najbardziej prawy wezel(max element) - juz nie, nie oszczedzam na przebiegach
//dodajac min element(najbardziej lewy wezel) mozna zaoszczedzic
//jeden przebieg(czyli zejscie w dol drzewa)
int tree[700042];
int root = 1;
int max_lvl = 262144*2; //2^18 * 2

void add(int x)
{
	tree[max_lvl + x]++;
	int c_index =  max_lvl + x;

	c_index = c_index/2;

	while(c_index != 0)
	{
		tree[c_index] = tree[2*c_index] + tree[2*c_index + 1];
		c_index = c_index/2;
	}
}

bool remove(int x)
{
	if(tree[max_lvl + x] < 1)
		return false;
	//cout<<"rem "<<x<<" "<< tree[max_lvl + x] <<endl;

	tree[max_lvl + x]--;
	int c_index =  max_lvl + x;

	c_index = c_index/2;

	while(c_index != 0)
	{
		tree[c_index] = tree[2*c_index] + tree[2*c_index + 1];
		c_index = c_index/2;
	}
	return true;
}

//chyba nie dzialaja, tzn. get_right i get_left
int get_left(int r)
{
	while( r*2 < max_lvl)
	{
		if( tree[r*2] > 0 )
			r = r*2;
		else
			r = r*2 + 1;
	}

	return tree[r];
}

int get_right(int r)
{
	while( r*2 < max_lvl)
	{
		if( tree[r*2 + 1] > 0 )
			r = r*2 + 1;
		else
			r = r*2;
	}
	return tree[r];
}

//znajduje x-ty element
int find_elem(int x, int node)
{
	if( node < max_lvl)
	{
		int l_size = tree[node*2];
		int r_size = tree[node*2 + 1];
		//cout<<x<<" "<<l_size<<" "<<r_size<<" "<<node<<endl;

		if(x <= l_size)
			return find_elem(x, node*2);
		else
			return find_elem(x - l_size, node*2 + 1);
	}
	else
		return node - max_lvl;


}

long long int val[100042];

//super nazwa...
int new_cmd_elem[100042];


void print_median()
{
	int n_elem = tree[root];
	//cout<<n_elem<<endl;

	if(n_elem == 0)
	{
		cout<<"Wrong!\n";
		return;
	}

	if(n_elem % 2)
		cout<<val[find_elem(n_elem/2 + 1, root)]<<"\n";
	else
	{
		long long int res = val[find_elem(n_elem/2, root)] + val[find_elem(n_elem/2+1, root)];

		if(res == -1)
			cout<<"-0.5\n";
		else
			if(res % 2)
				cout<<res/2<<".5\n";
			else
				cout<<res/2<<"\n";
	}
}


bool cmp(const  pair<int, int>  p1, const pair<int, int> p2)
{
	return p1.second < p2.second;
}

int main()
{
	for(int i = 0; i< max_lvl+1;i++ )
		tree[i] = 0;

	int n;
	cin>>n;
	char cmd[n];
	pair<int,int> elem[n];
	for(int a = 0; a<n; a++)
	{
		cin>>cmd[a]>>elem[a].second;
		elem[a].first = a;
		
		/*if(cmd == 'r')
			if(remove(elem))
				print_median(); 
			else
				cout<<"Wrong!\n";
		else
		{
			add(elem);
			print_median();
		}*/
	}

	sort(elem, elem+n, cmp);
	/*for(int z =0 ;z < n; z++)
		cout<<elem[z].second<<" ";*/

	//cout<<endl;
	long long int prev  = elem[0].second-4;
	int c_i = 0;
	for(int i = 0; i < n; i++)
	{
		//cout<<elem[i].first<<" "<<elem[i].second<<endl;
		if(elem[i].second != prev)
		{
			c_i++;
			//cout<<"B:"<<elem[i].second<<endl;
			//new_cmd_elem[elem[i].first] = c_i;
			val[c_i] = elem[i].second;
			prev = elem[i].second;
		}
		new_cmd_elem[elem[i].first] = c_i;

	}
	
	/*for(int z = 1;z <= 8; z++)
		cout<<val[z]<<" ";
	cout<<endl;*/

	for(int a = 0; a<n; a++)
	{
		char cm = cmd[a];
		int e = new_cmd_elem[a];
		//cout<<cm<<" "<<e<<endl;
		if(cmd[a] == 'r')
			if(remove(new_cmd_elem[a]))
				print_median(); 
			else
				cout<<"Wrong!\n";
		else
		{
			add(new_cmd_elem[a]);
			print_median();
		}
	}


	return 0;
}