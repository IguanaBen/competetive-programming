
inp = [x for x in input().split() ]

k = int(inp[0])
x = int(inp[1])


def func(x):
	if x < k:
		return 1

	if x % k == 0:
		return func(x-k) + func(x // k)

	return x - ( x % k)

print(func(x))