#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int t;
  type k;
  cin>>t;
  for(int i=0; i<t; i++)
  {
  	cin>>k;

  	if( k % 2)
  	{
  		type res =  (k/2) * (k/2 + 1);
  		cout<<res<<"\n";
  	}
  	else
  		cout<<(k/2)*(k/2)<<"\n";

  }

  return 0;
 
}



