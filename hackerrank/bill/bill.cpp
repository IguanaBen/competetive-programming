#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <set>

using namespace std;

#define type long long;

// 0-max_empty, 1-max_full
type max_val[100033][2];
type billboards[100033];

int main()
{
	int n,k;

	cin>>n>>k;

	for(int i=0; i<100033; i++)
		cin>>billboards[i];

	multiset<type> mset;

	max_val[0][0] = 0;
	max_val[0][1] = billboards[0];

	mset.insert(0);

	for(int i=1; i<k; i++)
	{
		max_val[i][0] = max(max_val[i-1][0], max_val[i-1][1]);
		max_val[i][1] = max(max_val[i-1][0], max_val[i-1][1]) + billboards[i];

		mset.insert(max_val[i][0]);
	}

	

    return 0;
}
