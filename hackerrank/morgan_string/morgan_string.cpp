#include <iostream>
#include <string>
#include <vector>

using namespace std;

#define type long long

const type bigprime = 1000000007;
const type smallprime = 1000003;

const int mxlen = 300033;

type hashes[mxlen];
type powers[mxlen];

string hashed_str;

void generate_hash(string str)
{
    hashed_str = str;
    powers[0] = 1;
    
    for(int i=1; i<str.length() + 2; i++) 
        powers[i] = (powers[i-1]*smallprime) % bigprime;

    hashes[1] = str[0];
    for(int i=1; i<str.length(); i++)
        hashes[i+1] = (hashes[i]*smallprime + str[i]) % bigprime;
}

int get_hash(int i, int j) // inclusive
{
    if( i > j) return 0;
    // cout<<powers[j-i+1]<<" ||\n";
    return (hashes[j+1] - (hashes[i]*powers[j-i + 1]) % bigprime + bigprime) % bigprime;
}

int lcp(int i, int j) //i,j- positions in string
{
    int start = 0;
    int end = min(hashed_str.length() - i, hashed_str.length()-j);

    while(start != end)
    {

        if(start == end-1)
            if(get_hash(i, i+end-1) == get_hash(j, j+end-1))
                start = end;
            else
                end = start;

        int mid = (start + end)/2;
        if(get_hash(i, i+mid-1) == get_hash(j, j+mid-1))
            start = mid;
        else
            end = mid-1;
    }
    return start;
}

const int lgn = 20;
bool is_gray[mxlen+3][lgn];
bool is_gray_err[mxlen+3][lgn];
// type hash_gray_err[mxlen+3][lgn];
int change_index[mxlen+3][lgn];

int charcnt[mxlen+3][27];
type to_rem[mxlen+3];
type total = 0;

type tempstarts[mxlen+3];
type tempends[mxlen+3];

type changeDiff[mxlen+3][27];

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    // string ss = "aabaa";
    // generate_hash(ss);
    // cout<<get_hash(0,0)<<" "<<lcp(0,3)<<"\n";

    int n;
    cin>>n;
    while(n--)
    {
    	string s1, s2;
    	cin>>s1>>s2;
    	string sc = s1 + "|" + s2 + "|";
    	// cout<<sc<<"\n";
    	generate_hash(sc);
    	int i1=0,i2=s1.length()+1;
    	// cout<<i1<<" "<<i2<<"\n";
    	while(i1 < s1.length() && i2 < sc.length()-1)
    	{
    		// cout<<sc[i1]<<" "<<sc[i2]<<"\n";
    		if(sc[i1] == sc[i2])
    		{
    			int lc = lcp(i1, i2);
    			if(sc[i1+lc] < sc[i2+lc])
    			{
    				cout<<sc[i1];
    				i1++;
    			}
    			else
    			{
    				cout<<sc[i2];
    				i2++;
    			}
    		}
    		else
    		{
    			if(sc[i1] < sc[i2])
    			{
    				cout<<sc[i1];
    				i1++;
    			}
    			else
    			{
    				cout<<sc[i2];
    				i2++;
    			}
    		}
    	}

    	while(i1 < s1.length())
    	{
    		cout<<sc[i1];
    		i1++;
    	}

    	while(i2 < sc.length()-1)
    	{
    		cout<<sc[i2];
    		i2++;
    	}

    	cout<<"\n";

    }

    return 0;
} 