#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

#define type long long

type solve(int n, int k)
{
    type mod = n%k;
    type div  = n/k;
    type div_1 = div+1;
    type res = 0;
    type m1 = div*div;
    type m2 = div_1*div;
    type m3 = div_1*div_1;
    for(int i=1;i <(k/2) + (k %2); i++)
    {
        if(i <= mod && k-i <=mod)
            res += m3;
        
        if(i <= mod && k-i > mod)
            res += m2;
        
        if(i > mod && k-i > mod)
            res += m1;
    }

    res += (div *div - div)/2;
   
    
    if( k % 2 == 0)
    {
        type num = div;
        if( k/2 <= mod)
            num++;
        
       res += (num*num - num)/2;
    }
    
    return res;
}

int main()
{
    int t,n,k;
    cin>>t;
    
    for(int a=0; a<t; a++)
    {
        cin>>n>>k;
        cout<<solve(n,k)<<"\n";
    }
    
    return 0;
}
