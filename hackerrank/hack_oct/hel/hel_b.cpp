#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

long long  solve(int n, int k)
{
    long long res = 0;

    for(int i=1;i<=n;i++)
    {
        for(int j=1;j<i;j++)
        {
            if( (i+j) % k == 0)
                res++;
        }
    }

    return res;
}

int main()
{
    int t,n,k;
    cin>>t;
    
    for(int a=0; a<t; a++)
    {
        cin>>n>>k;
        cout<<solve(n,k)<<"\n";
    }
    
    return 0;
}
