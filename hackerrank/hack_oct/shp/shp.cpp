#include <iostream>
#include <string>
#include <algorithm>


//rozwazaj wszystkie podciagi o maksymalnej sumie

#define type long long

using namespace std;

type mxsstr[1000042][2];
type sum[1000042];

//max index which can be reached with i(array index) sum
//from pos 0
type max_ind[6000042];
bool touched[6000042];

type val(char c)
{
	if (c == '1')
		return 3;
	else 
		return -2;
}

type abst(type x)
{
	if(x <0)
		return -x;
	return x;
}

type solve(string str, int n)
{
	int index = str.length()-1;
	
	mxsstr[index][0] = val(str[index]);
	mxsstr[index][1] = index;

	int length =str.length();

	index = 2*str.length()-1;

	for(int i= index; i>=0 ;i--)
	{
		if(mxsstr[(i+1) % length][0] < 0)
		{
			mxsstr[i % length][1] = i % length;
			mxsstr[i % length][0] = val(str[i % length]);
		}
		else
		{
			mxsstr[i % length][1] = mxsstr[(i+1) % length][1]; 
			mxsstr[i % length][0] = mxsstr[(i+1) % length][0] + val(str[i % length]);
		}
	}

	sum[0] = val(str[0]);
	for(int i = 1; i<str.length(); i++)
		sum[i] = sum[i-1] + val(str[i]);

	int ar = 3000001;

	for(int i = str.length()-1; i>=0; i-- )
	{
		int ind = 0;
		if(sum[i] >= 0)
			ind = -sum[i] + ar;
		else
			ind = -sum[i] + ar;

		while( !touched[ind] && ind < 6000042)
		{
			touched[ind] = 1;
			max_ind[ind] = i;
			ind++;
		}
	}

	// for(int i=0;i<length;i++)
	// {
	// 	cout<<i<<" "<<mxsstr[i][0]<<" "<<mxsstr[i][1]<<" "<<sum[i]<<endl;
	// }
	// cout<<endl<<endl;
	// for(int i=-20;i<20;i++)
	// {
	// 	cout<<i<<" "<<max_ind[i+ar]<<endl;;
	// }

	// cout<<endl;

	if(sum[length-1] < 0)	
	{
		// cout<<"sdu";
		type max_length = 0;
		for(int i=0; i< str.length(); i++)
		{
			
			type seg_left = n-1;

			type clength = 0;
			type Val = mxsstr[i][0];
			type dv = 0;
			type dzz = sum[length-1]*(-1);
			if(Val >= 0)
				dv = Val/dzz;
			else
				dv = 0;

			dv = Val/dzz;
			
			
			type left = 0;

			left = Val % dzz ;

			if( dv > n-1)
			// LEFT OVERFLOW, nizej tez tablica
				left = Val - (n-1)*(-sum[length-1]);

			// cout<<Val<<" "<<left<<" "<<dv<<" "<<mxsstr[i][1]<<" ";

			if(mxsstr[i][1] < i)
				seg_left--;
			
			if(dv > seg_left)
				dv = seg_left;

			// cout<<Val<<" "<<dzz<<"ndv: "<<dv<<" "<<endl;
			if(mxsstr[i][0] >= 0)
			{
				clength = dv*length;
				// + abst(mxsstr[i][1]-i);
				if(mxsstr[i][1] >= i)
				{
					clength += abst(mxsstr[i][1]-i) + 1;
				}
				else
					clength += (length + mxsstr[i][1] + 1 - i);
				//zle
				// cout<<i<<" "<< -sum[mxsstr[i][1]] + left + ar<<endl;
				type r = 0;
				if(-sum[mxsstr[i][1]] + left  > 3000000 )
					r = max_ind[ 3000000] - mxsstr[i][1];
				else
					r = max_ind[ -sum[mxsstr[i][1]] + left + ar] - mxsstr[i][1];

				if(r > 0)
					clength += r;
			}
			if(clength > length*n - i)
				clength =  length*n - i;

			//cout<<clength<<endl;
			max_length = max(max_length, clength);

		}

		return max_length;
	}
	else
	{
		type max_length = n*length;
		type seg_left = n-1;
	/*	for(int i=0; i< str.length(); i++)
		{
			type clength = seg_left * length;
			type Val = sum[n-1] * seg_left;

			type r =0;
			cout<<Val<<" "<<-sum[i] + val(str[i]) + Val;
			if(-sum[i] + val(str[i]) + Val > 3000000)
				r = max_ind[ 3000000] - i + 1;
			else
				r = max_ind[ -sum[i] + val(str[i]) + Val + ar] - i + 1;
			cout<<" "<<r<<endl;
			// if( r > i )
			// cout<<r<<endl;

			if(r > 0)
				clength += r;

			if(clength > length*n - i)
				clength =  length*n - i;

			max_length = max(max_length, clength);
		}
*/
		return max_length;
	}
}

int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	type n;
	string str;
	cin>>n>>str;
	cout<<solve(str,n);



	return 0;
}