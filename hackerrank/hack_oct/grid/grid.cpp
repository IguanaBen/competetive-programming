#include <iostream>

#define type long long int

using namespace std;

const type md = 1000000007;
type C[3042][3042];
type gcd[3042][3042];

void genC()
{
	for(int i=0; i<3042;i++)
	{
		C[0][i] = 0;
		C[i][0] = 1;
	}

	for(int i=1; i<3042;i++)
		for(int j=1; j<3042;j++)
			if( j<=i)
				C[i][j] = (C[i-1][j-1] + C[i-1][j]) % md;
			else
				C[i][j] = 0;
}

void genGcd()
{
	for(int i=0; i<3042;i++)
	{
		gcd[i][0] = i;
		gcd[0][i] = i;
	}

	for(int i=1;i<3042;i++)
		for(int j=1; j<3042;j++)
		{	
			if( i > j)
				gcd[i][j] = gcd[i-j][j];
			else
				gcd[i][j] = gcd[i][j-i];
		}
}


type absynt(type x)
{
	if(x < 0)
		return x*(-1);
	return x;
}

type solve(int n, int m, int k)
{
	type tri = 0;
	for(int i=1; i<n; i++)
		for(int j=1; j<m; j++)
		{
			type gd = gcd[i][j];
			// cout<<i<<" "<<j<<" "<<gcd[i][j]<<endl;
			type res = (2*C[gd-1][k-1] + C[gd-1][k-2]) % md;
				// cout<<res<<endl;
			tri += res;
			tri =  tri % md;
		}

	tri = tri + tri;
	tri = tri % md;
	tri = tri + tri;
	tri =  tri % md;

	type gd = gcd[n][m];
	// cout<<gd<<" "<<C[gd-1][k-1]<<" "<<C[gd-2][k-2]<<endl;
	type diag = (2*C[gd-1][k-1] + C[gd-1][k-2]) % md;
	// diag*=2;
	diag = diag % md;

	type h = 0;

	// cout<<tri<<endl;
	for(int i=0; i<=n; i++)
		for(int j=0; j<=n; j++)
		{
			if( (i==0 && j==0) || (i==n && j == n))
			{

			}
			else
				if( absynt(i-j) !=0)
				{
					type gd = gcd[absynt(i-j)][m];
					type res = (2*C[gd-1][k-1] + C[gd-1][k-2]) % md;
					h += res;
					h = (h % md);
				}
				else
				{
					h += 2*C[m-1][k-1] + C[m-1][k-2];
					h = (h % md);
				}
		}

	type v = 0;

	for(int i=0; i<=m; i++)
		for(int j=0; j<=m; j++)
		{
			if( (i==0 && j==m) || (i==m && j==0) || (i==0 && j==0) || (i==m && j == m))
			{

			}
			else
				if( absynt(i-j) !=0)
				{
					type gd = gcd[absynt(i-j)][n];
					type res = (2*C[gd-1][k-1] + C[gd-1][k-2]) % md;
					v += res;
					v = (v % md);
				}
				else
				{
					v += (2*C[n-1][k-1] + C[n-1][k-2]) % md;
					v = (v % md);
				}
		}

	type add = 2*C[n+1][k] + 2*C[m+1][k]; 
	// if(k==2 && !(n==1 && m==1 ))
		// add-=2;
	// cout<<tri<<" "<<h<<" "<<v<<" "<<diag<<endl;
	type R = (v + h + tri + add) % md;
	return R;
}

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	type n,m,k;
	genC();
	// cout<<C[1][2];
	genGcd();
	//wypisze genC i gengcd
	// cout<<C[3][0]<<endl;
	// cout<<C[7][8]<<endl;
	// cout<<gcd[1302][8*31]<<" "<<gcd[8*31][1302]<<endl;

	cin>>n>>m>>k;
	cout<<solve(n, m,k);

	return 0;
}