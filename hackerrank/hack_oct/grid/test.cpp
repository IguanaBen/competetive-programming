#include <iostream>
#include <string>
#include <map>

#define type long long

int gcd(int a, int b)
{
	if( a== 0 )
		return b;

	if( b== 0)
		return a;

	return gcd(b, a % b);
}

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	map<pair<int, int>, int > mm; 

	for(int i=1;i<2000;i++)
	{
		for(int j=1;j<2000;j++)
		{
			int gc = gcd(i, j);
			int l = i/gc;
			int m = j/gc;
			mm[make_pair(l,m)] = 1;
		}
		if( i % 300 == 0)
			cout<<"SD\n";
	}

		cout<<mm.size()<<endl;

	return 0;
}