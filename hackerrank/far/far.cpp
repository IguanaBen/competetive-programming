#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;


int max_num = 0;
vector<int>edges[132];
bool marked[142];

int dist[142][142];

//finding distances between vertices
//overkill, it can be done in O(n^2) time
//p.e. using bfs
void floyd(int n)
{
	for(int k=1; k<=n; k++)
		for(int i=1; i<=n; i++)
			for(int j=1; j<=n; j++)
				if( dist[i][k] + dist[k][j] < dist[i][j]  )
					dist[i][j] = dist[i][k] + dist[k][j];
}


bool out[142];

int main()
{
	int n, k;
	cin>>n>>k;
	int x,y;

	for(int k=1; k<=n; k++)
		for(int i=1; i<=n; i++)
			dist[i][k] = 999;

	for(int a =1; a <= n; a++)
	{
		dist[a][a] = 0;
	}


	for(int a =0; a< n-1; a++)
	{
		cin>>x>>y;
		/*edges[a].push_back(b);
		edges[b].push_back(a);*/
		dist[x][y] = 1;
		dist[y][x] = 1;
	}

//	cout<<endl;
	floyd(n);
	
	bool b = false;

	//wierzcholek, ktory jest w k parach z takimi v
	//ze d(u,v) >=k, i gdzie k jest maksymalnie, jest lisciem
	//korzysta sie z tego w dowodzie	
	int removed = 0;


//nadal da sie w n^2...
//trzeba te o odl k/2 + 1(dla k nieparzystych)
//zrobic jak ponizej
//n^2 czy lgn n^2?
	
	while(true)
	{
		int max_num = -1;
		int max_v = -1;
		for(int z =1; z<=n; z++)
		{
			bool x = false;
			int num = 0;
			if(!out[z])
			{
				for(int y =1; y<=n; y++)
				{
					if(!out[y])
					{
						if(dist[z][y] > k)
							num++;
					}
				}
			}

			if(num > max_num && num != 0)
			{
				max_num = num;
				max_v = z;
			}
		}

		if( max_v != -1)
		{
			out[max_v] = true;
			removed++;
		}
		else
			break;
	}

	cout<<removed<<"\n";
    return 0;
}
