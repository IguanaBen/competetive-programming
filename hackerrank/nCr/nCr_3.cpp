#include <iostream>
#include <algorithm>
#include <vector>
#define type int

using namespace std;


int rem_powers[100];

//n^t mod p
int pow_mod(int n, int t, int p)
{	

	if( t == 0 || n == 0)
		return 1;

	if(t == 1)
		return (n % p);

	if( t % 2 )
	{
		int result = pow_mod(n, t/2, p);
		result = result*result*n;
		result = result % p;
		return result;
	}
	else
	{
		int result = pow_mod(n, t/2, p);
		result = result*result;
		result = result % p;
		return result;
	}

}


type odwr_mod(type a, type b)
{
	type u,w,x,z,q;
	  u = 1; w = a;
	  x = 0; z = b;
	  while(w)
	  {
	    if(w < z)
	    {
	      q = u; u = x; x = q;
	      q = w; w = z; z = q;
	    }
	    q = w / z;
	    u -= q * x;
	    w -= q * z;
	  }
	  if(z == 1)
	  {
	    if(x < 0) x += b;
	   return x;
	  }
	  //nie powinno tak byc
	  return -1;
}

vector<int> factorial_mod_27(int n)
{
	vector<int> mod;
	vector<int> tab_powers;

	for(int a = 0; a<27; a++)
		tab_powers.push_back(0);

	for(int a=1; a < 27; a++)
	{	
		tab_powers[a] = 0;
		if(a % 3)
			mod.push_back(a);
	}

	int np = n;

	while(np > 0)
	{
		int n_div = np/27;
		for(int a=0; a < mod.size(); a++)
		{
			int t = mod[a];
			tab_powers[t] += n_div + (np % 27 >= t);
		}
		np /=3;
		tab_powers[3] += np;
	}

	return tab_powers;
}

int mod_27(int n, int k)
{

	vector<int> l_tab_p = factorial_mod_27(n);
	vector<int> m1_tab_p = factorial_mod_27(n-k);
	vector<int> m2_tab_p = factorial_mod_27(k);


	for(int a=0; a < 27; a++)
	{
		m1_tab_p[a] += m2_tab_p[a];
	}
	for(int a=0; a < 27; a++)
	{
		l_tab_p[a] -= m1_tab_p[a];
	}

	int licz = 1;
	int mian = 1;

	for(int a=0; a < 27; a++)
	{
		//niepotrzebne bo i tak powinny byc 0-ami(ale polepsza stala...)
		if( a % 3 && l_tab_p[a] != 0)
		{
				if(l_tab_p[a] >= 0)
				{
					licz *= pow_mod(a, l_tab_p[a], 27);
					licz = licz % 27;
				}
				else
				{
					mian *= pow_mod(a, -l_tab_p[a], 27);
					mian = mian % 27;
				}
		}
	}

	if(l_tab_p[3] >= 0)
			{
				licz *= pow_mod(3, l_tab_p[3], 27);
				licz = licz % 27;
			}
			else
			{
				mian *= pow_mod(3, -l_tab_p[3], 27);
				mian = mian % 27;
			}

	if( l_tab_p[3] % 3 == 0 && l_tab_p[3] !=0)
		licz = 0;

	//mian=0?? - JAK?
	if (licz == 0 || mian ==0)
		return 0;
	else
	{	
		// cout<<licz<<" "<<mian<<endl;
		int odwr = odwr_mod(mian, 27);
		// cout<<"O: "<<odwr<<endl;
		if(odwr < 0 )
		{
			odwr = odwr % 27;
			odwr = 27 + odwr;
		}

		int res = (licz * odwr) % 27;
		return res;
	}
}



vector<int> factorial_mod_p(int n, int p)
{
	vector<int> tab_powers;

	for(int a = 0; a<=p; a++)
		tab_powers.push_back(0);
	

	int n_div = n/p;


	int np = n;
	while(np > 0)
	{
		int n_div = np/p;
		for(int a=1; a <= p; a++)
		{
			int t = a;
			tab_powers[t] += n_div + (np % p >= t);
		}
		np /= p;
		tab_powers[p] += np;
	}

	return tab_powers;
}

int mod_p(int n, int k, int p)
{
	vector<int> l_tab_p = factorial_mod_p(n, p);
	vector<int> m1_tab_p = factorial_mod_p(n-k, p);
	vector<int> m2_tab_p = factorial_mod_p(k, p);

	for(int a=0; a <= p; a++)
	{
		m1_tab_p[a] += m2_tab_p[a];
	}
	for(int a=0; a <= p; a++)
	{
		l_tab_p[a] -= m1_tab_p[a];
	}

	int licz = 1;
	int mian = 1;

	for(int a=1; a <= p; a++)
	{
		if(l_tab_p[a] != 0)
			if(l_tab_p[a] >= 0)
			{
					//cout<<pow_mod(a, l_tab_p[a], p)<<"  ";
					licz *= pow_mod(a, l_tab_p[a], p);
					licz = licz % p;
			}
			else
			{
					mian *= pow_mod(a, -l_tab_p[a], p);
					mian = mian % p;
			}
	}
	//cout<<endl;

	//mian=0?? -j.w.
	if (licz == 0 || mian ==0)
		return 0;
	else
	{	
		int odwr = odwr_mod(mian, p);
		if(odwr < 0 )
		{
			odwr = odwr % p;
			odwr = p + odwr;
		}

		int res = (licz * odwr) % p;
		return res;
	}

}


pair<type, type> extended_euclid(type a, type b)
{
	type a0 = a;
	type b0 = b;
	 
	type p = 1;
	type q = 0;
	type r = 0;
	type s = 1;
	 
	while (b != 0)
	{
	  type c = a % b;
	  type quot = a/b;
	  a = b;
	  b = c;
	  type new_r = p - quot * r;
	  type new_s = q - quot * s;
	  p = r; 
	  q = s;
	  r = new_r;
	  s = new_s;
	}

	return make_pair(p, q);
}


pair<type, type> join_con(pair<type, type> p1, pair<type, type> p2)
{
	pair<type, type> coeff = extended_euclid(p1.first, p2.first);
	type fs =  p1.first * p2.first;
	type sn = (coeff.first* p1.first * p2.second) + (coeff.second*p2.first * p1.second);
	//cout<<fs<<" "<<sn<<endl;

	// sn = sn % fs;
	//cout<<"SN: "<< sn<<endl;

	//jeszcze bardziej rumunski spsob na przeciwnosc modulo
	if(sn < 0 )
		{
			sn = sn % fs;
			sn = fs + sn;
		}
	sn = sn % fs;
	return make_pair(fs, sn);
}

type primes[] = { 3, 11, 13, 37};

int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int t;
	cin>>t;
	for(int i=0; i<t; i++)
	{
		type n, k;
		cin>>n>>k;
		pair<type, type> res = make_pair(27, mod_27(n,k));
		for(int a=1; a<4; a++)
		{
			pair<type, type> t = make_pair(primes[a], mod_p(n,k,primes[a]) );
			res = join_con(res, t);
		}
		cout<<res.second<<"\n";
	}
	return 0;
}