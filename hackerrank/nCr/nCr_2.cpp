#include <iostream>
#include <algorithm>
#include <vector>
#define type long long 

using namespace std;


int rem_powers[100];

//n^t mod p
int pow_mod(int n, int t, int p)
{	
	if( t == 0)
		return 1;

	if(t == 1)
		return (n % p);

	if( t % 2 )
	{
		int result = pow_mod(n, t/2, p);
		result = result*result*n;
		result = result % p;
		return result;
	}
	else
	{
		int result = pow_mod(n, t/2, p);
		result = result*result;
		result = result % p;
		return result;
	}

}


type odwr_mod(type a, type b)
{
	type u,w,x,z,q;
	  u = 1; w = a;
	  x = 0; z = b;
	  while(w)
	  {
	    if(w < z)
	    {
	      q = u; u = x; x = q;
	      q = w; w = z; z = q;
	    }
	    q = w / z;
	    u -= q * x;
	    w -= q * z;
	  }
	  if(z == 1)
	  {
	    if(x < 0) x += b;
	   return x;
	  }
	  //nie powinno tak byc
	  return -1;
}

vector<int> factorial_mod_27(int n)
{
	vector<int> mod;
	vector<int> tab_powers;

	for(int a = 0; a<27; a++)
		tab_powers.push_back(0);

	for(int a=1; a < 27; a++)
	{	
		tab_powers[a] = 0;
		if(a % 3)
			mod.push_back(a);
	}

	int np = n;

	while(np > 0)
	{
		int n_div = np/27;
		for(int a=0; a < mod.size(); a++)
		{
			int t = mod[a];
			tab_powers[t] += n_div + (np % 27 >= t);
		}
		np /=3;
		tab_powers[3] += np;
	}
	/*
	for(int a=0; a< mod.size(); a++)
	{	
		cout<<mod[a]<<"|"<<tab_powers[mod[a]]<<" ";
	}
	cout<<3<<"|"<<tab_powers[3]<<endl;
	*/
	return tab_powers;


}

/*
vector<int> factorial_mod_27(int n)
{
	vector<int> mod;
	vector<int> tab_powers;
	for(int a = 0; a<27; a++)
		tab_powers.push_back(0);

	for(int a=1; a < 27; a++)
	{	
		tab_powers[a] = 0;
		if(a % 3)
			mod.push_back(a);
	}
	int n_div = n/27;

	for(int a=0; a < mod.size(); a++)
	{
		int t = mod[a];
		tab_powers[t] += n_div + (n % 27 >= t);
	}
	// tab_powers[3] += n_div;
	int t = 0;
	t = 3;
	tab_powers[3] += n_div + (n % 27 >= t);

	t = 6;
	tab_powers[2] += n_div + (n % 27 >= t);
	tab_powers[3] += n_div+ (n % 27 >= t);

	t = 9;
	tab_powers[3] += 2*(n_div + (n % 27 >= t));
	// tab_powers[3] = n/t + (n mod 27 >= t);
	t = 12;
	tab_powers[2] += 2*(n_div + (n % 27 >= t));
	tab_powers[3] += n_div + (n % 27 >= t);

	t = 15;
	tab_powers[3] += n_div + (n % 27 >= t);
	tab_powers[5] += n_div + (n % 27 >= t);

	t = 18;
	tab_powers[2] += n_div + (n % 27 >= t);
	tab_powers[3] += 2*(n_div + (n % 27 >= t));

	t = 21;
	tab_powers[3] += n_div + (n % 27 >= t);
	tab_powers[7] += n_div + (n % 27 >= t);

	t = 24;
	tab_powers[2] += 3*(n_div + (n % 27 >= t));
	tab_powers[3] += n_div + (n % 27 >= t);

	t = 27;
	tab_powers[3] += 3*(n_div + (n % 27 >= t));

	int fact = 1;
	for(int a=0; a< mod.size(); a++)
	{	
		cout<<mod[a]<<"|"<<tab_powers[mod[a]]<<" ";
		fact *= pow_mod(mod[a], tab_powers[mod[a]], 27);
		fact = fact % 27;
	}
	cout<<3<<"|"<<tab_powers[3]<<endl;
	fact *= pow_mod(3, tab_powers[3], 27);
	fact = fact % 27;

	//cout<<fact<<endl<<endl;

	return tab_powers;
}*/

int mod_27(int n, int k)
{

	vector<int> l_tab_p = factorial_mod_27(n);
	vector<int> m1_tab_p = factorial_mod_27(n-k);
	vector<int> m2_tab_p = factorial_mod_27(k);
	cout<<endl<<endl;

/*
	for(int a =0; a< 27; a++)
		cout<< a<<"|"<<l_tab_p[a] << " ";
	cout<<endl;
	for(int a =0; a< 27; a++)
		cout<< m1_tab_p[a] << " ";
	cout<<endl;
	for(int a =0; a< 27; a++)
		cout<< m2_tab_p[a] << " ";
	cout<<endl;
*/

	for(int a=0; a < 27; a++)
	{
		m1_tab_p[a] += m2_tab_p[a];
	}
	for(int a=0; a < 27; a++)
	{
		l_tab_p[a] -= m1_tab_p[a];
	}

	/*for(int a=0; a<27;a++)
		cout<<l_tab_p[a]<<" ";

	cout<<endl;
*/
	int licz = 1;
	int mian = 1;

	for(int a=0; a < 27; a++)
	{
		if( a % 3)
		{
			//cout<<a<<" ";
			if(l_tab_p[a] >= 0)
			{
				licz *= pow_mod(a, l_tab_p[a], 27);
				licz = licz % 27;
			}
			else
			{
				mian *= pow_mod(a, -l_tab_p[a], 27);
				mian = mian % 27;
			}
		}
	}

	if(l_tab_p[3] >= 0)
			{
				licz *= pow_mod(3, l_tab_p[3], 27);
				licz = licz % 27;
			}
			else
			{
				mian *= pow_mod(3, -l_tab_p[3], 27);
				mian = mian % 27;
			}

	if( l_tab_p[3] % 3 == 0 && l_tab_p[3] !=0)
		licz = 0;

	//mian=0??
	if (licz == 0 || mian ==0)
		return 0;
	else
	{	
		cout<<licz<<" "<<mian<<endl;
		int odwr = odwr_mod(mian, 27);
		// cout<<"O: "<<odwr<<endl;
		if(odwr < 0 )
		{
			odwr = odwr % 27;
			odwr = 27 + odwr;
		}

		int res = (licz * odwr) % 27;
		return res;
	}


}

int C[5000][5000];

int main()
{

	for(int a=1;a<5000; a++)
	{
		for(int b=0; b<5000;b++)
			{
				if( a== b ||b ==0)
					C[a][b] = 1;
				else
					C[a][b] = (C[a-1][b-1] + C[a-1][b]) % 27;
			}
	}

	int n, k;
	cin>>n>>k;
	// cout<<"PP: "<<C[n][k]<<endl;
	int c_3 =0;
	int np = n;
	while((np % 3) == 0)
	{
		np /= 3;

		c_3++;
	}
	cout<<"C3: "<<c_3<<endl;
	cout<<mod_27(n, k)<<endl;
	return 0;
}