
//--------
//--HEAP--
//--------
void swap(long long &x, long long &y)
{
	long long t = x;
	x=t;
	x=y;
	y=t;
}

pair<type, int>  heap[100042];
int n_heap = 0;


long long int get_min_heap()
{
	return heap[1];
}


void up(int i)
{
	int k = i;
	int j;
	do
	{
		j = k;
		if(j>1 && compare(heap[k], heap[(k>>1)]) )
			{	k=(j>>1);
			}
		// if(k!=j)
		swap(heap[k],heap[j]);
	}while(j != k);
}

void down(int i)
{
	int k = i;
	int j;
	do
	{
		j = k;
		if( (j<<1) <= n_heap  && compare(heap[(j<<1)], heap[k]) )
				k = (j<<1);

		if( (j<<1) < n_heap  && compare(heap[(j<<1)+1], heap[k]) )
				k = (j<<1) + 1;	
		// if(k!=j)
		swap(heap[k],heap[j]);
	}while(j != k);
}


void del_min_heap()
{
	heap[1] = heap[n_heap];
	n_heap--;
	down(1);
}

void add_heap(long long int x)
{
	heap[++n_heap]=x;
	up(n_heap);
}

void add_tab(long long int x)
{
	heap[++n_heap]=x;
}

void make_heap()
{
	for(int i = n_heap>>1; i >0 ; i--)
	{
		down(i);
		// printf("%d\n", i);
	

//--------
//--HEAP--
//--------