#include <iostream>
#include <algorithm>
#include <queue>
#include <vector>

#define type long long int

using namespace std;


//--------
//--HEAP--
//--------
bool compare(pair<type, int>  const& p1, pair<type, int>  const& p2)
{
	if(p1.first == p2.first)
		return p1.second < p2.second;
	else
		return p1.first < p2.first;
}

void swap(long long &x, long long &y)
{
	long long t = x;
	x=t;
	x=y;
	y=t;
}

pair<type, int>  heap[100042];
int n_heap = 0;


pair<type, int>  get_min_heap()
{
	return heap[1];
}


void up(int i)
{
	int k = i;
	int j;
	do
	{
		j = k;
		if(j>1 && compare(heap[k], heap[(k>>1)]) )
			{	k=(j>>1);
			}
		// if(k!=j)
		swap(heap[k],heap[j]);
	}while(j != k);
}

void down(int i)
{
	int k = i;
	int j;
	do
	{
		j = k;
		if( (j<<1) <= n_heap  && compare(heap[(j<<1)], heap[k]) )
				k = (j<<1);

		if( (j<<1) < n_heap  && compare(heap[(j<<1)+1], heap[k]) )
				k = (j<<1) + 1;	
		// if(k!=j)
		swap(heap[k],heap[j]);
	}while(j != k);
}


void del_min_heap()
{
	heap[1] = heap[n_heap];
	n_heap--;
	down(1);
}

void add_heap(pair<type, int> x)
{
	heap[++n_heap]=x;
	up(n_heap);
}

void add_tab(pair<type, int>  x)
{
	heap[++n_heap]=x;
}

void make_heap()
{
	for(int i = n_heap>>1; i >0 ; i--)
	{
		down(i);
		// printf("%d\n", i);
	}
}

//--------
//--HEAP--
//--------
// 2700000 ??
const type offset = 2600000;

type input[100042];
type result[100042];

type k;
type d;


bool check(type time)
{
	type sum = 0;
	for(int a = 0 ; a < k; a++)
		sum +=  (time/input[a]);

	if(d - offset > sum)
		return 0;
	else
		return 1;

}

type find_end(type start, type end)
{
	if(start == end)
		return start;

	if(end == start + 1)
	{
		if(check(start))
			return start;
		return end;
	}

	type m = start + (end - start)/2;
	// cout<<m<<endl;
	bool res = check(m);
	if(res)
		return find_end(start, m);
	else
		return find_end(m, end);
}

struct pair_compare
{
   bool operator() (pair<type, int>  const& p1, pair<type, int>  const& p2) const
	{
		if(p1.first == p2.first)
			return p1.second > p2.second;
		else
			return p1.first > p2.first;
	}
};

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	cin>>d>>k;

	cin>>input[0];
	type mn = input[0];
	for(int a = 1; a<k; a++)
	{
		cin>>input[a];
		mn = min(mn, input[a]);
	}
	 
	type pivot_time;
	// cout<<pivot_time<<endl;
	if(d < offset)
		pivot_time = 0;
	else
		pivot_time = find_end(1, d*mn);


	type sum = 0;
	// for(int a = 0 ; a < k; a++)
	// 

	priority_queue< pair<type, int>, vector< pair<type, int> >, pair_compare > qjuju;

	pair<type, int> temp;
	for(int a = 0; a< k; a++)
	{
		sum += (pivot_time/input[a]);
		temp.first = pivot_time - (pivot_time % input[a]);
		temp.second = a;
		qjuju.push( temp );
		// add_heap(temp);
	}

	// cout<<qjuju.top().first<<" "<<qjuju.top().second<<endl;
	// qjuju.pop();
	// cout<<qjuju.top().first<<" "<<qjuju.top().second<<endl;

	type remaining = d - sum;
	type remaining_copy = remaining;
	// cout<<remaining
	// int max_size = 0;
	int counter = 1;
	while(remaining > 0)
	{
		temp = qjuju.top();
		// temp = get_min_heap();
		// cout<<temp.first << " "<< temp.second <<endl;
		result[temp.second] = counter;
		counter++;

		qjuju.pop();
		// del_min_heap();

		// add_heap(make_pair(temp.first + input[temp.second], temp.second ));
		qjuju.push( make_pair(temp.first + input[temp.second], temp.second ) );
		remaining--;
	}
	// cout<<max_size<<" \n ";
	for(int a =0; a<k; a++)
		cout<<d - (remaining_copy - result[a])<<" ";

	cout<<"\n";

	return 0;
}
