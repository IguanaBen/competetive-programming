#include <iostream>
#include <algorithm>
#include <queue>
#include <vector>

#define type long long int

using namespace std;


// 2700000 ??
const type offset = 0;

type input[100042];
type result[100042];

type k;
type d;


bool check(type time)
{
	type sum = 0;
	for(int a = 0 ; a < k; a++)
		sum +=  (time/input[a]);

	if(d - offset > sum)
		return 0;
	else
		return 1;

}

type find_end(type start, type end)
{
	if(start == end)
		return start;

	if(end == start + 1)
	{
		if(check(start))
			return start;
		return end;
	}

	type m = start + (end - start)/2;
	// cout<<m<<endl;
	bool res = check(m);
	if(res)
		return find_end(start, m);
	else
		return find_end(m, end);
}

struct pair_compare
{
   bool operator() (pair<type, int>  const& p1, pair<type, int>  const& p2) const
	{
		if(p1.first == p2.first)
			return p1.second > p2.second;
		else
			return p1.first > p2.first;
	}
};

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	cin>>d>>k;

	cin>>input[0];
	type mn = input[0];
	for(int a = 1; a<k; a++)
	{
		cin>>input[a];
		mn = min(mn, input[a]);
	}
	 
	type pivot_time;
	// cout<<pivot_time<<endl;
	if(d < offset)
		pivot_time = 0;
	else
		pivot_time = find_end(1, d*mn);

	pivot_time -= 200042;
	if(pivot_time < 0)
		pivot_time = 0;


	type sum = 0;
	// for(int a = 0 ; a < k; a++)
	// 

	priority_queue< pair<type, int>, vector< pair<type, int> >, pair_compare > qjuju;

	pair<type, int> temp;
	for(int a = 0; a< k; a++)
	{
		sum += (pivot_time/input[a]);
		temp.first = pivot_time - (pivot_time % input[a]);
		temp.second = a;
		qjuju.push( temp );
		// add_heap(temp);
	}

	// cout<<qjuju.top().first<<" "<<qjuju.top().second<<endl;
	// qjuju.pop();
	// cout<<qjuju.top().first<<" "<<qjuju.top().second<<endl;

	type remaining = d - sum;
	type remaining_copy = remaining;
	// cout<<remaining
	// int max_size = 0;
	int counter = 1;
	while(remaining > 0)
	{
		temp = qjuju.top();
		// temp = get_min_heap();
		// cout<<temp.first << " "<< temp.second <<endl;
		result[temp.second] = counter;
		counter++;

		qjuju.pop();
		// del_min_heap();

		// add_heap(make_pair(temp.first + input[temp.second], temp.second ));
		qjuju.push( make_pair(temp.first + input[temp.second], temp.second ) );
		remaining--;
	}
	// cout<<max_size<<" \n ";
	for(int a =0; a<k; a++)
		cout<<d - (remaining_copy - result[a])<<" ";

	cout<<"\n";

	return 0;
}
