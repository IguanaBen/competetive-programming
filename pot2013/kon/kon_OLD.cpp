#include <iostream>
#include <algorithm>

#define type long long int

using namespace std;

type input[100042];
type result[100042];
pair<type, int> lasts_begining[100042];
//H_(10^5) * 10^5 /approx 12 * 10 ^5
pair<type, int> beginings[4000042];


type k;
type d;

// [------]
// [----]
// [--][--][--][--]

bool check(type time)
{
	type p_last_end = time - (time % input[0]);
	type p_last_begin = p_last_end - input[0]; //<0?
	type c_last_begin, c_last_end;
	type sum = 0;
	type temp_d =  d;
	for(int a = 0 ; a < k; a++)
	{	
		type div = time/input[a];
		// 123456789

		c_last_end =  time - (time % input[a]);
		c_last_begin = c_last_end - input[a];
		if(c_last_begin > p_last_end)
			return 0;

		if(c_last_end < p_last_begin)
			return 0;

		p_last_begin = c_last_begin;
		p_last_end = c_last_end;

		sum +=  (time/input[a]);
	}
	if(d > sum)
		return 0;
	else
		return 1;

}

//wyszukujemy binarnie punkt w ktorym
//wszystkie bilety zostaly sprawdzone
type find_end(type start, type end)
{
	if(start == end)
		return start;

	if(end == start + 1)
	{
		if(check(start))
			return start;
		return end;
	}

	type m = start + (end - start)/2;
	// cout<<m<<endl;
	bool res = check(m);
	if(res)
		return find_end(start, m);
	else
		return find_end(m, end);

}

bool compare(pair<type, int>  const& p1, pair<type, int>  const& p2)
{
	if(p1.first == p2.first)
		return p1.second < p2.second;
	else
		return p1.first < p2.first;
}

int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	cin>>d>>k;
	cin>>input[0];
	type mn = input[0];
	for(int a = 1; a<k; a++)
	{
		cin>>input[a];
		mn = min(mn, input[a]);
		// cout<<mn<<endl;
	}
	// cout<<mn*d<<endl;
	type end_time = find_end(1, d*mn);
	// cout<<end_time<<endl;
	// cout<<end_time%90365<<endl;
	// cout<<end_time%1754<<endl;

	// //przekraczasz ilosc przedzialow
	// cout<<end_time - (end_time%90365) - 90365<<endl;
	// cout<<end_time - (end_time%1754) - 1754<<endl;

	// cout<<35075717773346030-35075717773433992<<endl;

	// cout<<(end_time%1754)<<endl;
	// cout<<(end_time%90365)<<endl;
	// type xx =end_time - (end_time%90365);

	// // end_time = 34412156144769712 ;
	// // end_time = (158500210* 2000000000000)/92119
	// // end_time = 34412056144769266;
	for(type t = end_time - 1000; t < end_time+3; t++)
		if(check(t) == 1)
			cout<<"Sdsd"<<endl;

	//PRZEKRACZASZ LICZBE PRZEDZIALOW
	type used = 0;
	pair<type, int> fl;
	for(int a=0; a<k; a++)
	{
		// if(d % input[a] == 0)
			// lasts_begining[a].first = d-input[a];
		// else

		//JESLI NIC SIE NIE MIESCI?
		//kazdy konduktor sprawdzil co najmniej jednego, wiec niemozliwe
		lasts_begining[a].first = end_time - input[a] - (end_time % input[a]);
		lasts_begining[a].second = a;
		// cout<<lasts_begining[a].first<<endl;
	}

	sort(lasts_begining, lasts_begining +k, compare);

	// for(int a=0; a<k; a++)
	// {
	// 	cout<<lasts_begining[a].second<<" "
	// 	<<lasts_begining[a].first<<endl;
	// }

	int interval_n = 1;
	beginings[0] = lasts_begining[0];
	// 0?
	for(int a = 1; a < k; a++)
	{
		int index = lasts_begining[a].second;
		type start_inv;
		if(lasts_begining[0].first % input[index] != 0)
			start_inv = lasts_begining[0].first-
					(lasts_begining[0].first % input[index]) + input[index];
		else
			start_inv = lasts_begining[0].first;

		while(start_inv <= lasts_begining[a].first)
		{
			// cout<<index<<" "<<start_inv<<endl;
			beginings[interval_n].first = start_inv;
			beginings[interval_n].second = index;
			interval_n++;
			start_inv += input[index];
		}
	}
	sort(beginings, beginings + interval_n, compare);
	// for (int i = 0; i < interval_n; ++i)
	// {
	// 	cout<<end_time - beginings[i].first<<" "<<beginings[i].second<<endl;
	// } 

	int beg_index = 0;

	for(int a = 0; a < k; a++)
	{
		while( compare(beginings[beg_index], lasts_begining[a]) )
		{
			beg_index++;
		}
		result[lasts_begining[a].second] = beg_index;
	}
	// cout<<interval_n<<endl;
	for(int a = 0; a < k; a++)
	{
		type ind = interval_n - result[a] -1;
		cout<<d - ind<<" ";
	}
	cout<<"\n";
	return 0;
}