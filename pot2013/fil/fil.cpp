#include <iostream>
#include <algorithm>
#include <vector>

int filary[100042];
int mods[100042];

using namespace std;

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n;
	cin>>n;
	int mx = 0;

	for(int a = 0; a<n; a++)
	{
		cin>>filary[a];
		mx = max(mx, filary[a]);
	}

	pair<int,int> dd;
	dd.first = -100;

		// cout<<mx<<endl;
	for(int z =2; z <= mx; z++)
	{
		for(int a = 0; a<n; a++)
		{
			mods[a] = filary[a] % z;
		}
		sort(mods, mods + n);
		// for(int a=0; a<n; a++)
		// {
		// 	cout<<mods[a]<<" ";
		// }
		
		int prev = -100;
		int c = 0;
		int max_c = -3;
		int ddz;
		for(int a = 0; a<n; a++)
		{
			if(prev == mods[a])
			{
				c++;
				if(max_c < c)
				{
					max_c = c;
					ddz = mods[a];
				}
			}
			else
			{
				c = 1;
				if(max_c < c)
				{
					max_c = c;
					ddz = mods[a];
				}
				prev = mods[a];
			}
		}
		// cout<<endl;
		// cout<<max_c<<endl;
		if(max_c > dd.second )
		{
			dd.first = z;
			dd.second = max_c;
		}
	}

	cout<<dd.second<<" "<<dd.first<<"\n";

	return 0;
}