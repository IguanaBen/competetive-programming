#include <iostream>
#include <algorithm>

using namespace std;

int tab[2][300042];
long long int tab_l[300042];
const long long int bigc = 300042;

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int n;
	cin>>n;

	int t;
	for(int i = 0; i<2; i++)
		for(int j=1; j <= n; j++)
		{
			cin>>t;
			tab[i][t] = j;
		}

	for(int j = 1; j<=n; j++)
	{
		cin>>t;
		tab_l[j] = bigc*((tab[0][t]-j+n) % n) + ((tab[1][t]-j+n) % n);	//nie trzeba dodawac n?
	}

	sort(tab_l+1, tab_l+n+1);
	int mx = 1;
	int combo = 0;
	long long int last = -2;

	// for (int j=1; j<=n ; ++j)
	// {
	// 	cout<<tab_l[j]<<" ";
	// }
	// cout<<endl;

	for(int j=1; j<=n; j++)
	{
		if(tab_l[j] == last)
		{
			combo++;
			mx = max(mx, combo);
		}
		else
		{
			combo = 1;
			last = tab_l[j];
		}
	}
	cout<<mx<<"\n";

	return 0;
}