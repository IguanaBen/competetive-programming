#include <iostream>
#include <vector>

#define type long long int

using namespace std;

type wyjazd[405];
int deg[405];
type delay[405];

vector <int> out_edges[405];
vector <int> in_edges[405];
vector <type> dep_time[405];
vector <type> travel_time[405];


bool marked[402];

vector<int> indices;

void toposort(int n)
{
	if(!marked[n])
	{
		for(int a=0; a< out_edges[n].size(); a++)
		{
			// cout<<out_edges[n][a]<<endl;
			toposort(out_edges[n][a]);
		}

		marked[n]= true;
		indices.push_back(n);
	}
}

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);
	int n,m;
	cin>>n>>m;
	type dy;
	cin>>dy;

	int a, b;
	type w, p;
	wyjazd[1] = 0;
	for(int i = 0; i < m; i++)
	{
		cin>>a>>b>>w>>p;
		out_edges[a].push_back(b);
		in_edges[b].push_back(a);
		wyjazd[b] = max(wyjazd[b], w+p);
		dep_time[b].push_back(w);
		travel_time[b].push_back(p);
		// deg[a]++;
	}
	toposort(1);

	type max_delay  = -5;
	type sum_delay = 0;

	for(int a = n-2; a >= 0; a--)
	{	
		sum_delay = 0;

		for(int b = 1; b<n+2; b++)
			delay[b] = wyjazd[b];

		sum_delay+=dy;
		delay[indices[a]]+=dy;

		type arrive_time = 0;

		for(int x=a; x>=0; x--)
		{
			// cout<< x<<" " <<"Sd"<<endl;
			
			int v = indices[x];
			// cout<<"V: "<<v<<endl;
			type current_delay = -1;
			for(int e = 0; e < in_edges[v].size(); e++)
			{
				// ??
				arrive_time = max(delay[in_edges[v][e]], dep_time[v][e])  +
								+ travel_time[v][e];
				
				sum_delay += (arrive_time - (dep_time[v][e] + travel_time[v][e]));

				// cout<<endl;
				// cout<<in_edges[v][e]<<" ";
				// cout<<delay[in_edges[v][e]]<<" ";
				// cout<<dep_time[v][e]<<" ";
				// cout<<(arrive_time - (dep_time[v][e] + travel_time[v][e]))<<", ";
				delay[v] = max(delay[v], arrive_time);  
				// cout<<"S: "<<sum_delay<<endl;

			}
			// cout<<endl;
		}
		// cout<<"S: "<<sum_delay<<endl;
		// cout<<endl;
		// cout<<indices[a]<<" ";
		max_delay = max(max_delay, sum_delay);
	}
	cout<<max_delay<<"\n";
	return 0;
}