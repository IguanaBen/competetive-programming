#include <iostream>
#include <algorithm>

using namespace std;

int west[1000042];
int east[1000042];

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	long long int cap = 0;

	int n, m;
	cin>>n>>m;

	for(int i=0; i<n;i++)
		cin>>west[i];

	for(int i=0; i<m;i++)
		cin>>east[i];

	sort(west, west + n);
	sort(east, east + m);

	int index1 = 0;
	int index2 = 0;
	int m_ind = 0;

	if(west[n-1] != east[m-1])
	{
		// cout<<west[n-1]<<" "<<east[n-1]<<endl;
		cout<<"NIE\n";
		return 0;
	}

	while(west[index1]==0)
		index1++;

	while(east[index2]==0)
		index2++;

	long long int sum = 0;
	// long long int t = (n-index1)*(m-index2) * min(west[index1], east[index2] );
	// sum += t;
	// cout<<sum<<"\n";
	int e = 0;
	long long i1;
	long long i2;
	long long int old_e;
	while(index1 < n && index2 < m)
	{
		old_e = e;
		e = min(west[index1], east[index2] );
		// if( west[index1] )
		i1 = (n-index1);
		i2 = (m-index2);
		// long long int t = i1*i2;
		sum += (i1*i2*(e-old_e));
		// cout<<e << " "<< old_e<<"\n";
		// cout<<t<<" \n \n";

		while(west[index1]==e)
			index1++;

		while(east[index2]==e)
			index2++;

		// cout<<sum<<endl;
	}

	cout<<sum<<"\n";

	return 0;
}