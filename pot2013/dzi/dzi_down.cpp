#include <iostream>
#include <algorithm>

#define type long long int

using namespace std;

pair<type, type> points[7042];
pair<type, type> pointsY[7042];

pair<type, type> temp_points[7042];
pair<type, type> stack[7042];
int n_stack = 0;

bool compareY(pair<type, type> const& p1, 
				pair<type, type>  const& p2)
{
	if(p1.second == p2.second)
		return p1.first < p2.first;
	else
		return p1.second < p2.second;
}

bool compareX(pair<type, type> const& p1, 
				pair<type, type>  const& p2)
{
	if(p1.first == p2.first)
		return p1.second < p2.second;
	else
		return p1.first < p2.first;
}

type dist_pow2(pair<type, type> const& p1, 
				pair<type, type>  const& p2)
{
	return (p1.first - p2.first) * (p1.first - p2.first) 
		+ (p1.second - p2.second) * (p1.second - p2.second);
}

type ccw(	pair<type, type> const& px,
			pair<type, type> const& p1, 
			pair<type, type>  const& p2)
{
	return  (p1.first - px.first) * (p2.second - px.second) 
				- (p1.second-px.second) * (p2.first - px.first);
}

pair<type, type> p0; 

bool compare_polar_angle(
							pair<type, type> const& p1, 
							pair<type, type>  const& p2)
{

	// type res = (p1.first - p0.second) * (p2.second - p0.second) 
	// 			-(p1.second-p0.second) * (p2.first - p0.first);
	type res = ccw(p0,p2,p1);
	if(res < 0)
		return true;
	if(res == 0)
	{
		// cout<<"Eg";
		return(dist_pow2(p0, p1) < dist_pow2(p0, p2));
	}

	return false;

}



void push_stack(pair<type, type> const& p1)
{
	stack[n_stack] = p1;
	n_stack++;
}

void pop()
{
	n_stack--;
}

pair<type, type> top()
{
	return stack[n_stack-1];
}

type abs_l(type a)
{
	if(a < 0)
		return -a;
	else
		return a;
}

type tri_area_2(pair<type, type> &p0, pair<type, type> &p1, pair<type, type> &p2)
{	
	// cout<<p0.first<<" "<<p0.second<<" "<<p1.first<<" "<<p1.second<<" "<<p2.first<<" "<<p2.second<<endl;
    return abs_l(
    	// p0.first * (p1.second - p2.second) + 
    	// p1.first * ( p2.second - p0.second ) +
    	// p2.first * (p0.second - p1.second ) );
    	(p1.first - p0.first)*(p2.second - p0.second) - (p2.first - p0.first)*(p1.second - p0.second));

    ;
}

pair< type, type> pppts[7042];

type convex_hull(int i1, int i2, pair<type, type>  *pointsz)
{	
	n_stack = 0;

	if(i2 - i1 <=1)
		return 0;

	int index_min = i1;
	pair<type, type> point_min = pointsz[i1];

	temp_points[0] = point_min;

	for(int a = i1+1; a<=i2; a++)
	{
		temp_points[a-i1] = pointsz[a];
		if(compareY(pointsz[a], pointsz[index_min] ))
			index_min = a - i1;
	}


	// for(int a = 0; a<=i2-i1; a++)
	// {
	// 	cout<<temp_points[a].first<<" "<<temp_points[a].second<<endl;
	// }
	// cout<<endl<<endl;


	swap(temp_points[0], temp_points[index_min]);
	p0 = temp_points[0];


	sort(temp_points , temp_points + (i2-i1) + 1, compare_polar_angle);

	// for(int z =1; z < i2; z++)
	// 	cout<<compare_polar_angle(temp_points[z], temp_points[6])<<endl<<endl;

	// for(int a = 0; a<=i2-i1; a++)
	// {
	// 	cout<<temp_points[a].first<<" "<<temp_points[a].second<<endl;
	// }
	// cout<<endl<<endl;

	push_stack(p0);
	push_stack(temp_points[1]);
	push_stack(temp_points[2]);

	

	for (int i = 3; i <= i2-i1; ++i)
	{
		// cout<<n_stack<<endl;
		while( ccw(stack[n_stack-2], stack[n_stack-1], temp_points[i]) <= 0)
			pop();

		push_stack(temp_points[i]);
	}

	// cout<<endl;
	// for(int j =0; j < n_stack; j++)
	// {
	// 	cout<<stack[j].first<<" "<<stack[j].second<<endl;
	// }

	type area = 0;

	for(int a =1; a < n_stack-1; a++)
	{
		// cout<<tri_area_2(temp_points[0], temp_points[a], temp_points[a+1])<<" ";
		 area += tri_area_2(stack[0], stack[a], stack[a+1]);
	}


	return area;

}

 
struct cmpX
{
    bool operator() (const pair<type, type> & p1, 
				const pair<type, type>  & p2)
		{
			if(p1.first == p2.first)
				return p1.second < p2.second;
			else
				return p1.first < p2.first;
		}
};

struct cmpY
{
    bool operator()(pair<type, type> const& p1, 
				pair<type, type>  const& p2)
	{
		if(p1.second == p2.second)
			return p1.first < p2.first;
		else
			return p1.second < p2.second;
	}
};

// bool compareY(pair<type, type> const& p1, 
// 				pair<type, type>  const& p2)
// {
// 	if(p1.second == p2.second)
// 		return p1.first < p2.first;
// 	else
// 		return p1.second < p2.second;
// }

// bool compareX(pair<type, type> const& p1, 
// 				pair<type, type>  const& p2)
// {
// 	if(p1.first == p2.first)
// 		return p1.second < p2.second;
// 	else
// 		return p1.first < p2.first;
// }


bool compX(const pair<type, type>  p1, 
				const pair<type, type>  p2)
{
	if(p1.first == p2.first)
		return p1.second < p2.second;
	else
		return p1.first < p2.first;
}

int main()
{

	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int k, n;
	cin>>k>>n;

	for(int a=0; a<n;a++)
	{
		cin>>points[a].first>>points[a].second;
		pointsY[a] = points[a];
	}

	// cout<<convex_hull( 0, 3)<<"\n";
	sort(points, points + n, compareX);
	sort(pointsY, pointsY + n, compareY);
	// for(int zz =0; zz< n ;zz++)
	// 	cout<<points[zz].first<<" "<<points[zz].second<<endl;

	// cout<<endl;


	int m;
	cin>>m;

	int x1, x2, y1, y2;
	int lx1, lx2, ly1, ly2;

	for(int x =0; x<m; x++)
	{
		cin>>x1>>x2>>y1>>y2;
		lx1 = int( lower_bound(points, points + n, make_pair(x1, 0) , compareX ) - points );
		lx2 = int( upper_bound(points, points + n, make_pair(x2, k+1) , compareX ) - points );

		ly1 = int( lower_bound(pointsY, pointsY + n, make_pair(0, y1) , compareY ) - pointsY );
		ly2 = int( upper_bound(pointsY, pointsY + n, make_pair(k+1, y1) , compareY ) - pointsY ); 

		int zz = 0;

		if(lx2 - lx1 > ly2 - ly1)
		{
			lx2--;

			// int startA = lx1;
			// int startB = lx2;

			for(int a = lx1; a<=lx2; a++)
			{
				if(points[a].second >=y1 && points[a].second <= y2)
				{
					pppts[zz] = points[a];
					zz++;
				}
			}
		}
		else
		{
			ly2--;

			// int startA = lx1;
			// int startB = lx2;

			for(int a = ly1; a<=ly2; a++)
			{
				if(pointsY[a].first >=x1 && pointsY[a].first <= x2)
				{
					pppts[zz] = pointsY[a];
					zz++;
				}
			}

		}	


		type res = convex_hull(0, zz-1, pppts);
		if( res % 2)
			cout<<res/2<<".5\n";
		else
			cout<<res/2<<".0\n";

	}

	return 0;
}