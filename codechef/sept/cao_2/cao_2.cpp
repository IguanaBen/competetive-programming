#include <iostream>
#include <algorithm>

using namespace std;


char array[503][503];


bool prime[501];
int mul[501];


int ar[503][503];

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	for(int a=2; a< 500; a++)
	{
		bool p = 1;
		for(int b=2; b<a;b++)
			if(a % b == 0)
			{
				p = 0;
				break;
			}

		if(p)
			prime[a] = 1;
	}
	int c = 0;
	for(int a=0;a<500;a++)
	{
		if(prime[a])
			c++;
		mul[a] = c;
	}


	int T;
	int R, C;
	cin>>T;

	for(int t=0; t<T;t++)
	{	
		cin>>R>>C;
		for(int r=0; r<R; r++)
			for(int c=0; c<C; c++)
				cin>>array[r][c];

		for(int r=0;r<R; r++)
		{
			int cmb = 0;
			for(int c=0;c<C;c++)
			{
				if(array[r][c] == '#')
					cmb = 0;
				else
					cmb++;
				ar[r][c] = cmb-1;
			}
		}

		for(int r=0;r<R;r++)
		{
			int cmb = 0;
			for(int c=C-1;c>= 0 ;c--)
			{
				if(array[r][c] == '#') cmb = 0; else cmb++;
				ar[r][c] = min(ar[r][c], cmb-1);
			}
		}

		
		for(int c=0;c<C;c++)
		{
			int cmb = 0;
			for(int r=R-1;r>=0; r--)
			{
				if(array[r][c] == '#') cmb = 0; else cmb++;
				ar[r][c] = min(ar[r][c], cmb-1);
			}
		}
		int sum = 0;

		for(int c=0;c<C;c++)
		{
			int cmb = 0;
			for(int r=0;r<R; r++)
			{
				if(array[r][c] == '#') cmb = 0; else cmb++;
				ar[r][c] = min(ar[r][c], cmb-1);
				sum += mul[ar[r][c]];
			}
		}
		// for(int r=0; r<R; r++)
		// {
		// 	for(int c=0; c<C; c++)
		// 		cout<<ar[r][c];

		// 	cout<<endl;
		// }
		cout<<sum<<"\n";

	}

	return 0;
}