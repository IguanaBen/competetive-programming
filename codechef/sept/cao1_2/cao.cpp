#include <iostream>

using namespace std;


char array[503][503];


bool check(int i, int j)
{

	for(int x=-2; x<3; x++)
		if(array[i + x][j] == '#' || array[i][j + x] == '#')
			return false;

	return true;
}

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int T;
	int R, C;
	cin>>T;

	for(int t=0; t<T;t++)
	{
		cin>>R>>C;
		for(int r=0; r<R;r++)
			for(int c=0; c<C;c++)
				cin>>array[r][c];

		int sum = 0;
		for(int r=2; r<R-2;r++)
			for(int c=2;c<C-2;c++)
			{
				sum += check(r,c);
			}

		cout<<sum<<"\n";
	}

	return 0;
}