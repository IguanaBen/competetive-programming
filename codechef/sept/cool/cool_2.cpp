#include <iostream>
#include <math.h>

#define type long long

using namespace std;

type gcd(type a, type b)
{

	if( b == 0)
		return a;

	if( a == 0)
		return b;

	return gcd(b, a % b);

}

int main()
{
	cin.tie(NULL);
	std::ios::sync_with_stdio(false);

	int t;
	cin>>t;
	for(int i=0; i<t; i++)
	{
		type n;
		cin>>n;
		type sq = sqrt(n);
		type sum =0;

		int index =1;
		while(n/index > sq)
		{
			sum += n/index;
			index++;
		}

		while(sq > 0)
		{
			sum += (n/sq-n/(sq+1))*sq;
			sq--;
		}

		type np2 = n*n;

		type gc = gcd(sum, np2);
		cout<<sum/gc<<"/"<<np2/gc<<"\n";

	}

	return 0;
}