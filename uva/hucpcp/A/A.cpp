#include <iostream>
#include <sstream>
#include <algorithm>

// zadanie jest uszkodzone
// ' aba' - akceptuje 3

using namespace std;

pair<int, int> solve(string str, int k)
{

	int res = 10000;
	int max_length = 0;

	//i-pivot
	for(int i=0; i<str.length(); i++)
	{
		int ck = k;

		int index1 = i;
		int index2 = i;

		int length = 1;
		index1--;
		index2++;

		int beg = i;

		while(ck >= 0 && index1 >= 0 && index2 < str.length() )
		{
			
			bool br = 0;
			int spaces = 0;
			while(str[index1] < 'a' || str[index1] > 'z')
			{
				index1--;
				length++;
				beg--;

				if(index1 < 0)
				{
					br = 1;
					break;
				}
			}

			if(br)
				break;


			while(str[index2] < 'a' || str[index2] > 'z')
			{
				index2++;
				length++;
				if(index2  >= str.length())
				{
					br = 1;
					break;
				}
			}

			if(br)
				break;

			// cout<<str[index1]<<" "<<str[index2]<<" "<<length<<" "<<ck<<endl;
			// cout<<index1<<" "<<index2<<" "<<length<<" "<<ck<<endl;


			if(str[index1] >= 'a' && str[index1] <= 'z' 
				&&  str[index2] >= 'a' && str[index2] <= 'z'  )
			{

				if(str[index1] != str[index2])
					if( ck > 0 )
						ck--;
					else
						break;

				length += 2;
				beg--;
				index1--;
				index2++;
			}
		}

		while(str[beg] < 'a' || str[beg] > 'z')
		{
			beg++;
			length--;

			if(beg >= str.length())
				break;
		}
		int end = beg + length-1;

		while(str[end] < 'a' || str[end] > 'z')
		{
			end--;
			length--;
		}

		// for(int z = beg; z< beg+length; z++)
		// 	cout<<str[z];

		// cout<<endl;


		if( length > max_length)
		{
			res = beg + 1;
			max_length = length;
		}

		// cout<<beg<<" "<<end<<endl;
		// cout<<beg<<" "<<length<<endl;
	}

	// cout<<max_length<<" "<<res<<endl;
	return make_pair(max_length, res);
	// cout<<endl;
}

pair<int, int> solve2(string str, int k)
{

	int res = 10000;
	int max_length = 0;

	//i-pivot
	for(int i=0; i<str.length()-1; i++)
	{
		int ck = k;

		int index1 = i;
		int index2 = i+1;

		int length = 0;
		// index1--;
		// index2++;

		int beg = i+1;

		while(ck >= 0 && index1 >= 0 && index2 < str.length() )
		{
			
			bool br = 0;
			int spaces = 0;
			while(str[index1] < 'a' || str[index1] > 'z')
			{
				index1--;
				length++;
				beg--;

				if(index1 < 0)
				{
					br = 1;
					break;
				}
			}

			if(br)
				break;


			while(str[index2] < 'a' || str[index2] > 'z')
			{
				index2++;
				length++;
				if(index2  >= str.length())
				{
					br = 1;
					break;
				}
			}

			if(br)
				break;

			// cout<<str[index1]<<" "<<str[index2]<<" "<<length<<" "<<ck<<endl;
			// cout<<index1<<" "<<index2<<" "<<length<<" "<<ck<<endl;


			if(str[index1] >= 'a' && str[index1] <= 'z' 
				&&  str[index2] >= 'a' && str[index2] <= 'z'  )
			{

				if(str[index1] != str[index2])
					if( ck > 0 )
						ck--;
					else
						break;

				length += 2;
				beg--;
				index1--;
				index2++;
			}
		}

		while(str[beg] < 'a' || str[beg] > 'z')
		{
			// cout<<beg<<endl;
			beg++;
			length--;

			if(beg >= str.length())
				break;
		}
		int end = beg + length-1;

		while(str[end] < 'a' || str[end] > 'z')
		{
			end--;
			length--;
		}

		// for(int z = beg; z< beg+length; z++)
		// 	cout<<str[z];

		// cout<<endl;

		if( length > max_length)
		{
			res = beg + 1;
			max_length = length;
		}

		// cout<<beg<<" "<<length<<endl;
	}

	return make_pair(max_length, res);
	// cout<<max_length<<" "<<res<<endl;
	// cout<<endl;
}

string convert(string str)
{
	string res = "";

	std::transform(str.begin(), str.end(), str.begin(), ::tolower);

	// for(int i=0; i<str.length(); i++)
	// {
	// 	if(str[i] >= 'a' && str[i] <= 'z')
	// 	{
	// 		res += str[i];
	// 	}
	// }

	return str;
}

int main()
{
	// cin.tie(NULL);
	// std::ios::sync_with_stdio(false);

	std::string num, line;
	while (std::getline(std::cin, num))
	{
		// cout<<"S";
		std::stringstream ss(num);
		int k = 0;
		ss>>k;
		std::getline(std::cin, line);
		// cout<<k<<" "<<line<<" "<<line[line.length()-1] <<endl;
		// cout<<convert(line)<<endl;
		pair<int, int> p1 = solve(convert(line), k);
		pair<int, int> p2 = solve2(convert(line), k);
		if(p1.first == p2.first)
		{
			if( p2.second < p1.second)
			{
				cout<<p2.first<<" "<<p2.second<<"\n";
			}
			else
				cout<<p1.first<<" "<<p1.second<<"\n";
		}
		else
		{
			if(p1.first > p2.first)
				cout<<p1.first<<" "<<p1.second<<"\n";
			else
				cout<<p2.first<<" "<<p2.second<<"\n";
		}
		// solve("racecat", k);	
	}

	return 0;
}