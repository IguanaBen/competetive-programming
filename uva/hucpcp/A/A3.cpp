#include <iostream>
#include <sstream>
#include <algorithm>
#include <map>

using namespace std;
// int tab[1001][1001][201];

int valL[1003];
int valR[1003];
int sum[1003];

string convert(string str)
{
	string res = "";

	std::transform(str.begin(), str.end(), str.begin(), ::tolower);

	int cmb = 0;
	int ind = 0;
	for(int i=0; i<str.length(); i++)
	{
		if(str[i] >= 'a' && str[i] <= 'z')
		{
			cmb++;
			valR[ind] = cmb;
			cmb = 0;
			ind++;
			res += str[i];
		}
		else
		{
			cmb++;
		}
	}

	cmb = 0 ;
	ind--;
	// ind = str.length()-1;
	for(int i=str.length()-1; i >= 0; i--)
	{
		if(str[i] >= 'a' && str[i] <= 'z')
		{
			cmb++;
			// cout<<"sd";
			valL[ind] = cmb;

			// if(id)
				// sum[ind] = valL[ind];
			cmb = 0;
			ind--;
			// res += str[i];
		}
		else
		{
			cmb++;
		}
	}

	sum[0] = valL[0];
	for(int i = 1; i<res.length(); i++)
		sum[i] = sum[i-1] + valL[i];

	return res;
}

// a....a

int solve(string str, int k)
{
	string st = convert(str);

	// for(int i=0; i<st.length(); i++)
	// 	cout<<valL[i];

	// cout<<endl;

	// for(int i=0; i<st.length(); i++)
	// 	cout<<valR[i];

	// cout<<endl;

	// cout<<st<<endl;


	int max_length = 0;
	int start = 0;

	//odd
	for(int i=0; i< st.length(); i++)
	{
		int ck = k;
		int ind1 = i;
		int ind2 = i;
		int length = 1;
		int star = sum[i] - valL[i];
		ind1--;
		ind2++;

		if( st[i] >= 'a' && st[i] <= 'z')
		{
			while(ind1 >= 0 && ind2 < st.length() && ck>=0 )
			{
				if(st[ind1] == st[ind2])
				{
					length += (valL[ind1] + valR[ind2]);
					// cout<<"S: "<<star<<" "<<i<<endl;
					star -= valL[ind1];
					// cout<<ind1<<" "<<ind2<<endl;
					// cout<<valL[ind1]<<" "<<valR[ind2]<<endl;
				}
				else
				{
					ck--;
					if( ck >= 0 )
					{
						length += (valL[ind1] + valR[ind2]);
						star -= valL[ind1];
					}
					else
						break;
				}

				ind1--;
				ind2++;

			}
		}

		if(length > max_length)
		{
			max_length = length;
			start = star;
		}
	}

	//even
	for(int i=0; i< st.length()-1; i++)
	{
		int ck = k;
		int ind1 = i;
		int ind2 = i;
		int length = 0;
		int star = sum[i];
		// ind1;
		ind2++;

		// if()


		if(ind1 >=0 && ind2 < st.length())
		{

			/*if( st[ind1] >= 'a' && st[ind1] <= 'z' && st[ind2] >= 'a' && st[ind2] <= 'z')
			{*/
				while(ind1 >= 0 && ind2 < st.length() && ck>=0 )
				{
					if(st[ind1] == st[ind2])
					{
						// cout<<ind1<<" "<<ind2<<endl;;
						if(ind1 == ind2-1)
							length += ( valL[ind1] + 1 );
						else
							length += (valL[ind1] + valR[ind2]);

						// cout<<"S: "<<star<<" "<<i<<endl;
						star -= valL[ind1];
						// cout<<ind1<<" "<<ind2<<endl;
						// cout<<valL[ind1]<<" "<<valR[ind2]<<endl;
					}
					else
					{
						ck--;
						if( ck >= 0 )
						{
							
							if(ind1 == ind2-1)
								length += ( valL[ind1] + 1 );
							else
								length += (valL[ind1] + valR[ind2]);
							
							star -= valL[ind1];
						}
						else
							break;
					}

					ind1--;
					ind2++;

				}
			// }

		}


		if(length > max_length)
		{
			max_length = length;
			start = star;
		}

	}

	cout<<max_length<<" "<<start+1<<"\n";

}

int main()
{
	// cin.tie(NULL);
	// std::ios::sync_with_stdio(false);

	std::string num, line;
	while (std::getline(std::cin, num))
	{
		// cout<<"S";
		std::stringstream ss(num);
		int k = 0;
		ss>>k;
		std::getline(std::cin, line);
		solve(line, k);
		// cout<<res(0, line.length()-1, k, line);
		// cout<<k<<" "<<line<<" "<<line[line.length()-1] <<endl;
			
	}

	return 0;
}