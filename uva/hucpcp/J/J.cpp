#include <iostream>
#include <sstream>
#include <algorithm>

using namespace std;

#define type long long int


int main()
{
	// cin.tie(NULL);
	// std::ios::sync_with_stdio(false);


	std::string num, line;
	int caseNum = 1;
	while (std::getline(std::cin, num))
	{
		std::stringstream ss(num);
		int x, y;
		ss>>x>>y;

		int nm = 0;
		// cout<<x<<y<<endl;

		for(type i=x; i <= min(y, 1006); i++)
			for(type j=x; j<= min(y,1006);j++)
			{
				// cout<<i<<" "<<j<<endl;
				type res = i*i*i + j*j*j;
				type temp = res/10;
				// cout<<res - temp*10 <<endl;
				if(res - temp*10 == 3)
				{
					if(temp <= y)
					{
						nm++;
					}
				}
			}
		cout<<"Case "<<caseNum<<": "<<nm<<"\n";
		
	}

	return 0;
}