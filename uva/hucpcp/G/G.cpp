#include <iostream>
#include <sstream>
#include <algorithm>
#include <map>

using namespace std;


string student[103];
int dist[103][2];


int main()
{
	for(int j=0; j<100;j++)
	{
		dist[j][0] = 1000;
		dist[j][1] = 1000;
	}


	int n;
	cin>>n;

	for(int i=0; i<n; i++)
	{
		string t;
		cin>>t;
		student[i] = t;

	}

	int c_dist = 1000;
	for(int i=0; i<n; i++)
	{
		if(student[i] != "?")
			c_dist = 0;

		dist[i][0] = c_dist;
		c_dist++;
	}

	c_dist = 1000;
	for(int i=n-1; i>=0; i--)
	{
		if(student[i] != "?")
			c_dist = 0;

		dist[i][1] = c_dist;
		c_dist++;
	}


	int q;
	cin>>q;

	for(int i=0; i<q;i++)
	{
		int t;
		cin>>t;

		if(dist[t-1][0] == dist[t-1][1])
		{
			if(dist[t-1][0] == 0)
				cout<<student[t-1]<<"\n";
			else
				cout<<"middle of "<<student[t-1 - dist[t-1][0]]<<" and "<<student[t-1 + dist[t-1][1]]<<"\n";
		}
		else
		{
			if(dist[t-1][0] < dist[t-1][1])
			{
				for(int z=0; z<dist[t-1][0]; z++)
					cout<<"right of ";

				cout<<student[t-1 - dist[t-1][0]]<<"\n";
			}
			else
			{
				for(int z=0; z<dist[t-1][1]; z++)
					cout<<"left of ";

				cout<<student[t-1 + dist[t-1][1]]<<"\n";

			}

		}
	}

	return 0;
}