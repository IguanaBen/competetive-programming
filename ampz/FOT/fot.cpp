#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type_u unsigned long long
#define type long long


using namespace std;

int deg[1000043];

vector<int> edges[1000033];

map<int, int> deg_map;

int outt[1000033];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n,k;
  cin>>n>>k;

  for(int i=0; i<n-1; i++)
  {
  	int t1, t2;
  	cin>>t1>>t2;
    edges[t1].push_back(t2);
    edges[t2].push_back(t1);
    deg[t1]++;
    deg[t2]++;
  }

  queue<int> deg1;

  for(int i=1; i<=n;i++)
  {
  	if(edges[i].size() == 1 )
  		deg1.push(i);
  }

  

  while(k > 1)
  {

  	int size = deg1.size();

  	for(int i=0; i<size; i++)
  	{

  		int leaf = deg1.front();
  		deg1.pop();
  		outt[leaf] = 1;

  		for(int j=0; j<edges[leaf].size(); j++)
  		{
  			if( outt[edges[leaf][j]] == 0)
  			{
  				deg[edges[leaf][j]]--;
  				if(deg[edges[leaf][j]] == 1)
  				{
  					deg1.push(edges[leaf][j]);
  				}
  			}
  		}
  	}
  	k-=2;
  }

  if(k == 1)
  {
  	for(int i=0; i<n; i++)
  	{
  		if(outt[i+1] == 0)
  		{
  			outt[i+1] = 1;
  			break;
  		}
  	}
  }

  int num = 0;

  for(int i=0; i<n; i++)
  	{
  		if(outt[i+1])
  		{
  			num++;
  		}
  	}

  	cout<<num<<"\n";

  for(int i=0; i<n; i++)
  {
  	if(outt[i+1])
  		cout<<i+1<<" ";
  }

  cout<<"\n";

  return 0;
 
}