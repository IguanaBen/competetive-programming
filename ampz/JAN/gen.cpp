#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type_u unsigned long long
#define type long long


using namespace std;


int gen(int i)
{
	if( i == 1)
		return 0;

 	return (i % 2) + 2*gen(i/2);
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;
  type sum = 0;
  for(int i=1; i<=n; i++)
  {
  	// cout<<i<<" "<<gen(i)<<endl;
  	sum+= gen(i);
  }

  cout<<endl<<sum<<endl;

  return 0;
 
}