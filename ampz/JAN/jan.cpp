#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type_u unsigned long long
#define type long long


using namespace std;

type vals[300];
type pow2[300];

type sm(type i)
{
	return i*(i+1)/2;
}

int main()
{

	pow2[0] = 1;
	pow2[1] = 2;

	vals[1] = 0;
	vals[2] = 1;

	for(int i=2; i< 65;i++)
	{
		pow2[i] = 2*pow2[i-1];
		vals[i] = sm(pow2[i]/2 - 1) + vals[i-1];
	}
	// cout<<pow2[18]<<endl;
  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  type n;
  cin>>n;

  int ind = 0;

  while(pow2[ind] < n)
  {
  	ind++;
  }



  type dif = n-pow2[ind];

  if(dif < 0)
  {
  	ind--;
  	dif = n-pow2[ind];
  }

  // cout<<dif<<" "<<vals[ind]<<"\n";
  type res = vals[ind] + sm(dif);
  cout<<res<<"\n";

  return 0;
 
}