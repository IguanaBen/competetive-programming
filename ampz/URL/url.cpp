#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type unsigned long long

using namespace std;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  string str;
  int n;
  cin>>n;

  cin>>str;

  int cmb = 0;
  int max_c = 0;

  for(int i=0; i<n; i++)
  {
  	if(str[i] == 'P')
  	{
  		cmb = 0;
  	}
  	else
  	{
  		cmb++;
  		max_c = max(max_c, cmb);
  	}
  }

  cout<<max_c<<"\n";

  return 0;
 
}