#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type  long long

using namespace std;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;
  type a, b;

  for(int i=0; i<n; i++)
  {
  	cin>>a>>b;
  	cout<<a+b<<"\n";
  }

  return 0;
 
}