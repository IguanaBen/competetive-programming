#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <algorithm>

#define type_u unsigned long long
#define type long long


using namespace std;

struct sect
{	
	int ind;
	int val;
	bool oc;

};

sect sections[1000033];

const type dd = 1000000009;

type tab[1000033];

bool cmp(sect s1, sect s2)
{
	if(s1.val == s2.val)
		return s1.oc > s2.oc;
	else
		return s1.val < s2.val;
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  cin>>n;

  map<type, type> vals;

  int indz = 1;
  for(int i=0; i<2*n; i+=2)
  {
  	type t1, t2;
  	cin>>t1>>t2;
  	sections[i].val = t1;
  	sections[i].oc = 0;

  	sections[i+1].val = t2;
  	sections[i+1].oc = 1;

  	sections[i].ind = indz;
  	sections[i+1].ind = indz;

  	indz++;

  }

  int nn = 2*n;
  sort(sections, sections + nn, cmp);

  set<int> open;

  int open_n = 0;

  int last = sections[0].val;

  set<int>::iterator it;

  for(int i=0; i<nn; i++)
  {
  	// while(sections[i].oc == 1)

  	// cout<<sections[i].ind<<" "<<sections[i].oc<<" "<<sections[i].val<<endl;
  	

  	// cout<<last<<endl;
  	int dif = sections[i].val - last;
  	
  	last = sections[i].val;

  	// cout<<dif<<endl;

  	if(dif != 0)
  	{
  		// cout<<"DIF\n";
	  	if(open_n == 1)
	  	{
	  		it=open.begin();

	  		int open_1 = *it;
	  		// cout<<"O: "<<open_1<<endl;
	  		vals[open_1] += dif;
	  	}

	  	if(open_n == 2)
	  	{
	  		it=open.begin();

	  		int open_1 = *it;

	  		it++;
	  		int open_2 = *it;

	  		type indx = 0;

	  		if(open_1 < open_2)
				indx = open_1 + open_2*dd;
			else
				indx = open_2 + open_1*dd;

			// cout<<"IND: "<<indx<<endl;

			// cout<<"O: "<<open_1<<" "<<open_2<<endl;

	  		vals[indx] += dif;

	  	}

  	}

  	if(!sections[i].oc)
  	{
  		open_n++;
  		// cout<<"I: "<<sections[i].ind<<endl;
  		open.insert(sections[i].ind);

  		// for (it=open.begin(); it!=open.end(); ++it)
    // 		std::cout << ' ' << *it;

    // 	cout<<endl;
  	}
  	else
  	{
  		open_n--;
  		// cout<<"E: "<<sections[i].ind<<endl;
  		it = open.find(sections[i].ind);
  		open.erase(it);

  		// for (it=open.begin(); it!=open.end(); ++it)
    // 		std::cout << ' ' << *it;

    // 	cout<<endl;
  	}
  	// cout<<endl;

  }

  // cout<<vals[102301203]<<endl;

  map<type, type>::iterator iterr;

  int numc = 0;
  int numk = 0;

  type mx = 0;

  for(iterr= vals.begin(); iterr != vals.end(); ++iterr)
  {
  	if(iterr->first >= dd)
  	{
  		// cout<<iterr->first<<" "<<iterr->second<<endl;
  		type o1 = iterr->first % dd ;;
  		type o2 = (iterr->first - o1)/dd;;
  		// cout<<o1<<" "<<o2<<endl;

  		// cou
  		// cout<<vals[o2]<<" "<<vals[o1]<<endl;
  		type rr = iterr->second + (vals[o1]+vals[o2]);
  		// cout<<rr<<endl;
  		mx = max(rr, mx);
  		numc++;
	}
	else
	{
		// cout<<"Sd";
		tab[numk] = iterr->second;
		numk++;
	}
  }

  // cout<<vals[1]<<endl;



  	if(numk > 0)
  	{
  		sort(tab, tab+numk);

  		if(numk > 1)
  			mx = max(tab[numk-1]+tab[numk-2], mx);
  		else
  			mx = max(tab[numk-1], mx);
  	}

  	cout<<mx<<"\n";

  return 0;
 
}