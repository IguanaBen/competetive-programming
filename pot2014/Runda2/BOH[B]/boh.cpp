#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

pair<pair<type, type>, int > tab1[100033];
pair<pair<type, type>, int >  tab2[100033];

// int res[100033];

bool cmp1(pair<pair<type, type>, int > p1, pair<pair<type, type>, int > p2)
{
	return p1.first.first < p2.first.first;
}

bool cmp2(pair<pair<type, type>, int > p1, pair<pair<type, type>, int > p2)
{
	return (p1.first.second > p2.first.second);
}


int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int n;
  type z;
  cin>>n>>z;

  int n1, n2;
  n1 = 0;
  n2 = 0;

  for(int i=0; i<n; i++)
  {
  	type t1, t2;
  	cin>>t1>>t2;
  	if(t1 <= t2 )
  		tab1[n1++] = make_pair(make_pair(t1,t2), i+1);
  	else
  		tab2[n2++] = make_pair(make_pair(t1,t2), i+1);
  }

  sort(tab1, tab1 + n1, cmp1);
  sort(tab2, tab2 + n2, cmp2);


  // int t = 0;
  // cout<<n1<<" "<<n2<<"\n";
  bool ok = true;
  for(int i=0; i<n1 && ok; i++)
  {
  	type x = tab1[i].first.first;
  	type y = tab1[i].first.second;
  	z -= x;
  	if(z > 0)
  		z += y;
  	else
  		ok = false;
  }

  for(int i=0; i<n2 && ok; i++)
  {
  	type x = tab2[i].first.first;
  	type y = tab2[i].first.second;
  	z -= x;
  	if(z > 0)
  		z += y;
  	else
  		ok = false;
  }

  if(ok)
  {
  	cout<<"TAK\n";
  	
  	for(int i=0; i<n1; i++)
  		cout<<tab1[i].second<<" ";

  	for(int i=0; i<n2; i++)
  		cout<<tab2[i].second<<" ";

  	cout<<"\n";

  }
  else
  	cout<<"NIE\n";

  return 0;
 
}