#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

pair<pair<int, int>, int> tab1[50033];
pair<pair<int, int>, int> tab2[50033];

int pos1[50033];
int pos2[50033];

bool cmpX(pair<pair<int, int>, int> p1, pair<pair<int, int>, int> p2)
{
	return p1.first.first < p2.first.first;
}

const int last_level = 65536;

int qtree[131079];

void reset_tree()
{
	for(int i=0; i<131079; ++i) qtree[i] = 0;
}

void build_tree()
{
	for(int i=65536-1; i>0; --i)
		qtree[i] = max(qtree[i<<1], qtree[(i<<1) + 1]);
}

int get_max(int n)
{
	if(n < 0)
		return 0;

	int index = n + last_level;
	int mx = qtree[index];

	while(index > 1)
	{
		if(index % 2)
			mx = max(qtree[index-1], mx);
		
		index >>= 1;
	}

	return mx;
}

void del_value(int n)
{
	int index = n + last_level;
	qtree[index] = 0;
	index >>= 1;
	while(index > 0)
	{
		qtree[index] = max(qtree[index<<1], qtree[(index<<1) + 1]); 
		index >>= 1;
	}
}

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int T;
  cin>>T;

  while(T--)
  {
  	int n, w;
  	cin>>n>>w;

  	for(int i=0; i<n; ++i)
  	{
  		int x1, x2, y1,y2;	
  		cin>>x1>>y1>>x2>>y2;
  		int X = min(x1, x2);
  		int H = abs(y1-y2);
  		tab1[i].first.first = X;
  		tab1[i].first.second = H;
  		tab1[i].second = i;
  	}

  	for(int i=0; i<n; ++i)
  	{
  		int x1, x2, y1,y2;	
  		cin>>x1>>y1>>x2>>y2;
  		int X = min(x1, x2);
  		int H = abs(y1-y2);
  		tab2[i].first.first = X;
  		tab2[i].first.second = H;
  		tab2[i].second = i;
  	}

  	sort(tab1, tab1 + n, cmpX);
  	sort(tab2, tab2 + n, cmpX);

  	reset_tree();

  	for(int i=0; i<n; i++) 
  	{	
  		pos1[tab1[i].second] = i;
  		qtree[last_level + i] = tab1[i].first.second;
  	}

  	build_tree();

  	bool bb = true;

  	for(int i=0; i<n && bb; i++)
  	{
  		int pos = pos1[tab2[i].second];
  		int mx = get_max(pos-1);
  		del_value(pos);

  		if( w - mx < tab2[i].first.second)
  			bb = false;
  	}
  	
  	if(bb)
  		cout<<"TAK\n";
  	else
  		cout<<"NIE\n";
  }
 
}