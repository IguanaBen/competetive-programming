#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

bool out[100033];

type tab[100033][4];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  int t;
  cin>>t;
  
  while(t--)
  {


  	int n;

  	cin>>n;

  	for(int i=0; i<n; i++)
  		out[i] = 0;

  	for(int i=0; i<n; i++)
  		cin>>tab[i][0]>>tab[i][1]>>tab[i][2]>>tab[i][3];

  	for(int j=0; j<2; j++)
  	{
  		type mn = 1000000033;
  		for(int i=0; i<n; i++)
  			mn = min(mn, tab[i][0 + j*2]);

  		for(int i=0; i<n; i++)
  			if(tab[i][0+j*2] > mn)
  				out[i] = 1;
  	}

  	for(int j=0; j<2; j++)
  	{
  		type mx = -3;
  		for(int i=0; i<n; i++)
  			mx = max(mx, tab[i][1 + j*2]);

  		for(int i=0; i<n; i++)
  			if(tab[i][1+j*2] < mx)
  				out[i] = 1;
  	}

  	type cc = 0;
  	for(int i=0; i<n; i++)
  		cc += out[i];

  	if(cc == n)
  		cout<<"NIE\n";
  	else
  		cout<<"TAK\n";

  }


  return 0;
 
}