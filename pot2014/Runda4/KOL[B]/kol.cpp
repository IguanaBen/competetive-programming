#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>
#include <set>

#include "message.h"
#include "kollib.h"

#define type long long

using namespace std;


set<int> get_differentRands(int n, int mx)
{
	set<int> elements;

	for(int i=0; i<n; i++)
	{
		int new_rand = 1 + (rand() % mx);
		elements.insert(new_rand);
	}

	int new_rand = 1;

	while(elements.size() < n)
	{
		elements.insert(new_rand);
		new_rand++;
	}

	return elements;
}

struct returnValue
{
	int left, me, right;
};


int separators[142];
int order[142];
bool revers[142];

returnValue vals[1000];

map<int, int> nodeMapsL[144];
map<int, int> nodeMapsR[144];

int qFrom[242];
int qTo[242];

int rorder[242];

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  srand (time(NULL));

  int n = NumberOfStudents();
  int m = NumberOfQueries();

  map<int, int> mapa;
  set<int> ends;

  for(int i=1; i<=m; i++)
  {
  	int qf = QueryFrom(i);
  	int qt = QueryTo(i);
  	mapa[ qf ] = qt;
  	ends.insert(qf);
  	ends.insert(qt);
  	qFrom[i] = qf;
  	qTo[i] = qt;
  }

  int nodes = min(NumberOfNodes(), n);
  int myId = MyNodeId();

  if(myId >= n)
  	return 0;


  set<int> sep = get_differentRands( nodes, n);

  // for(int i=0;i<sep.size(); i++)
  // {
  // 	cout<<myId<<" "<<sep[i]<<"\n";
  // }

  // if(n < 10)
  	// sep = get_differentRands(1, n);


  set<int>::iterator it = sep.begin();
  int i = 0;
  while(it != sep.end())
  {
  	// cout<<myId<<" "<<(*it)<<"\n";
  	separators[i++] = (*it);
  	it++;
  }

  map<int, int> distancesL;
  map<int, int> distancesR;


  int left = FirstNeighbor(separators[myId]);
  int right = SecondNeighbor(separators[myId]);
  int prev_left = separators[myId];
  int prev_right = separators[myId];

  int left_distance = 0;
  int right_distance = 0;

  if(ends.find(separators[myId]) != ends.end())
  	distancesL[separators[myId]] = 0;

  if(ends.find(left) != ends.end())
  		distancesL[left] = left_distance+1;

  if(sep.find(left) == sep.end())
  	distancesL[left] = left_distance+1;

  while( sep.find(left) == sep.end())
  {
  	// cout<<(sep.find(left) != sep.end())<<"\n";
  	left_distance++;

  	if(ends.find(left) != ends.end())
  		distancesL[left] = left_distance;


  	if(FirstNeighbor(left) == prev_left)
  	{
  		prev_left = left;
  		left = SecondNeighbor(left);
  	}
  	else
  	{
  		prev_left = left;
  		left = FirstNeighbor(left);
  	}

  	// if(myId == 0)
  		// cout<<left<<"\n";

  }

  if(ends.find(right) != ends.end())
  		distancesR[right] = right_distance+1;

  if(sep.find(right) == sep.end())
  	distancesR[right] = right_distance+1;

  while(sep.find(right) == sep.end())
  {
  	right_distance++;

  	if(ends.find(right) != ends.end())
  		distancesR[right] = right_distance;

  	if(FirstNeighbor(right) == prev_right)
  	{
  		prev_right = right;
  		right = SecondNeighbor(right);
  	}
  	else
  	{
  		prev_right = right;
  		right = FirstNeighbor(right);
  	}

  }

  // if(left_distance != 0)
  	distancesL[left] = left_distance+1;
  
  distancesR[right] = right_distance+1;

  // returnValue retVal;
  // retVal.left = left;
  // retVal.me = separators[myId];
  // retVal.right = right;

  int me= separators[myId];

  // if(myId == 0)
  // {
  // 	for(int i=0; i<nodes; i++)
  // 	{
  // 		// cout<<separators[i]<<"\n";
  // 	}
  // }


  // int n_sep = 

  // if(n >= 0)
  // {

  if(myId != 0)
  {
  	PutInt(0, left);
  	// Send(0);
  	PutInt(0, me);
  	// Send(0);
  	PutInt(0, right);
  	PutInt(0, distancesL.size());
  	PutInt(0, distancesR.size());
  	Send(0);

  	map<int, int>::iterator iter = distancesL.begin();
  	while(iter != distancesL.end())
  	{
  		PutInt(0, iter->first);
  		PutInt(0, iter->second);
  		iter++;
  	}

  	iter = distancesR.begin();

    while(iter != distancesR.end())
  	{
  		PutInt(0, iter->first);
  		PutInt(0, iter->second);
  		iter++;
  	}
  	Send(0);


  }
  else
  {
  	// cout<<left<<" "<<me<<" "<<right<<"\n";

  	vals[0].left = left;
  	vals[0].me = me;
  	vals[0].right = right;

  	map<int, int>::iterator iter = distancesL.begin();
  	while(iter != distancesL.end())
  	{
  		nodeMapsL[0][iter->first] = iter->second;
  		iter++;
  	}

  	iter = distancesR.begin();

    while(iter != distancesR.end())
    {
  		nodeMapsR[0][iter->first] = iter->second;
  		iter++;
  	}

  	for(int i=1; i<nodes; i++)
  	{
  		Receive(i);
  		vals[i].left = GetInt(i);
  		// Receive(i);
  		vals[i].me = GetInt(i);
  		// Receive(i);
  		vals[i].right = GetInt(i);
  		int lS = GetInt(i);
  		int rS = GetInt(i);
  		Receive(i);
  		for(int jj = 0; jj<lS; jj++)
  		{
  			int t1 = GetInt(i);
  			int t2 = GetInt(i);
  			// cout<<t1<<" "<<t2<<"\n";
  			nodeMapsL[i][t1] = t2;
  		}
  		// cout<<"|"<<"\n";
  		for(int jj = 0; jj<rS; jj++)
  		{
  			int t1 = GetInt(i);
  			int t2 = GetInt(i);
  			// cout<<t1<<" "<<t2<<"\n";	
  			nodeMapsR[i][t1] = t2;
  		}

  		// cout<<vals[i].left<<" "<<vals[i].me<<" "<<vals[i].right<<"\n";
  	}

  	int curr = me;
  	int cr = right;

  	order[0] = 0;
  	revers[0] = 0;

  	for(int i=0; i<nodes-1; i++)
  	{
  		for(int j=0; j<nodes; j++)
  		{
  			if( vals[j].me != curr)	// ??
  			{
  				if(curr == vals[j].left && cr == vals[j].me)
  				{
  					revers[j] = 0;
  					order[j] = i+1;
  					curr = vals[j].me;
  					cr = vals[j].right;
  					break;
  				}

  				if(curr == vals[j].right && cr == vals[j].me)
  				{
  					revers[j] = 1;
  					order[j] = i+1;

  					curr = vals[j].me;
  					cr = vals[j].left;
  					break;
  				}

  			}
  		}
  	}



  	// for(int j=0; j<nodes; j++)
  		// cout<<vals[j].left<<" "<<vals[j].me<<" "<<vals[j].right<<" "<<order[j]<<":"<<revers[j]<<"\n";

  	rorder[0] = 0;
  	for(int j=1; j<nodes; j++)
  		rorder[order[j]] = j;

  	for(int i=1; i<=m; i++)
  	{
  		int qf = qFrom[i];
  		int qt = qTo[i];
  		map<int, int>::iterator itt;
  		int foundIndex = 0;
  		bool rev = 0;
  		for(int jj=0; jj<nodes ; jj++)
  		{
  			if(revers[jj])
  			{
  				itt = nodeMapsL[jj].find(qf);

  				if(itt != nodeMapsL[jj].end())
  				{
  					foundIndex = jj;
  					rev = 0;
  					break;
  				}
  			}
  			else
  			{
  				itt = nodeMapsR[jj].find(qf);

  				if(itt != nodeMapsR[jj].end())
  				{
  					foundIndex = jj;
  					rev = 1;
  					break;
  				}
  			}
  		}

  		// cout<<foundIndex<<"\	n";

  		int totalDistance = 0;
  		map<int, int>::iterator itx;

  		bool tw = 0;

  		// if(rev)
  		// {
  		//  	itx = nodeMapsL[foundIndex].find(qt);
  		//  	if(itx != nodeMapsL[foundIndex].end())
  		//  	{
  		//  		totalDistance = 
  		//  		abs(nodeMapsL[foundIndex][qt] - nodeMapsL[foundIndex][qf]);
  		//  		totalDistance = min(totalDistance, n-totalDistance);
  		//  		tw = 1;
  		//  	}
  		//  	else
  		//  		tw = 0;
  		// }
  		// else
  		// {
  		//  	itx = nodeMapsR[foundIndex].find(qt);
  		//  	if(itx != nodeMapsR[foundIndex].end())
  		//  	{
  		//  		totalDistance = 
  		//  		abs(nodeMapsR[foundIndex][qt] - nodeMapsR[foundIndex][qf]);
  		//  		totalDistance = min(totalDistance, n-totalDistance);
  		//  		tw = 1;
  		//  	}
  		//  	else
  		//  		tw = 0;
  		// }

  		// if(itx != node)

  		if(!tw)
  		{
  			if(revers[foundIndex])
  		 		totalDistance += 
  		 		(nodeMapsL[foundIndex][vals[foundIndex].left]
  		 				- nodeMapsL[foundIndex][qf] );
  		 	else
  		 		 totalDistance += 
  		 		(nodeMapsR[foundIndex][vals[foundIndex].right]
  		 				- nodeMapsR[foundIndex][qf] );			

  			// cout<<vals[foundIndex].me<<" "<<totalDistance<<"\n";
  			foundIndex = rorder[(order[foundIndex] + 1) % nodes];

  		}

  		while(!tw)
  		{
  			// cout<<vals[foundIndex].me<<" "<<totalDistance<<"\n";
  			if(revers[foundIndex])
  			{
  		 		itx = nodeMapsL[foundIndex].find(qt);
  		 		if(itx != nodeMapsL[foundIndex].end())
  		 		{
  		 			// cout<<"QT: "<<qt<<"\n";
  		 			totalDistance += nodeMapsL[foundIndex][qt];
  		 			break;
  		 		}
  		 		else
  		 			totalDistance += nodeMapsL[foundIndex][vals[foundIndex].left];

  			}
  			else
  			{
  				itx = nodeMapsR[foundIndex].find(qt);
  		 		if(itx != nodeMapsR[foundIndex].end())
  		 		{
  		 			// cout<<"QT: "<<qt<<"\n";

  		 			totalDistance += nodeMapsR[foundIndex][qt];
  		 			break;
  		 		}
  		 		 else
  		 			totalDistance += nodeMapsR[foundIndex][vals[foundIndex].right];
  			}
  			// cout<<totalDistance<<"\n";
  			foundIndex = rorder[(order[foundIndex] + 1) % nodes];
  		}
  			
  		totalDistance = totalDistance % n;

  		totalDistance = min(totalDistance, n-totalDistance);

  		// cout<<"R: "<<tw<<" ";
  		cout<<totalDistance<<"\n";

  		// cout<<"|\n";
  	}

  }

  // }
  // else
  // {

  // }


  // }

  return 0;
}