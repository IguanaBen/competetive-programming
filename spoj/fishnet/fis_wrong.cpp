#include <iostream>
#include <vector>

using namespace std;

vector<int> graph[1042];
bool gr[1042][1042];
bool graph2[1042][1042];

void zero_graphs(int n)
{
	n++;
	for(int i=0; i<n;i++)
	{
		for(int j=0; j<n;j++)
		{
			gr[i][j] = 0;
			graph2[i][j] = 0;
		}

		graph[i].clear();
	}

}

void gen_graph2(int n)
{
	n++;
	for(int i=1; i<n; i++)
		for(int j=0; j<graph[i].size(); j++)
			for(int k=0; k<graph[graph[i][j]].size(); k++)
			{
				graph2[i][ graph[graph[i][j]][k] ] = 1;
				graph2[graph[graph[i][j]][k] ][i] = 1;
			}
}

bool check(int n)
{
	n++;

	for(int i=1; i<n; i++)
		for(int j=i+1; j<n; j++)
			if(graph2[i][j] && !gr[i][j])
				return false;

	return true;
}

int main()
{
	int n,m;
	cin>>n>>m;
	while(n != 0)
	{
		int x, y;
		zero_graphs(n);

		for(int i=0; i<m; i++)
		{
			cin>>x>>y;
			graph[x].push_back(y);
			graph[y].push_back(x);
			gr[x][y] = 1;
			gr[y][x] = 1;
		}

		gen_graph2(n);
		if(check(n))
			cout<<"Perfect\n\n";
		else
			cout<<"Imperfect\n\n";
		cin>>n>>m;

	}
	return 0;
}