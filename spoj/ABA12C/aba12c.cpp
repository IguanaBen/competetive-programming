#include <iostream>
#include <algorithm>

using namespace std;

int test_case = 0;

int visited[102][102][102];
int dp[102][102][102];

int price[102];

int inf = 100000000;

int result(int n, int k, int w)
{

	if(n < 0 || k<0||w<1)
		return inf;

	if(k == 0 && n >=0)
		return 0;	


	if(visited[n][k][w] == test_case)
		return dp[n][k][w];

	visited[n][k][w] = test_case;
	// cout<<n<<" "<<k<<" "<<w<<"\n";
	dp[n][k][w] = min(result(n, k, w-1), result(n-1, k-w, w) + price[w]);
}

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	int t;
	cin>>t;
	while(t--)
	{
		test_case++;
		int n,k;
		cin>>n>>k;
		for(int i=0; i<k; i++)
		{
			cin>>price[i+1];
			if(price[i+1] < 0 )
				price[i+1] = inf;
		}

		int r = result(n,k,k);

		if(r < inf)
			cout<<r<<"\n";
		else
			cout<<"-1\n";
	}

	return 0;
}