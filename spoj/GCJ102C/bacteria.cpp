#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;
vector<int> graph[1042];

bool visited1[1042];
bool visited2[1042];
bool visited3[1042];


IPR lower[1042];
IPR upper[1042];

IPR corner[1042];

// bool in_rec(IPR lower, IPR upper, IPR point)
// {
// 	if( lower.first <= point.first <= upper.first
// 		&&
// 		lower.second <= point.second <= upper.second)
// 		return true;
// 	return false;
// }

bool recCol(IPR L1, IPR U1, IPR L2, IPR U2)
{
	if(L1.first > U2.first || U1.first < L2.first
	  ||
	  L1.second > U2.second || U1.second < L2.second)
		return false;
	return true;

}

IPR left_upper_corner(int current)
{
	visited1[current] = true;

	int resX = lower[current].first;
	int resY = upper[current].second;

	FORI(i, graph[current].size())
	{
		if(!visited1[graph[current][i]])
		{
			IPR t = left_upper_corner(graph[current][i]);
			resX = min(resX, t.first);
			resY = max(resY, t.second);
		}
	}
	return make_pair(resX, resY);
}

int maxX(int current)
{
	visited3[current] = true;

	int res = upper[current].first;

	FORI(i, graph[current].size())
	{
		if(!visited3[graph[current][i]])
		{
			int t = maxX(graph[current][i]);
			res = max(res, t);
		}
	}
	return res;
}

//we can simply find point s.t. p.first + p.second is minimal
//and it lies on the diameter, dont have to know corner at this moment
int max_diam(int current, IPR corner)
{

	IPR res = lower[current];
	
	visited2[current] = true;

	int X = res.first - corner.first;
	int Y = -(res.second - corner.second);

	int cDiff = Y - X;

	int X2 = upper[current].first - corner.first;
	int Y2 = -(upper[current].second - corner.second);

	if(Y2 - X2 > cDiff)
	{
		res = upper[current];
		cDiff = Y2 - X2;
	}

	FORI(i, graph[current].size())
	{
		// cout<<visited2[graph[current][i]]<<"\n";
		if(!visited2[graph[current][i]])
		{
			int t = max_diam(graph[current][i], corner);
			// cout<<t<<"\n";
			// int X2 =. t.first - corner.first;
			// int Y2 = t.first - corner.second;

			if(t > cDiff)
			{
				// res = t;
				cDiff = t;
			}
		}
	}
	return cDiff;
}



int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	int T;
	cin>>T;
	int cas = 1;
	while(T--)
	{

	for(int i=0; i<1042; i++)
	{
		graph[i].clear();
		visited1[i] = false;
		visited2[i] = false;
		visited3[i] = false;

	}

	int R;
	cin>>R;

	FORI(i, R)
	{
		cin>>lower[i].first>>lower[i].second;
		cin>>upper[i].first>>upper[i].second;

		// lower[i].first = lower[i].first;
		// lower[i].second = -lower[i].second;
		// upper[i].first = -upper[i].first;
		// upper[i].second = -upper[i].second;	

	}

	FORI(i, R)
	{
		FORI(j, R)
		{
			if(i != j)
			{

				IPR lp1 = make_pair(lower[i].first, lower[i].second -1);
				IPR up1 = make_pair(upper[i].first, upper[i].second + 1);

				IPR lp2 = make_pair(lower[i].first - 1, lower[i].second);
				IPR up2 = make_pair(upper[i].first + 1, upper[i].second);

				IPR lpp = make_pair(lower[i].first -1, upper[i].second + 1);
				IPR upp = make_pair(upper[i].first +1, lower[i].second - 1);

				
				// if(in_rec(lp, up, lower[j]) || in_rec(lp, up, upper[j]) )
				if(
					recCol(lp1, up1, lower[j], upper[j])||
					recCol(lp2, up2, lower[j], upper[j])||
					recCol(lpp, lpp, lower[j], upper[j])||
					recCol(upp, upp, lower[j], upper[j])
					)
				{
					// cout<<i<<" "<<j<<"\n";
					graph[i].push_back(j);
				}
			}
		}
	}

	int result = 0;

	FORI(i, R)
	{
		if(!visited1[i])
		{
			// cout<<"NV: "<<i<<"\n";
			corner[i] = left_upper_corner(i);
			int d = max_diam(i, corner[i]);
			int mx = maxX(i);
			// cout<<corner[i].first<<" "<<mx-corner[i].second<<" "<<mx<<" "<<d<<"\n";
			result = max(result, (mx-corner[i].first) + d + 1);
			// cout<<(mx-corner[i].first) + d + 1<<"\n";

		}
	}
	// cout<<result<<"\n";

	cout<<"Case #"<<cas++<<": "<<result<<"\n";

	}
	// cout<<"\n";

	/*FORI(i, R)
	{
		if(!visited2[i])
		{
			int d = max_diam(i, corner[i]);
			cout<<d<<"\n";
			// IPR d= max_diam(i, corner[i]);
			// cout<<d.first<<" "<<d.second<<"\n";
			// corner[i] = left_lower_corner(i);
			// cout<<corner[i].first<<" "<<corner[i].second<<"\n";
		}
	}*/

	return 0;
}