#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <map>
#include <queue>
#include <algorithm>

#define type long long

using namespace std;

int main()
{

  cin.tie(NULL);
  std::ios::sync_with_stdio(false);

  type n;
  cin>>n;
  while(n != 42)
  {
  	cout<<n<<"\n";
  	cin>>n;
  }

  return 0;
 
}