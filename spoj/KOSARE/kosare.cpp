#include <iostream>
#include <algorithm>
#include <string>
#include "stdio.h"
#include <map>

using namespace std;

#define type long long int
 
int N, M;

//n^t mod p
type pow_mod(type n, type t, type p)
{	

	if(n == 0 && t == 0)
		return 1;

	if(n == 0)
		return 0;

	if( t == 0)
		return 1;

	if(t == 1)
		return (n % p);

	if( t % 2 )
	{
		type result = pow_mod(n, t/2, p);
		result = (result*result) % p;
		result = (result*n)% p;
		return result;
	}
	else
	{
		type result = pow_mod(n, t/2, p);
		result = result*result;
		result = result % p;
		return result;
	}

}

const int big_prime = 1000000007;

int sets[2097152];
type b[2097152];

void genB(type s, type e)
{
	// cout<<s<<" "<<e<<"\n";
	if(s == e)
	{
		b[s] = sets[s];
		return;
	}

	type pivot = (e-s)/2 + s;

	genB(s, pivot);
	genB(pivot+1, e);

	for(int i=s; i <= pivot; i++)
	{
		b[pivot + i - s + 1] += b[i];
		// b[pivot + i - s + 1] %= big_prime;
	}
}

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	type res = 0;
	
	for(int a =0; a<2097152; a++)
	{
		sets[a] = 0;
		b[a] = 0;
	}

	cin>>N>>M;
	int mx = 0;
	for(int i=0; i<N; i++)
	{
		int k,t;
		cin>>k;

		int set = 0;
		// type total = 0;

		for(int j=0; j<k; j++)
		{
			cin>>t;
			set |= 1<<(t-1);
		}

		mx =  max(mx, set);
		sets[set]++;
		// cout<< (1<<M) -1<<"\n";
	}

	genB(0, (1<<M) -1);

	for(int i=0; i<(1<<M); i++)
	{
		// cout<<i<<" "<<b[i]<<"\n";
		// cout<<sets[i]<<"\n";
		if(__builtin_popcount(i) % 2 )
			res += pow_mod(2, b[i], big_prime);
		else
			res -= pow_mod(2, b[i], big_prime);

		if( res >= big_prime )
			res -= big_prime;

		if( res < 0 )
			res += big_prime;

		// res %= big_prime;
	}

	if( M % 2 == 0)
		res = big_prime - res;

	// res %= big_prime;

	// if( res < 0 )
	// 	res += big_prime;

	// cout<<mx<<"\n";
	// cout<<sets[0]<<"\n";

	cout<< res <<"\n";

	return 0;
}