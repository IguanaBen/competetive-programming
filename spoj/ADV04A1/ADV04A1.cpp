#include <iostream>
#include <algorithm>
#include <string>
#include "stdio.h"

using namespace std;

#define type long long int

//hackenbush

type black_factors[1042]; // number of 1/2^i values in game
type white_factors[1042];

int main()
{
	//std::ios_base::sync_with_stdio(0);
	// cin.tie(NULL);
	int t;
	cin>>t;

	string str;
	while(t--)
	{
		int n;
		cin>>n;

		for(int j=0; j<1010; j++)
		{
			black_factors[j] = 0;
			white_factors[j] = 0;
		}
		for(int i=0; i<n; i++)
		{
			cin>>str;
			// cout<<str<<"\n"
			int pw = 0;
			for(int j=0; j<str.length(); j++)
			{
				// cout<<pw<<"\n";

				if(j > 0)
					if(str[j] != str[j-1] || pw > 0)
						pw++;

				if(str[j] == 'B')
					black_factors[pw]++;
				else
					white_factors[pw]++;

				
			}
		}
		
		for(int j=1005; j>=0; j--)
		{
			black_factors[j] += black_factors[j+1]/2;
			black_factors[j+1] = black_factors[j+1] % 2;
				
			white_factors[j] += white_factors[j+1]/2;
			white_factors[j+1] = white_factors[j+1] % 2;
		}

		for(int j=1005; j>=0; j--)
			black_factors[j] -= white_factors[j];

		bool white_win = true;
		for(int j=0; j<1005; j++)
			if(black_factors[j] > 0)
				white_win = false;
			else
				if(black_factors[j] < 0)
					break;

		/*for(int j=0; j<13; j++)
			cout<<black_factors[j]<<" ";
		cout<<"\n";*/

		if(white_win)
			cout<<"Loss\n";
		else
			cout<<"Win\n";
	}

	return 0;
}