#include <iostream>
#include <algorithm>
#include "stdio.h"

using namespace std;

#define type long long int

// http://math.stackexchange.com/questions/761670/how-to-find-the-lcm-sum-function-lcm1-n-lcm2-n-lcmn-n

type phi[1000042];
type result[1000042];



int main()
{
	//std::ios_base::sync_with_stdio(0);
	// cin.tie(NULL);
	phi[1] = 1;
	phi[2] = 1;

	for(int i=1; i<1000042; i++)
		phi[i] = i;	
	
	for(int i=2; i<1000042; i++)
		if(phi[i] == i)
			for(int j=i; j<1000042; j += i)
				phi[j] -= phi[j]/i;

	for(int i=1; i<1000042; i++)
		for(int j=i; j<1000042; j+=i)
			result[j] += phi[i]*i;
	// result[1] = 1;
	// cout<<result[1]<<"\n";
	int t;
	cin>>t;
	while(t--)
	{
		int x;
		scanf("%d", &x);
		// cin>>x;
		type res = ((result[x] + 1)*x)/2;
		printf("%lld\n", res);
		// cout<<res<<"\n";
	}

	return 0;
}