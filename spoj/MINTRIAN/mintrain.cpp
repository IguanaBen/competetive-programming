#include <iostream>
#include <vector>
#include <queue>
#include <set>
#include <algorithm>
#include <sstream>

using namespace std;

vector<int> graph[1042];
int neighbour_count[1042];
int removed[1042];

void zero_graphs(int n)
{
	for(int i=0; i<n + 2; ++i)
		graph[i].clear();

}


struct cmp
{
    bool operator() (const int &a, const int &b)
    {
        if (neighbour_count[a] > neighbour_count[b]) return true;
        if (neighbour_count[a] < neighbour_count[b]) return false;
        return a<b;
    }
};

//maximum cardinality search
vector<int> mcs(vector<int> graph[], int n)
{
	vector<int> res;
	set<int, cmp> marked_heap;

	for(int i=0; i<n+3; i++)
		neighbour_count[i] = 0;


	for(int i=0; i<n; i++)
	{
		marked_heap.insert(i);
		removed[i] = 0;
	}

	while( !marked_heap.empty() )
    {
        int mn = *(marked_heap.begin());
        marked_heap.erase(marked_heap.begin());
        res.push_back(mn);
	    removed[mn] = 1;

	    // for(int i=0; i<n; i++)
	    // 	cout<<neighbour_count[i]<<" ";
	    // cout<<"\n";

        for(int i=0; i< graph[mn].size(); ++i)
        {
        	if(!removed[graph[mn][i]])
        	{
	        	marked_heap.erase(marked_heap.find(graph[mn][i]));
	        	++neighbour_count[graph[mn][i]];
	        	marked_heap.insert(graph[mn][i]);
	        }

        }
    }

    return res;
}

int pos_in_order[1042];

int main()
{

	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	int t;
	cin>>t;

	while(t--)
	{
		int n,m;
		// cin>>n>>m;

		string s1;
		string s2;

		cin>>s1;
		// cout<<s1<<"\n";
		// getline(cin, s2);
		// cout<<s2<<"\n";
		string new_s1 = " ";

		for(int i=0; i<s1.length(); i++)
			if(s1[i] >= '0' && s1[i] <= '9')
				new_s1 += s1[i];
			else
				new_s1 += " ";

		stringstream  ss(new_s1);
		ss>>n>>m;
		// cout<<n<<" "<<m<<"\n";


		// cout<<s1<<" "<<s2<<" ";
		// cout<<new_s1<<" "<<new_s2<<"\n";

		// return 0;

		int x, y;
		zero_graphs(n);

		set<pair<int, int> > edge_set;

		for(int i=0; i<m; i++)
		{
			// cin>>x>>y;
			cin>>s2;
			string new_s2 = " ";

			for(int i=0; i<s2.length(); i++)
			if(s2[i] >= '0' && s2[i] <= '9')
				new_s2 += s2[i];
			else
				new_s2 += " ";
			stringstream sss(new_s2);

			// cout<<new_s2<<" \n";
			sss>>x>>y;
			// cout<<x<<" "<<y<<"\n";

			graph[x].push_back(y);
			graph[y].push_back(x);
			edge_set.insert(make_pair(x,y));
			edge_set.insert(make_pair(y,x));
		}
			// return 0;
		// cout<<n<<" "<<m<<"\n";
		vector<int> pe_ordering = mcs(graph, n);
		// reverse(pe_ordering.begin(), pe_ordering.end());

		for(int i=0; i<pe_ordering.size(); ++i)
		{
			pos_in_order[pe_ordering[i]] = i;
			// cout<<pe_ordering[i]<<"\n";
		}

		bool ff = true;

		for(int i=pe_ordering.size()-1; i>=0; i--)
		{
			int mx = -1;
			int u = -1;
			for(int j=0; j<graph[pe_ordering[i]].size(); j++)
				if( mx < pos_in_order[graph[pe_ordering[i]][j]] && pos_in_order[graph[pe_ordering[i]][j]] < i)
				{
					mx = pos_in_order[graph[pe_ordering[i]][j]];
					u = graph[pe_ordering[i]][j];
				}

			if(mx != -1)
				for(int j=0; j<graph[pe_ordering[i]].size(); j++)
					if( u != graph[pe_ordering[i]][j])
						if(mx > pos_in_order[graph[pe_ordering[i]][j]])
							if(edge_set.find(make_pair(u, graph[pe_ordering[i]][j])) == edge_set.end())
								ff = false;

		}

		if(ff)
			cout<<"YES\n";
		else
			cout<<"NO\n";

		// cout<<"\n";
		// cout<<t<<"\n";
		// cin>>n>>m;
	}
	return 0;
}