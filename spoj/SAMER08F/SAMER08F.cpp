#include <iostream>
#include <algorithm>
#include <string>
#include "stdio.h"

using namespace std;

#define type long long int



int result[133];

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	result[0] = 0;
	for(int i=1; i<102; i++)
	{
		result[i] = result[i-1] + i*i;
	}

	int n = 1;
	while(1)
	{
		cin>>n;
		if(n == 0)
			return 0;

		cout<<result[n]<<"\n";
	}

	return 0;
}