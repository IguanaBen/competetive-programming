#include <iostream>
#include <algorithm>
#include <string>
#include "stdio.h"
#include <map>
#include <vector>

using namespace std;

#define type long long int

vector<int> tree[100010];

struct nfo
{
	int g;
	type x[22];
	type y[22];
	nfo(int k)
	{
		g = 0;
		for(int i=0; i<k+1; i++)
		{
			x[i] = 0;
			y[i] = 0;
		}
	}
};

int N, S, K;

nfo solve(int current, int prev, int k)
{
	// cout<<"V: "<<current<<"\n";

	if(tree[current].size() == 1 && tree[current][0] == prev)
	{
		nfo res(k);
		res.g = 0;
			
		res.y[0] = 1;
		return res;
	}

	nfo res(k);
	for(int i=0; i<tree[current].size(); i++)
	{

		if(tree[current][i] != prev)
		{
			// cout<<"C: "<<current<<" Child: "<<tree[current][i]<<"\n";
			nfo t = solve(tree[current][i], current, k);
			res.g += t.g;
			for(int j=0; j<k+1; j++)
			{
				res.x[j] += t.x[j];
				res.y[j] += t.y[j];
			}
		}
	}



	// cout<<res.g<<"\n";

	for(int i=0; i<k ;i++)
		res.x[i] = res.x[i+1];

	for(int i=k; i>0; i--)
		res.y[i] = res.y[i-1];

	res.x[k] = 0;
	res.y[0] = 1;

	
	// cout<<"V1: "<<current<<" || ";
	// for(int i=0; i<=k; i++)
	// 	cout<<res.y[i]<<" ";
	// cout<<"\n";

	// cout<<"V2: "<<current<<" || ";
	// for(int i=0; i<=k; i++)
	// 	cout<<res.x[i]<<" ";
	// cout<<"\n";

	if(current != 1)
	{

		int c = (res.y[k] + S - 1)/S;

		res.x[k] = S*c;
		res.g += c;

		// cout<<"C: "<<c<<"\n";

		for(int i=0; i<=k; i++)
		{
			int t = min(res.x[i], res.y[i]);
			res.x[i] -= t;
			res.y[i] -= t;
		}

		for(int i=0; i<k; i++)
		{
			int t = min(res.x[i+1], res.y[i]);
			res.x[i+1] -= t;
			res.y[i] -= t;
		}

		// cout<<"VR: "<<current<<" || ";
		// for(int i=0; i<=k; i++)
		// 	cout<<res.y[i]<<" ";
		// cout<<"\n";

		// cout<<"VRX: "<<current<<" || ";
		// for(int i=0; i<=k; i++)
		// 	cout<<res.x[i]<<" ";
		// cout<<"\n";


		// cout<<"Ret: "<<current<<" "<<res.g<<"\n";
		return res;
	}
	else
	{
		for(int i=0; i<=k; i++)
		{
			for(int j=i;j<=k; j++)
			{
				int t = min(res.x[j], res.y[i]);
				res.x[j] -= t;
				res.y[i] -= t;
			}
		}

		int sumY = 0;
		for(int i=0; i<=k; i++)
			sumY += res.y[i];

		// cout<<"SUM: "<<sumY<<"\n";

		int dv = (sumY + S - 1)/S;
		res.g += dv;

		return res;
	}
}

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);

	cin>>N>>S>>K;

	for(int i=0; i<N-1; i++)
	{
		int x,y;
		cin>>x>>y;
		tree[x].push_back(y);
		tree[y].push_back(x);
	}

	if( N != 1)
	{
		nfo n= solve(1, 0, K);
		cout<<n.g<<"\n";
	}
	else
		cout<<"1\n";

	return 0;
}