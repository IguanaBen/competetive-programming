#include <iostream>
#include <algorithm>
#include <string>
#include "stdio.h"
#include <map>

using namespace std;

#define type long long int
 
 map<int, type> supermap;
 map<int, type> supermap2;


//n^t mod p
type pow_mod(type n, type t, type p)
{	

	if( t == 0 || n == 0)
		return 1;

	if(t == 1)
		return (n % p);

	if( t % 2 )
	{
		type result = pow_mod(n, t/2, p);
		result = result*result*n;
		result = result % p;
		return result;
	}
	else
	{
		type result = pow_mod(n, t/2, p);
		result = result*result;
		result = result % p;
		return result;
	}

}

const int big_prime = 1000000007;

const int N = 50002;

int mu[N];
int mu_prefix_sum[N];

type coprime_triples(int A, int B, int C)
{
	type res = 0;
	type mn = min(min(A,B),C);

	int next = 1;

	int dA, dB, dC;

	for(int i=1; i<=mn;)
	{
		dA = A/i;
		dB = B/i;
		dC = C/i;

		next = min(min(A/dA, B/dB), C/dC);

		res += (type)dA*dB*dC*(mu_prefix_sum[next] - mu_prefix_sum[i-1]);

		i = next + 1;
	}
	return res;
}

int coprime_pairs(int A, int B)
{
	int res = 0;
	int mn = min(A,B);

	int dA, dB;
	int next = 1;
	for(int i=1; i<=mn;)
	{
		dA = A/i;
		dB = B/i;

		next = min(A/dA, B/dB);

		res += dA*dB*(mu_prefix_sum[next] - mu_prefix_sum[i-1]);

		i = next + 1;
	}
	return res;
}

type pows[12][N];


int main()
{
	// std::ios_base::sync_with_stdio(0);
	// cin.tie(NULL);

	mu[1] = 1;

	for(int i=1; i<=N; i++)
		for(int j=2*i; j<=N; j+=i)
			mu[j] -= mu[i];

	mu_prefix_sum[1] = mu[1];
	for(int i=2; i<=N; i++)
		mu_prefix_sum[i] = mu_prefix_sum[i-1] + mu[i];

	int t;
	cin>>t;
	while(t--)
	{
		int n;
		cin>>n;
		type res = n*n*n - 3*n*(n*n - coprime_pairs(n,n)) + (n*n*n - coprime_triples(n,n,n));
		cout<<res<<"\n";
	}
	return 0;
}