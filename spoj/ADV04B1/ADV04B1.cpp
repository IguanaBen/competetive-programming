#include <iostream>
#include <algorithm>
#include <string>
#include "stdio.h"

using namespace std;

#define type long long int

// Delannoy number (central)

const int prime = 1000003;

//
type odwr_mod(type a, type b)
{
	type u,w,x,z,q;
	  u = 1; w = a;
	  x = 0; z = b;
	  while(w)
	  {
	    if(w < z)
	    {
	      q = u; u = x; x = q;
	      q = w; w = z; z = q;
	    }
	    q = w / z;
	    u -= q * x;
	    w -= q * z;
	  }
	  if(z == 1)
	  {
	    if(x < 0) x += b;
	   return x;
	  }
	  
	  return -1; //nie powinno tak byc
}


type delannoy[1000042];

int main()
{
	std::ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	// cout<<odwr_mod(4,7)<<"\n";
	delannoy[0] = 1;
	delannoy[1] = 3;
	// delannoy[2] = 3;
	for(int i=2; i<1000042; i++)
		delannoy[i] = ( (3*(2*i-1)*delannoy[i-1] - (i-1)*(delannoy[i-2]) ) % prime + prime)* odwr_mod(i, prime) % prime;

	// cout<<odwr_mod(28, prime)<<"\n";
	// for(int i=28;i<29; i++)
		// cout<<delannoy[i]<<"\n";

	int t;
	cin>>t;
	while(t--)
	{
		int n;
		cin>>n;
		cout<<delannoy[n-1]<<"\n";
	}

	// cout<<(odwr_mod(2, prime)* 26) % prime<<"\n";
	// cout<<delannoy[2]<<"\n";

	return 0;
}