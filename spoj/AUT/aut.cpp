#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;

int dp[41][41][41*41];

int c[41];
int l[41];

int main()
{
	// std::ios_base::sync_with_stdio(0);
	// cin.tie(NULL);

	int t;
	// cin>>t;
	scanf("%d", &t);

	while(t--)
	{
	int n,k;
	// cin>>n>>k;
	scanf("%d %d", &n, &k);

	int maxC = 0;
	int limit = 0;
	FORI(i, n){ /*cin>>c[i+1];*/ scanf("%d", &c[i+1]); maxC = max(maxC, c[i+1]); }
	FORI(i, n){ /*cin>>l[i+1];*/ scanf("%d", &l[i+1]); limit = max(limit, l[i+1]); }

	int maxp = maxC*limit;
	maxp = min(maxp, k);

	int res = 0;

	//rekursja??
	// cout<<" "<<maxp<<" "<<" "<<limit<<"\n";

	FORI(snack, n+1)
	{
		if(snack > 0)
			FORI(boughtm, limit+1)
				FORI(p, maxp+1)
					dp[snack][boughtm][p] =  dp[snack-1][boughtm][p] + min(boughtm, l[snack])*c[snack];

		// if(snack > 0)
		for(int boughtm = limit; boughtm >=0; boughtm--)
			FORI(p, maxp+1)
			{
				if(limit - boughtm <= l[snack])
					if(boughtm == limit || p-c[snack] < 0)
						dp[snack][boughtm][p] = dp[snack][boughtm][p];
					else
						dp[snack][boughtm][p] = max(dp[snack][boughtm][p], dp[snack][boughtm+1][p-c[snack]]);
				else
					if(snack == 0)
						dp[snack][boughtm][p] = 0;
					else
					{
						for(int i=0; i<=l[snack]; i++)

							if(p - i*c[snack] >=0 && boughtm+i < limit+1)
								dp[snack][boughtm][p] = max(dp[snack][boughtm][p], 
											dp[snack-1][boughtm+i][p-i*c[snack]]
											+ min((boughtm + i), l[snack])*c[snack]);
							else
								break;
					}
			}

		/*FORI(boughtm, limit+1)
			FORI(p, maxp+1)
			{
				if(snack == 0)
					dp[snack][boughtm][p] = 0;
				else
				{
					// int mx = -SINF;
					// dp[snack][boughtm][p] = 0;
					for(int i=0; i<=l[snack]; i++)

						if(p - i*c[snack] >=0 && boughtm+i < limit+1)
							dp[snack][boughtm][p] = max(dp[snack][boughtm][p], 
										dp[snack-1][boughtm+i][p-i*c[snack]] 
											/*+ min((boughtm + i), l[snack])*c[snack]*/
								/*		);
						else
							break;
				}
			}*/
	}

	FORI(p, maxp+1)
		res = max(res, dp[n][0][p]);
	// cout<<dp[1][2][0]<<"\n";
	// cout<<res<<"\n";
	printf("%d\n", res);
	}

	return 0;
}