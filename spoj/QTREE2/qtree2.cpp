#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include <cstdio>
#include <string.h>

using namespace std;

#define type long long int

#define LL long long int
#define ULL unsigned long long int
#define IPR pair<int, int>
#define LPR pair<long long, long long>
#define FORI(i, C) for(int i=0; i<(int)C; i++) 
#define SINF 1000000042;


const int maxN = 10042;
const int maxLog2N = 14;
const int lastLevelTree = 16384;

vector<int> tree[maxN + 1];
int segmentTree[lastLevelTree*2 + 3];

vector<int> chains[maxN];
int chainIndex[maxN + 1];	// index of chain where given vertex belong
int treeIndex[maxN + 1];

int preorder[maxN + 1];
int parent[maxN + 1];
int treeSize[maxN + 1];
// int chainEnd[maxN + 1];
int chainBeg[maxN + 1];
int edgeCosts[maxN + 1]; 	//cost from v to its parent
vector<IPR> edges;
int edgeWeights[maxN + 1];


int dynLCA[maxN + 1][maxLog2N+1];

int current_chain;
int current_tree_ind;

void reset(int n)
{
	current_chain = 0;
	current_tree_ind = 1;
	edges.clear();
	for(int i=0; i<n+3; i++)
	{
		tree[i].clear();
		chainBeg[i] = -1;
		// chains[i].clear();
		// edgeCosts[i] = 0;
	}
}

int initPreorder(int v, int prev, int c_index)
{
	preorder[v] = c_index;
	c_index++;
	for(int i=0; i<tree[v].size(); i++)
		if(tree[v][i] != prev)
		{
			c_index = initPreorder(tree[v][i], v, c_index);
			// edgeCosts[tree[v][i].first] = tree[v][i].second;
			// parent[tree[v][i].first] = v;
		}

	treeSize[v] = c_index - preorder[v];
	return c_index;
}

// u descendant of v
int descendant(int u, int v)
{
	return (preorder[u] >= preorder[v] &&
			preorder[u] < preorder[v] + treeSize[v]);
}

int lca(int u, int v)
{
	if(descendant(u,v)) return v;
	if(descendant(v,u)) return u;

	int current = u;
	int lgn = maxLog2N;

	while(lgn >= 0)
	{
		if( descendant(v, dynLCA[current][lgn]))
			lgn--;
		else
			current = dynLCA[current][lgn];
	}

	return dynLCA[current][0];
}

void initLCA(int n)
{
	for(int i=1; i<=n; ++i)
		dynLCA[i][0] = 0;

	for(int i=0; i<=n; ++i)
		for(int j=0; j<= maxLog2N; ++j)
			dynLCA[i][j] = 1;  // root

	for(int i=0; i<n-1; ++i)
	{
		// cout<<edges[i].first<<" "<<edges[i].second<<"\n";
		if(preorder[edges[i].first] < preorder[edges[i].second])
			dynLCA[edges[i].second][0] = edges[i].first;
		else
			dynLCA[edges[i].first][0] = edges[i].second;
	}



	// for(int i=1; i<=n; ++i)
		// for(int j=0; j<tree[i].size(); ++j)
			// if( preorder[i] < preorder[tree[i][j]])
				// dynLCA[tree[i][j]][0] = i;
				// else
					// dynLCA[i][0] = tree[i][j];

	for(int i=1; i<=n; ++i)
		for(int j=1; j<= maxLog2N; ++j)
			dynLCA[i][j] = dynLCA[dynLCA[i][j-1]][j-1];
}

void initTree(int n)
{
	parent[1] = 1;	//not neccessary i think
	for(int i=0; i<n-1; i++)
	{
		if(preorder[edges[i].first] < preorder[edges[i].second])
		{
			// edgeCosts[edges[i].second] = edgeWeights[i];
			parent[edges[i].second] = edges[i].first;
		}
		else
		{
			// edgeCosts[edges[i].first] = edgeWeights[i];
			parent[edges[i].first] = edges[i].second;
		}
	}
}

void initHLD(int v)
{
	if(chainBeg[current_chain] == -1)
		chainBeg[current_chain] = v;

	// cout<<"Chain: "<<current_chain<<" "<<v<<" "<<current_tree_ind<<"\n";

	chainIndex[v] = current_chain;
	treeIndex[v] = current_tree_ind;
	segmentTree[ lastLevelTree +  current_tree_ind] = edgeCosts[v];
	++current_tree_ind;

	int mx_ind = -1;
	int mx_size = -1;

	for(int i=0; i<tree[v].size(); i++)
		if(preorder[tree[v][i]] > preorder[v])
			if(treeSize[tree[v][i]] > mx_size)
			{
				mx_size = treeSize[tree[v][i]];
				mx_ind = i;
			}

	if(mx_ind != -1)
	{
		initHLD(tree[v][mx_ind]);

		for(int i=0; i<tree[v].size(); i++)
			if(i != mx_ind && preorder[tree[v][i]] > preorder[v] )
			{
				// cout<<"abc\n";
				current_chain++;
				initHLD(tree[v][i]);
			}
	}
}

//not really segment tree
void initSegmentTree()
{
	// fill(segmentTree, segmentTree + maxN*2 + 3,  0);

	// int treeInd = 1;
	// for(int i=0; i<n_chains; ++i)
	// {
	// 	for(int j=0; j<chains[i].size(); ++j)
	// 	{
	// 		treeIndex[chains[i][j]] = treeInd;
	// 		segmentTree[lastLevelTree + treeInd] = edgeCosts[chains[i][j]];
	// 		treeInd++;
	// 	}
	// 	chainEnd[i] = chains[i][ chains[i].size()-1];
	// }

	for(int i=lastLevelTree-1; i>0; --i)
		segmentTree[i] = max(segmentTree[(i<<1)], segmentTree[(i<<1)+1]);
}

int setSegmentTree(int x, int val)
{
	int cInd = lastLevelTree + x;
	segmentTree[cInd] = val;

	cInd >>= 1;

	while(cInd > 0)
	{
		segmentTree[cInd] = max(segmentTree[(cInd<<1)], segmentTree[(cInd<<1) + 1]);
		cInd >>= 1;
	}
}
/*
int querySingle(int a)
{
	int cInd = lastLevelTree + a;
	int res = segmentTree[cInd];

	while(cInd > 0)
	{
		if(cInd % 2)
			res += segmentTree[cInd-1];

		cInd = cInd >> 1;
	}

	return res;
}*/

/*
int queryTree(int a, int b)
{
	return querySingle(b) - querySingle(a-1);
}*/

int queryTree(int a, int b)
{
	int aI = a + lastLevelTree;
	int bI = b + lastLevelTree;

	int res = max(segmentTree[aI], segmentTree[bI]);

	while( (aI>>1) != (bI>>1))
	{
		if(aI % 2 == 0) res = max(res, segmentTree[aI + 1]);
		if(bI % 2) res = max(res, segmentTree[bI - 1]);

		aI >>= 1;
		bI >>= 1;
	}

	return res;
}

// b is the ancestor of a
/*
int distance_ancestor(int a, int b)
{
	if(a == b)
		return 0;

	if(chainIndex[a] == chainIndex[b])
	{
		// cout<<"oo\n";
		return queryTree(treeIndex[a], treeIndex[b] - 1);
	}
	else
	{
		// cout<<"A: "<<a<<" "<<treeIndex[a]<<" "<<chainEnd[chainIndex[a]]<<"\n";
		// cout<<"B: "<<b<<" "<<treeIndex[ chainEnd[chainIndex[a]] ]<<" "<<chainEnd[chainIndex[b]]<<"\n";

		// cout<<parent[chainEnd[chainIndex[a]]]<<" "<<b<<"\n";
		return queryTree(treeIndex[a], treeIndex[ chainEnd[chainIndex[a]] ])
				+ distance_ancestor( parent[chainEnd[chainIndex[a]]], b);
	}
}*/


int distance_ancestor(int a, int b)
{

	// return 0;
	if(a == b)
		return 0;

	int res = -SINF;

	int ta = a;

	// cout<<endl<<endl;
	while(1)
	{
		// cout<<ta<<" "<<b<<" "<<res<<"\n";
	// cout<<chainIndex[ta]<<" "<<chainIndex[b]<<"\n";
	// cout<<treeIndex[ta]<<" "<<treeIndex[b]<<"\n";

		if(ta == b)
			return res;

		if(chainIndex[ta] == chainIndex[b])
		{
			// return 0;
			return max(res, queryTree(treeIndex[b] + 1, treeIndex[ta] ));
		}
		else
		{
			// cout<<"chbeg: "<<chainBeg[chainIndex[ta]]<<"\n";
			// cout<<"sst: "<<segmentTree[treeIndex[3] + lastLevelTree]<<"\n";
			// cout<<"sst: "<<segmentTree[treeIndex[ta] + lastLevelTree]<<"\n";
			// cout<<queryTree( treeIndex[ chainBeg[chainIndex[ta]] ], treeIndex[ta])<<"|||\n";
			res = max(res, queryTree( treeIndex[ chainBeg[chainIndex[ta]] ], treeIndex[ta]));
			ta = parent[chainBeg[chainIndex[ta]]];
		}
	}
	return res;
}

int distanceN(int a, int b)
{
	int lc = lca(a,b);
	// int lc = a;
	// cout<<a<<" "<<b<<"\n";
	// cout<<preorder[a]<<" "<<preorder[b]<<"\n";
	// cout<<lc<<"\n";
	// return 0;
	// cout<<distance_ancestor(a, lc)<<" DDA\n";
	return max(distance_ancestor(a, lc), distance_ancestor(b, lc));
}

void updateCost(int child_node, int val)
{
	// cout<<child_node<<"\n";
	setSegmentTree(treeIndex[child_node], val);
}

int main()
{
	// std::ios_base::sync_with_stdio(0);
	// cin.tie(NULL);

	// int t;
	// scanf("%d", &t);
	// cin>>t;
	// while(t--)
	{
		int n,q;
		// cin>>n;
		scanf("%d", &n);
		// cout<<n<<"\n";
		int a,b,w;
		reset(n);

		for(int i=0; i<n-1; i++)
		{
			// cin>>a>>b>>w;
			scanf("%d%d", &a, &b);
			tree[a].push_back(b);
			tree[b].push_back(a);
			edges.push_back(make_pair(a,b));
			// edgeWeights[i] = w;
		}

		initPreorder(1, 0, 1);
		initLCA(n);
		initTree(n);
		initHLD(1);
		initSegmentTree();
		// for(int a = 0; a<=maxLog2N; a++)
			// cout<<dynLCA[10][a]<<" ";

		// cout<<"\n";
		// return 0;

		// string cmd;
		char cmd[10];
		// cin>>cmd;
		scanf("%s", &cmd);

		// cout<<edgeCosts[2]<<"\n";

		while(cmd[0] != 'D')
		{
			int x,y;
			// cin>>x>>y;
			scanf("%d%d", &x, &y);
			if(cmd[0] == 'Q')
				printf("%d\n", distanceN(x,y));
			else
				if(preorder[edges[x-1].first] > preorder[edges[x-1].second])
					updateCost(edges[x-1].first, y);
				else
					updateCost(edges[x-1].second, y);

			scanf("%s", &cmd);
		}
	}

	return 0;
}