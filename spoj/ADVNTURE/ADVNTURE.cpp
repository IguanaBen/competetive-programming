// #include <iostream>
#include <algorithm>
// #include <string>
#include "stdio.h"
#include <map>

using namespace std;

#define type long long int
 
 map<int, type> supermap;
 map<int, type> supermap2;


//n^t mod p
type pow_mod(type n, type t, type p)
{	

	if( t == 0 || n == 0)
		return 1;

	if(t == 1)
		return (n % p);

	if( t % 2 )
	{
		type result = pow_mod(n, t/2, p);
		result = result*result*n;
		result = result % p;
		return result;
	}
	else
	{
		type result = pow_mod(n, t/2, p);
		result = result*result;
		result = result % p;
		return result;
	}

}

const int big_prime = 1000000007;

const int N = 50002;

int mu[N];
int mu_prefix_sum[N];

type coprime_triples(int A, int B, int C)
{
	// type key = A*50003*50003 + B*50003 + C;

	// if(supermap.find(key) != supermap.end())
		// return supermap[key];

	type res = 0;
	type mn = min(min(A,B),C);

	int next = 1;

	int dA, dB, dC;

	for(int i=1; i<=mn;)
	{
		dA = A/i;
		dB = B/i;
		dC = C/i;

		next = min(min(A/dA, B/dB), C/dC);

		res += (type)dA*dB*dC*(mu_prefix_sum[next] - mu_prefix_sum[i-1]);

		i = next + 1;
	}

	// supermap[key] = res;
	return res;
}

int coprime_pairs(int A, int B)
{
	 // type key = A*50002 + B;

	 // if(supermap2.find(key) != supermap2.end())
		//  return supermap2[key];

	int res = 0;
	int mn = min(A,B);

	int dA, dB;
	int next = 1;
	for(int i=1; i<=mn;)
	{
		dA = A/i;
		dB = B/i;

		next = min(A/dA, B/dB);

		res += dA*dB*(mu_prefix_sum[next] - mu_prefix_sum[i-1]);

		i = next + 1;
	}

	// supermap2[key] = res;
	return res;
}

type pows[12][N];


int main()
{
	// std::ios_base::sync_with_stdio(0);
	// cin.tie(NULL);

	mu[1] = 1;

	for(int i=1; i<=N; i++)
		for(int j=2*i; j<=N; j+=i)
			mu[j] -= mu[i];


	mu_prefix_sum[1] = mu[1];
	for(int i=2; i<=N; i++)
		mu_prefix_sum[i] = mu_prefix_sum[i-1] + mu[i];

	for(int i=1; i<11; i++)
	{
		pows[i][0] = 1;

		type kp = i;

		for(int j=1; j<N; j++)
		{
			pows[i][j] = ( pows[i][j-1] + kp);
			if(pows[i][j] >= big_prime) pows[i][j] -= big_prime;

			kp = (kp*i) % big_prime;
		}
	}

	// cout<<pows[2][1]<<"\n";

	int t;
	scanf("%d", &t);
	// cin>>t;
	// t=1;
	while(t--)
	{
		// cout<<t<<"\n";
		int A, B, C, K;
		scanf("%d%d%d%d", &A, &B, &C, &K);
		// cin>>A>>B>>C>>K;
		// cout<<A<<" "<<B<<" "<<C<<" "<<K<<"\n";
		type res = 0;

		type mx = max(max(A,B),C);

		int next = 1;

		int dA, dB, dC;
		int zA, zB, zC;
		
		for(int i=1; i<=mx;)
		{
			// cout<<next<<"\n";
			dA = A/i;
			dB = B/i;
			dC = C/i;

			if(i > A) zA = N; else zA = A/dA;
			if(i > B) zB = N;  else zB = B/dB;
			if(i > C) zC = N;  else zC = C/dC;

			next = min(min(zA, zB), zC);

			type coprime = coprime_triples(dA, dB, dC) + coprime_pairs(dA, dB) + 
							coprime_pairs(dA, dC) + coprime_pairs(dB, dC);

			coprime += (dA > 0);
			coprime += (dB > 0);
			coprime += (dC > 0);

			// if(dA > 0)
			// 	coprime++;
			// if(dB > 0)
			// 	coprime++;
			// if(dC > 0)
			// 	coprime++;

			// cout<<next<<" : "<<i-1<<" | "<<(pows[K][next] - pows[K][i-1])<<"\n";
			// cout<<coprime<<"\n";

			// coprime = coprime % big_prime;

			// int pw = (pows[K][next] - pows[K][i-1]);
			// pw = pw % big_prime;
			// if(pw < 0)
				// pw += big_prime;

			res += (coprime % big_prime) * (pows[K][next] - pows[K][i-1]);
			res %= big_prime;
			// res = res % big_prime;

			

			// res += dA*dB*dC*(mu_prefix_sum[next] - mu_prefix_sum[i-1]);

			i = next + 1;
		}

		if(res < 0)
			res+=big_prime;

		printf("%lld\n", res);
		// cout<<"R: ";
		// cout<<res<<"\n";
	}



	// for(int i=1;i<20;i++)
		// cout<<mu[i]<<"\n";

	// cout<<mu[5]*(5/s)

	// cout<<__gcd(2,4)<<"\n";

	return 0;
}