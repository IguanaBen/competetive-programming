#include <iostream>
#include <algorithm>
#include <string>
#include "stdio.h"

using namespace std;

#define type long long int
 
//n^t mod p
type pow_mod(type n, type t, type p)
{	

	if( t == 0 || n == 0)
		return 1;
	
	if(t == 1)
		return (n % p);

	if( t % 2 )
	{
		type result = pow_mod(n, t/2, p);
		result = result*result*n;
		result = result % p;
		return result;
	}
	else
	{
		type result = pow_mod(n, t/2, p);
		result = result*result;
		result = result % p;
		return result;
	}

}

const type big_prime = 1000000007;

const int N = 50042;

type mu[N], delta[N];
type mu_prefix_sum[N];


int dfff(int X)
{
	int prev = -1;
	int df = 0;
	for(int i=1; i<X; i++)
	{
		if(X/i != prev)
			df++;

		prev = X/i;
	}

	return df;
}

type coprime_triples(type A, type B, type C)
{
	type res = 0;
	type mn = min(min(A,B),C);

	int next = 1;
	for(int i=1; i<=mn;)
	{
		int dA = A/i;
		int dB = B/i;
		int dC = C/i;

		int zA = A/dA;
		int zB = A/dB;
		int zC = A/dC;

		next = min(min(zA, zB), zC);

		res += dA*dB*dC*(mu_prefix_sum[next] - mu_prefix_sum[i-1]);

		i = next + 1;
	}

	return res;
}

int main()
{
	// std::ios_base::sync_with_stdio(0);
	// cin.tie(NULL);

	/*for(int i=0;i<=N;i++)
		delta[i]=0;
	delta[1] = 1;

	for(int i=1;i<=N;i++)
	{
		mu[i]=delta[i];
		for(int j=i;j<=N;j+=i)
			delta[j]-=mu[i];
	}*/

	mu[1] = 1;

	for(int i=1; i<=N; i++)
		for(int j=2*i; j<=N; j+=i)
			mu[j] -= mu[i];

	mu_prefix_sum[1] = mu[1];

	for(int i=2; i<=N; i++)
		mu_prefix_sum[i] = mu_prefix_sum[i-1] + mu[i];

	// for(int i=1; i<8; i++)
	// 	cout<<mu[i]<<" ";

	// cout<<"\n";

	/*int prev = -1;
	int df = 0;
	for(int i=1; i<N; i++)
	{
		if(N/i != prev)
			df+=dfff(N/i);

		prev = N/i;
	}

	cout<<df<<"\n";*/
	int A,B,C, d;
	cin>>A>>B>>C;
	cin>>d;

	int c = 0;
	int ss = 0;
	for(int i=1; i<=A; i++)
		for(int j=1; j<=B; j++)
			for(int k=1; k<=C; k++)
				if(__gcd(__gcd(i,j),k) == d)
					c++;


	int c2 = 0;
	A/=d;
	B/=d;
	C/=d;

	for(int i=1; i<=min(min(A,B), C); i++)
	{
		// if(min(min(A,B), C) % i == 0)
		{
			c2 += (A/i)*(B/i)*(C/i)*mu[i];
		}
	}			

	cout<<c<<" "<<c2<<" "<<coprime_triples(A,B,C)<<"\n";

	return 0;
}